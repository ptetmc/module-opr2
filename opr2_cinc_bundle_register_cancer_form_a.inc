<?php

function opr2_cinc_bundle_register_cancer_form_a() {
  $bundle = array(
    'machine_name' => 'register_cancer_form_a',
    'fields' => array(
      array(
        'field_name' => 'field_serial_register_pc',
        'type' => 'serial',
        'cardinality' => '1',
        'instance' => 
        array (
          'label' => 'Serial Register-PC',
          'widget' => 
          array (
            'type' => 'serial',
          ),
        ),
      ),
    ),

    'instances' => array(
      'field_drug_consumption' => array(
        'i18n' => array(
          'label' => 'Drug abuse',
        ),
      ),
      'field_pancdis_acute_times2' => array(
        'i18n' => array(
          'label' => 'How many times did the patient have acute episodes before the diagnosis of pancreatic cancer?',
        ),
      ),
      'field_pancdisord' => array(
        'i18n' => array(
          'label' => 'Congenital pancreas malformations',
        ),
      ),
      'field_otherdis' => array(
        'label' => 'Egyéb krónikus betegségek',
      ),
      'field_bmi_before' => array(
        'label' => 'Jellemző Body Mass Index (BMI a pancreas tumort megelőzően)',
        'i18n' => array(
          'label' => 'Body Mass Index (BMI) before the diagnosis of pancreatic cancer',
        ),
      ),
      'field_weight_before' => array(
        'label' => 'Jellemző testsúly a pancreas tumort megelőzően',
      ),
      'field_height_before' => array(
        'label' => 'Testmagasság',
      ),
      'field_weight' => array(
        'label' => 'Testsúly diagnóziskor',
      ),
      'field_height' => array(
        'label' => 'Testmagasság diagnóziskor',
      ),
      'field_diag_date_ct' => array(
        'description' => 'vagy',
      ),
      'field_diag_date_mr' => array(
        'description' => 'vagy',
      ),
      'field_diag_date_ercp' => array(
        'description' => 'vagy',
      ),
      'field_abdpain' => array(
        'i18n' => array(
          'label' => 'Abdominal pain at the time of diagnosis',
        ),
      ),
      'field_vomiting_times2' => array(
        'label' => 'naponta hányszor',
        'i18n' => array(
          'label' => 'how many times per day',
        ),
      ),
      'field_fever' => array(
        'i18n' => array(
          'label' => 'Recurrent fever',
        ),
      ),
      'field_complication_death_date' => array(
        'label' => 'Halál időpontja',
        'i18n' => array(
          'label' => 'Date of death',
        ),
      ),
    ),

    'groups' => array(
      'group_symptoms' => array(
        'label' => '3. Panaszok, tünetek',
        'i18n' => array(
          'label' => '3. Symptoms and signs',
        ),
      ),

      'group_pancdis_fam' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'col_map' => '4',
          ),
        ),
      ),

      'group_finalreport' => array(
        'label' => '8. Zárójelentés',
        'i18n' => array('label' => '8. Final report'),
      ),
    ),

    'tree' => array(
      'group_paging' => array(
        'group_page_1' => array(
          'field_inpatient',
          'group_personal_cont' => array(
            'field_interview_date',
          ),
          'group_anamnestic' => array(
            'group_alcohol' => array(
              'field_alcohol_consumption',
              'field_alcohol_freq',
              'field_alcohol_amount2',
              'field_alcohol_since2',
              'group_alcohol_past' => array(
                'field_alcohol_past',
                'field_alcohol_past_freq',
                'field_alcohol_past_amount',
                'field_alcohol_past_years',
              ),
            ),
            'group_smoking' => array(
              'field_smoking',
              'field_smoking_amount2',
              'field_smoking_since2',
              'group_smoking_past' => array(
                'field_smoking_past',
                'field_smoking_past_amount',
                'field_smoking_past_years',
              ),
            ),
            'group_drugs' => array(
              'field_drug_consumption',
              'field_drug_name',
              'field_drug_amount',
              'field_drug_since3',
            ),
            'group_chemicalexposure' => array(
              'field_chemicalexposure',
              'field_chemicalexposure_type',
              'field_chemicalexposure_years',
            ),
            'group_infdis' => array(
              'group_infdis_hepb' => array(
                'field_infdis_hepb',
                'field_infdis_hepb_when',
              ),
              'group_infdis_hepc' => array(
                'field_infdis_hepc',
                'field_infdis_hepc_when',
              ),
              'group_infdis_hpv' => array(
                'field_infdis_hpv',
                'field_infdis_hpv_when',
              ),
              'group_infdis_chronvir' => array(
                'field_infdis_chronvir',
                'field_infdis_chronvir_type',
                'field_infdis_chronvir_when',
              ),
              'group_infdis_chronbac' => array(
                'field_infdis_chronbac',
                'field_infdis_chronbac_type',
                'field_infdis_chronbac_when',
              ),
              'group_infdis_other' => array(
                'field_infdis_other',
                'field_infdis_other_type',
                'field_infdis_other_when',
              ),
            ),
            'group_diabetes' => array(
              'field_diabetes',
              'field_diabetes_type',
              'field_diabetes_since',
            ),
            'group_lipdis' => array(
              'field_lipdis',
              'field_lipdis_since',
            ),
            'group_pancdis' => array(
              'field_pancdis',
              'field_pancdis_type',
              'field_pancdis_other',
              'group_pancdis_acute' => array(
                'field_pancdis_acute_times2',
                'field_pancdis_acute_first',
              ),
              'group_pancdis_chronic' => array(
                'field_pancdis_chronic_when',
                'field_pancdis_chronic_first',
                'field_pancdis_chron_acute_times',
              ),
              'group_pancdis_ai' => array(
                'field_pancdis_ai_when',
              ),
            ),
            'group_pancdis_fam' => array(
              'field_pancdis_fam',
              'field_pancdis_fam_deg1',
              'field_pancdis_fam_deg2',
              'field_pancdis_fam_deg3',
            ),
            'group_pancdisord' => array(
              'field_pancdisord',
              'field_pancdisord_type',
            ),
            'group_otherdis' => array(
              'field_otherdis',
              'field_otherdis_type',
            ),
            'group_medi' => array(
              'field_medi',
            ),
            'group_medi_list' => array(
              'field_medi_type',
              'field_medi_amount',
            ),
            'group_diet' => array(
              'field_diet',
              'field_diet_type',
            ),
            'group_bmi_before' => array(
              'field_weight_before',
              'field_height_before',
              'field_bmi_before',
            ),
            'field_anamnestic_notes',
          ), // end anamnestic

          'group_symptoms' => array(
            'field_symptom_since',

            'group_abdpain' => array(
              'field_abdpain',
              'field_abdpain_since3',
              'field_abdpain_type',
              'field_abdpain_strength',
              'field_stoma_loc',
              'field_stoma_loc_detail',
            ),

            'group_abdpain_general' => array(
              'field_abdpain_general',
              'field_abdpain_general_behav2',
            ),

            'group_nausea' => array(
              'field_nausea',
            ),
            'group_vomiting' => array(
              'field_vomiting',
              'field_vomiting_times2',
              'field_vomiting_contents2',
            ),
            'group_fever' => array(
              'field_fever',
              'field_fever_since2',
              'field_fever_amount2',
            ),
            'group_appetite' => array(
              'field_appetite',
            ),
            'group_weightloss' => array(
              'field_weightloss',
              'field_weightloss_weeks2',
              'field_weightloss_amount2',
            ),
            'group_stool' => array(
              'field_stool',
            ),
            'group_jaundice' => array(
              'field_jaundice',
            ),
          ), // end symptoms

          'group_patient_data' => array(
            'group_diag_date' => array(
              'field_diag_date',
              'field_diag_date_ct',
              'field_diag_date_mr',
              'field_diag_date_ercp',
              'field_diag_date_eus',
            ),

            'group_histver' => array(
              'field_histver',
              'field_histver_date',
            ),

            'group_patient_state' => array(
              'field_weight',
              'field_height',
              'field_bmi',
              'field_ecog',
            ),

            'group_tummark' => array(
              'group_tummark_row1' => array(
                'field_tummark_ca199',
                'field_tummark_ca199_date',
              ),
              'group_tummark_row2' => array(
                'field_tummark_cea',
                'field_tummark_cea_date',
              ),
            ),

            'group_patient_alive' => array(
              'field_patient_alive',
              'field_complication_death_date',
            ),
          ), // end patient data

          'group_tumor_data' => array(
            'group_tumor_stage' => array(
              'field_tumor_stage_t',
              'field_tumor_stage_n',
              'field_tumor_stage_m',
            ),

            'group_tumor_inline1' => array(
              'field_tumor_pos',

              'group_tumor_histology' => array(
                'field_tumor_histology_res', // TODO js
                'group_tumor_histology_indent' => array(
                  'field_tumor_histology_res_duct',
                  'field_tumor_histology_res_nonepi',
                ),
                'field_tumor_histology_sampling',
              ),

              'field_tumor_resect',
            ),

            'group_tumor_surgery' => array(
              'field_tumor_surgery',
              'field_tumor_surgery_date',
              'field_tumor_surgery_type',
              'group_tumor_surgery_palltype' => array(
                'field_tumor_surgery_palltype', // TODO js
              ),

              'field_tumor_surgery_obst',
            ),

            'group_tumor_pathstate' => array(
              'field_tumor_pathstate_pt',
              'field_tumor_pathstate_pn',
              'field_tumor_pathstate_poslymph',
              'field_tumor_pathstate_remlymph',
              'field_tumor_pathstate_mpm',
            ),

            'group_tumor_inline2' => array(
              'field_tumor_diffgrade',
              'field_tumor_residual',
              'field_tumor_perinv',
              'field_tumor_vascinv',
            ),

            'group_tumor_immhistochem' => array(
              'field_tumor_immhistochem_ck7',
              'field_tumor_immhistochem_ck20',
              'field_tumor_immhistochem_cdx2',
              'field_tumor_immhistochem_ki67',
              'field_tumor_immhistochem_mib1',
              'field_tumor_immhistochem_cga',
              'field_tumor_immhistochem_other',
            ),

            'group_tumor_neuroendocrin' => array(
              'field_tumor_whograde',
              'group_tumor_endoscopy' => array(
                'field_tumor_endoscopy',
                'group_tumor_biliarstent' => array(
                  'field_tumor_biliarstent',
                  'field_tumor_biliarstent_type',
                ),
                'field_tumor_duodenalstent',
              ),
              'field_tumor_ivradiology',
            ),
          ), // end tumor data

          'group_oncotherapy' => array(
            'group_oncotherapy_adenocarcinoma' => array(
              'group_radtherapy' => array(
                'field_radtherapy',
                'field_radtherapy_startdate',
                'field_radtherapy_enddate',
                'field_radtherapy_goal',
                'field_radtherapy_fracts',
                'field_radtherapy_dose',
              ),

              'group_chemtherapy' => array(
                'field_chemtherapy',
                'field_chemtherapy_startdate',
                'field_chemtherapy_enddate',
                'field_chemtherapy_goal',

                'group_chemtherapy_protocol' => array(
                  'field_chemtherapy_protocol_type',
                  'field_chemtherapy_protocol_med',
                  'field_chemtherapy_protocol_dose',
                ),

                'group_chemtherapy_dosered' => array(
                  'field_chemtherapy_dosered',
                  'field_chemtherapy_dosered_reason',
                ),

                'field_chemtherapy_cycles',
                'field_chemtherapy_bestresp',
                'field_chemtherapy_progdate',
              ),

              'group_moltherapy' => array(
                'field_moltherapy',
                'field_moltherapy_startdate',
                'field_moltherapy_enddate',
                'field_moltherapy_goal',

                'group_moltherapy_protocol' => array(
                  'field_moltherapy_protocol_type',
                  'field_moltherapy_protocol_med',
                  'field_moltherapy_protocol_dose',
                ),

                'group_moltherapy_dosered' => array(
                  'field_moltherapy_dosered',
                  'field_moltherapy_dosered_reason',
                ),

                'field_moltherapy_cycles',
                'field_moltherapy_bestresp',
                'field_moltherapy_progdate',
              ),
            ), // end oncotherapy adenocarcinoma

            'group_oncotherapy_neuroendocrine' => array(
              'group_ssatherapy' => array(
                'field_ssatherapy',
                'field_ssatherapy_startdate',
                'field_ssatherapy_enddate',
                'field_ssatherapy_goal',
                'field_ssatherapy_med',
                'field_ssatherapy_dose',

                'group_ssatherapy_dosered' => array(
                  'field_ssatherapy_dosered',
                  'field_ssatherapy_dosered_reason',
                ),

                'field_ssatherapy_cycles',
                'field_ssatherapy_bestresp',
                'field_ssatherapy_progdate',
              ),

              'group_inftherapy' => array(
                'field_inftherapy',
                'field_inftherapy_startdate',
                'field_inftherapy_enddate',
                'field_inftherapy_goal',
                'field_inftherapy_med',
                'field_inftherapy_dose',

                'group_inftherapy_dosered' => array(
                  'field_inftherapy_dosered',
                  'field_inftherapy_dosered_reason',
                ),

                'field_inftherapy_cycles',
                'field_inftherapy_bestresp',
                'field_inftherapy_progdate',
              ),

              'group_chemther_ne' => array(
                'field_chemther_ne',
                'field_chemther_ne_startdate',
                'field_chemther_ne_enddate',
                'field_chemther_ne_goal',

                'group_chemther_ne_protocol' => array(
                  'field_chemther_ne_type',
                  'field_chemther_ne_med',
                  'field_chemther_ne_dose',
                ),

                'group_chemther_ne_dosered' => array(
                  'field_chemther_ne_dosered',
                  'field_chemther_ne_dosered_reason',
                ),

                'field_chemther_ne_cycles',
                'field_chemther_ne_bestresp',
                'field_chemther_ne_progdate',
              ),

              'group_molther_ne' => array(
                'field_molther_ne',
                'field_molther_ne_startdate',
                'field_molther_ne_enddate',
                'field_molther_ne_goal',

                'group_molther_ne_protocol' => array(
                  'field_molther_ne_type',
                  'field_molther_ne_med',
                  'field_molther_ne_dose',
                ),

                'group_molther_ne_dosered' => array(
                  'field_molther_ne_dosered',
                  'field_molther_ne_dosered_reason',
                ),

                'field_molther_ne_cycles',
                'field_molther_ne_bestresp',
                'field_molther_ne_progdate',
              ),

              'group_peptherapy' => array(
                'field_peptherapy',
                'field_peptherapy_startdate',
                'field_peptherapy_enddate',
                'field_peptherapy_goal',

                'group_peptherapy_protocol' => array(
                  'field_peptherapy_type',
                  'field_peptherapy_med',
                  'field_peptherapy_dose',
                ),

                'group_peptherapy_dosered' => array(
                  'field_peptherapy_dosered',
                  'field_peptherapy_dosered_reason',
                ),

                'field_peptherapy_cycles',
                'field_peptherapy_bestresp',
                'field_peptherapy_progdate',
              ),

            ), // end oncotherapy neuroendocrine
          ), // end oncotherapy

          'group_support' => array(
            'group_support_painmanage' => array(
              'field_support_painmanage',
              'group_support_painmanage_multi' => array(
                'field_support_painmanage_med',
                'field_support_painmanage_dose',
                'field_support_painmanage_dur', // TODO js
              ),
            ),

            'group_support_pancenzsub' => array(
              'field_support_pancenzsub',
              'field_support_pancenzsub_dur2',
            ),

            'group_support_nutri' => array(
              'field_support_nutri',
              'field_support_nutri_dur2',
            ),
          ),

          'group_clinicaltrial_outer' => array(
            'group_clinicaltrial' => array(
              'field_clinicaltrial_name',
              'field_clinicaltrial_startdate',
              'field_clinicaltrial_enddate',
            ),
          ),

          'group_finalreport' => array(
            'field_finalreport',
          ),

          'field_notes',
          'field_tumor_info',

        ), // end page1
      ),
      'field_serial_register_pc',
    ), // end tree
  );

  return $bundle;
}

