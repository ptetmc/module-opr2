<?php

function opr2_cinc_bundle_prepast_form_a() {
  $bundle = array(
    'machine_name' => 'prepast_form_a',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => 'v1',
      ),
      'field_hospital' => array(
        'settings' => array(
          'list_filter' => array_keys(opr2_cinc_bundle_prepast_form_a_hospitals()),
        ),
      ),
    ),
    'groups' => array(
      'group_bmi' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'classes' => 'field_group_div group-inline-fields group-category',
          ),
        ),
      ),
      'group_lab_results' => array(
        'label' => 'Laboratóriumi vizsgálatok',
        'i18n' => array(
          'label' => 'Laboratory tests',
        ),
      ),
      'group_lab_results_req' => array(
        'label' => '',
        'format_settings' => array(
          'instance_settings' => array(
            'label_visibility' => 1,
          ),
        ),
      ),
    ),
    'tree' => array(
      'group_nopaging' => array(
        'group_nopage_1' => array(
          'group_opr_data' => array(
            'field_hospital',
            'field_opr_doctor_code',
            'field_serial_prepast',
          ),

          'group_personal' => array(
            'field_patient',
            'field_inpatient_start',
          ),

          'field_form_version',
          'field_imported',

          'group_screening' => array(
            'group_incl' => array(
              'group_incl_diagap' => array(
                'field_incl_diagap_abdpain',
                'field_incl_diagap_serumimag',
              ),
              'field_incl_biolorig',
              'field_incl_18plus',
              'field_incl_ercpable',
              'field_incl_recentsymptoms',
              'field_incl_consent',
            ),

            'group_excl' => array(
              'field_excl_pregn',
              'field_excl_alcpc',
              'field_excl_oldpain',
              'field_excl_livcirrh',
              'field_excl_inr',
              'field_excl_fluidnecr',
              'field_excl_sphinct',
            ),

            'group_acutechol' => array(
              'group_acutechol_sysinf' => array(
                'field_acutechol_fevershake',
                'field_acutechol_labinfl',
              ),
              'group_acutechol_cholest' => array(
                'field_acutechol_jaundice',
                'field_acutechol_lablft',
              ),
              'group_acutechol_imaging' => array(
                'field_acutechol_bildil',
                'field_acutechol_evidetil',
              ),
            ),

            'group_cholest' => array(
              'field_cholest_imaging',
              'field_cholest_lft',
            ),

            'group_randomization' => array(
              'field_rand_critmet',
              'field_rand_acutechol',
              'field_rand_cholest',
            ),
          ),

          'group_assessment' => array(
            'group_alcohol' => array(
              'field_alcohol_consumption',
              'field_alcohol_amount2',
            ),
            'group_medications' => array(
              'field_medi_azathiroprine',
              'field_medi_opioids',
              'field_medi_furosemid',
              'field_medi_enalapril',
              'field_medi_sulfasalzine',
              'field_medi_sulphametoxazole',
              'field_medi_tetracycline',
              'field_medi_steroids',
              'field_medi_oralcontracept',
            ),
            'group_cholecystectomy' => array(
              'field_cholecystectomy',
            ),
            'group_pasthist' => array(
              'field_gallstone',
              'field_biliarcolic',
              'field_pancdis',
            ),
            'group_asa' => array(
              'field_asa',
            ),
            'group_physex' => array(
              'group_admission_rows' => array(
                'group_admission_row1' => array(
                  'field_bp',
                  'field_bp_dias',
                  'field_pulse',
                ),
                'group_admission_row3' => array(
                  'field_resprate',
                  'field_bodytemp',
                ),
                'group_admission_row4' => array(
                  'field_abdomtender',
                  'field_abdomguard',
                ),
              ),
            ),
            'group_bmi' => array(
              'field_weight',
              'field_height',
              'field_bmi',
            ),
            'group_comorbidity' => array(
              'field_diabetes',
              'field_hypertension',
              'field_stroke',
              'group_heartfail' => array(
                'field_heartfail',
                'field_heartfail_severity',
              ),
              'field_renalfail',
              'field_hyperlipidaemia',
            ),
            'group_assess_abdus' => array(
              'group_assess_abdus_gallbladder' => array(
                'field_assess_abdus_gallstone',
              ),
              'group_assess_abdus_bileduct' => array(
                'field_assess_abdus_inhepbiledil',
                'field_assess_andus_cbd_dia',
                'field_assess_andus_cbd_stones',
              ),
              'group_assess_abdus_panc' => array(
                'field_assess_abdus_pancvis',
                'field_assess_abdus_pancoed',
                'field_assess_abdus_pancinh',
                'field_assess_abdus_pancenl',
                'field_assess_abdus_pancperifluid',
                'field_assess_abdus_pancasc',
              ),
            ),
            'group_ivfluids' => array(
              'field_ivfluid_beforercp',
              'field_therapadm_iv_24h',
              'field_ivfluid_type',
            ),
            'group_lab_results' => array(
              'group_lab_results_req' => array(
                'field_lab_result_wbc',
                'field_lab_result_hg',
                'field_lab_result_htoc',
                'field_lab_result_gluc',
                'field_lab_result_urean',
                'field_lab_result_creat',
                'field_lab_result_na',
                'field_lab_result_k',
                'field_lab_result_ca',

                'field_lab_result_bilitot',
                'field_lab_result_asatgot',
                'field_lab_result_alatgpt',
                'field_lab_result_ggt',
                'field_lab_result_alph',
                'field_lab_result_amilase',
                'field_lab_resut_lipase',
                'field_lab_result_crp2',
                'field_lab_result_procalc2',
              ),
            ),
            'group_assess_sirs' => array(
              'field_assess_sirs',
            ),
            'group_assess_bisap' => array(
              'field_bisap',
            ),
            'group_assess_resp' => array(
              'field_o2sat',
              'field_resprate',
            ),
            'group_assess_bloodgas' => array(
              'field_lab_result_pao2',
              'field_lab_result_ph',
            ),
            'group_assess_organfail' => array(
              'field_assess_marshallgt2',
            ),
          ),

          'group_ercp' => array(
            'field_ercp_doctor',
            'field_ercp_datetime',
            'field_ercp_hrsafteradm',
            'field_ercp_duration',
            'group_ercp_premed' => array(
              'field_ercp_premed',
              'field_ercp_premed_butylhyoscine',
              'field_ercp_premed_nalbuphin',
              'field_ercp_premed_midazolam',
              'field_ercp_premed_fentanyl',
              'field_ercp_premed_dolargan',
              'field_ercp_premed_other',
            ),
            'field_ercp_diffbil',
            'field_ercp_cbddia',
            'field_ercp_stonpap',
            'group_ercp_cbdston' => array(
              'field_ercp_cbdston',
              'field_ercp_cbdston_num',
              'field_ercp_cbdston_size',
            ),
            'field_ercp_cbdsludge',
            'field_ercp_papoedem',
            'field_ercp_duodoedem',
            'field_ercp_lacorif',
            'field_ercp_juxtapdiv',

            'field_ercp_pancgwman',
            'field_ercp_pancinj',
            'field_ercp_pancacin',
            'field_ercp_pancdiv',
            'group_ercp_pdstent' => array(
              'field_ercp_pdstent',
              'field_ercp_pdstent_type',
              'field_ercp_pdstent_outend',
            ),

            'group_ercp_bilest' => array(
              'field_ercp_bilest_convsphinct',
              'group_ercp_bilest_prepap' => array(
                'field_ercp_bilest_prepap',
                'field_ercp_bilest_prepap_nk',
                'field_ercp_bilest_prepap_lon',
                'field_ercp_bilest_prepap_fist',
              ),
            ),

            'group_ercp_addproc' => array(
              'field_ercp_addproc_nbdrain',
              'field_ercp_addproc_cbdst',
              'field_ercp_addproc_lithot',
              'field_ercp_addproc_baldil',
              'field_ercp_addproc_other',
            ),

            'field_ercp_diff_shutz',
            'field_ercp_diff_subj',

            'group_ercp_comp' => array(
              'field_ercp_comp_bleed',
              'field_ercp_comp_sev',

              'group_ercp_comp_et' => array(
                'group_ercp_comp_et_inj' => array(
                  'field_ercp_comp_et_inj',
                  'field_ercp_comp_et_inj_vol',
                ),
                'group_ercp_comp_et_other' => array(
                  'field_ercp_comp_et_other',
                  'field_ercp_comp_et_other_desc',
                ),
              ),

              'field_ercp_comp_perf',
            ),
          ), // group_ercp end
        ),
        'group_nopage_2' => array(
          'group_24h' => array(
            'field_24h_int',
            'group_24h_phys_rows' => array(
              'group_24h_phys_row1' => array(
                'field_24h_bp',
                'field_24h_bp_dias',
              ),
              'group_24h_phys_row2' => array(
                'field_24h_pulse',
                'field_24h_bodytemp',
              ),
            ),
            'group_24h_ab' => array(
              'field_24h_ab',
              'field_24h_ab_intent',
              'field_24h_ab_med',
              'field_24h_ab_ind',
            ),
            'group_24h_sirs' => array(
              'field_24h_sirs',
              'field_24h_sirs_res',
            ),
            'group_24h_orgf' => array(
              'field_24h_orgf',
              'field_24h_orgf_res',
            ),
            'field_24h_pain',
            'group_24h_glasgow' => array(
              'field_24h_glasgow',
            ),
          ), // end group_24h
        ),
        'group_nopage_3' => array(
          'group_48h' => array(
            'field_48h_int',
            'group_48h_nutr' => array(
              'field_48h_nutr_oral',
              'group_48h_nutr_ent' => array(
                'field_48h_nutr_ent',
                'field_48h_nutr_ent_nj',
                'field_48h_nutr_ent_ng',
              ),
              'group_48h_nutr_oth' => array(
                'field_48h_nutr_oth',
                'field_48h_nutr_oth_det',
              ),
            ),
            'group_48h_phys_rows' => array(
              'group_48h_phys_row1' => array(
                'field_48h_bp',
                'field_48h_bp_dias',
              ),
              'group_48h_phys_row2' => array(
                'field_48h_pulse',
                'field_48h_bodytemp',
              ),
              'group_48h_phys_row3' => array(
                'field_48h_resprate',
                'field_48h_bowsnd',
              ),
              'group_48h_phys_row4' => array(
                'field_48h_abdomtender',
                'field_48h_abdomguard',
              ),
            ),
            'group_48h_sirs' => array(
              'field_48h_sirs',
              'field_48h_sirs_res',
            ),
            'group_48h_orgf' => array(
              'field_48h_orgf',
              'field_48h_orgf_res',
            ),
          ), // end group_48h
        ),
        'group_nopage_4' => array(
          'group_72h' => array(
            'field_72h_int',
            'group_72h_sirs' => array(
              'field_72h_sirs',
              'field_72h_sirs_res',
            ),
            'group_72h_orgf' => array(
              'field_72h_orgf',
              'field_72h_orgf_res',
            ),
            'field_72h_pain',
            'group_72h_abdus' => array(
              'group_72h_abdus_gallbladder' => array(
                'field_72h_abdus_gallstone',
                'field_72h_abdus_pchcyfluid',
              ),
              'group_72h_abdus_bileduct' => array(
                'field_72h_abdus_inhepbiledil',
                'field_72h_abdus_cbd_dia',
                'field_72h_abdus_cbd_stones',
              ),
              'group_72h_abdus_panc' => array(
                'field_72h_abdus_pancvis',
                'field_72h_abdus_pancoed',
                'field_72h_abdus_pancinh',
                'field_72h_abdus_pancenl',
                'field_72h_abdus_pancperifluid',
                'field_72h_abdus_pancasc',
              ),
            ),
          ), // end group_72h
        ),
        'group_nopage_5' => array(
          'group_96h' => array(
            'group_96h_abdct' => array(
              'group_96h_abdct_cond' => array(
                'field_96h_abdct',
                'field_96h_abdct_when',
                'field_96h_abdct_balthazar',
                'field_96h_abdct_necro',
              ),
            ),
            'field_96h_loccomp',
            'field_96h_syscomp',
          ),
        ),
        'group_nopage_6' => array(
          'group_out' => array(
            'field_out_stay',
            'field_out_icustay',
            'group_out_orgfail' => array(
              'field_out_orgfail',
              'field_out_orgfail_mult',
              'field_out_orgfail_sev',
              'field_out_orgfail_req',
            ),
            'group_out_syscomp' => array(
              'field_out_syscomp',
              'field_out_syscomp_type',
            ),
            'group_out_loccomp' => array(
              'field_out_loccomp',
              'field_out_loccomp_type',
            ),
            'group_out_death' => array(
              'field_out_death',
              'field_out_death_date',
              'field_out_death_cause',
            ),
            'group_out_ab' => array(
              'field_out_ab',
              'field_out_ab_med',
            ),
            'field_out_jejfeed',
            'field_out_ctscore',
            'field_out_maxcrp',
            'group_out_surg' => array(
              'field_out_surg',
              'field_out_surg_type',
            ),
          ),
          'field_notes',
        ),
      ), // end paging

    ), // end tree
  );

  return $bundle;
}

function opr2_cinc_bundle_prepast_form_a_hospitals() {
  return array(
    624 => '01',
    642 => '02',
    649 => '03',
  );
}

function opr2_cinc_bundle_prepast_form_a_presave(&$node) {
  if (!empty($node->field_serial_prepast['und'][0]['value'])) {
    return;
  }

  $ok_hosp = FALSE;
  $part_hosp = 'XX';

  if (!empty($node->field_hospital['und'][0]['target_id'])) {
    $hosp_id = $node->field_hospital['und'][0]['target_id'];
    $hospitals = opr2_cinc_bundle_prepast_form_a_hospitals();
    if (isset($hospitals[$hosp_id])) {
      $part_hosp = $hospitals[$hosp_id];
      $ok_hosp = TRUE;
    }
    else {
      $part_hosp = 'EE';
    }
  }

  $ok_group = FALSE;
  $part_group = 'C';
  if (empty($node->field_rand_acutechol['und'][0]['value'])) {
    if (!empty($node->field_rand_cholest['und'][0]['value'])) {
      $part_group = 'B';
      $ok_group = TRUE;
    }
  }
  else {
    $part_group = 'A';
    $ok_group = TRUE;
  }

  if ($ok_group && $ok_hosp) {
    $prefix = "$part_hosp-$part_group%";
  }
  else {
    $prefix = 'NI%';
  }

  $prev_ids = db_select('field_data_field_serial_prepast', 'rid')
    ->fields('rid', array('field_serial_prepast_value'))
    ->condition('bundle', 'prepast_form_a')
    ->condition('field_serial_prepast_value', $prefix, 'LIKE')
    ->execute()
    ->fetchCol();

  $max_serial = 0;
  foreach ($prev_ids as $pid) {
    $prev_serial = intval(substr($pid, 5));
    if ($prev_serial > $max_serial) {
      $max_serial = $prev_serial;
    }
  }

  $part_serial = sprintf('%03d', $max_serial+1);


  dpm(array($part_hosp,$part_group,$part_serial), 'presaveprepast');

  if ($ok_group && $ok_hosp) {
    $serial = "$part_hosp-$part_group-$part_serial";
  }
  else {
    $serial = "NI-$part_serial";
  }
  $node->field_serial_prepast = array(
    'und' => array(
      array(
        'value' => $serial,
      ),
    ),
  );
}
