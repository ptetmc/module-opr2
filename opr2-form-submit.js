(function ($) {
Drupal.behaviors.Opr2FormSubmit = {
  attach: function(context) {
    var debug = false;

    if (debug) console.log('attach', context);

    $('form.node-form', context).unbind('submit').bind('submit', function(e){
      var $form = $(this), $allSubmits = $form.find('.form-actions input.form-submit');
      if (debug) console.log('submit', $form.data('submitted'), $form.data('submitButton'));

      if ($form.data('submitted')) return false;

      // if no form-actions buttons were clicked, it was probably ajax => we're OK
      if (!$form.data('submitButton')) return true;

      $form.data('submitted', true);

      // NOT disabling buttons because op data would be missing
      // $allSubmits.attr('disabled', 'disabled');

      $form.find('.form-actions').append('&nbsp;<i class="fa fa-spinner fa-pulse"></i>');
      if (debug) {
        console.log('letting submit go');
      }
    });

    $('form.node-form .form-actions input[type=submit]', context).unbind('click').bind('click', function() {
      var $submit = $(this), $form = $submit.parents('form');
      $form.data('submitButton', $submit.attr('id'));
    });
  }
};
})(jQuery);


