(function ($) {
Drupal.behaviors.Opr2FormNotes = {
  attach: function(context) {
    $('div.opr-forms-container .opr-qa-reason').click(function(){
      var $a = $(this),
        modalHtml = '';

      if (!$a.attr('title').length) return;
      modalHtml += '<div class="messages status"><pre>'+
        $a.attr('title')+
        '</pre></div>';

      Drupal.CTools.Modal.show('opr2-note-modal-style');
      $('#modal-title').html(Drupal.t('Form notes for @form', {'@form':$a.closest('tr').find('td:first-child').text()}));
      $('#modal-content').html(modalHtml);
      return false;
    });
  }
};
})(jQuery);
