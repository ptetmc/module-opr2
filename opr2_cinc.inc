<?php

function opr2_cinc_update_bundle($bundle, $settings = array()) {
  module_load_include('inc', 'opr2', 'opr2_o2l');

  $bundle_callback = "opr2_cinc_bundle_$bundle";
  module_load_include('inc', 'opr2', $bundle_callback);
  if (!function_exists($bundle_callback)) {
    drupal_set_message("Could not load bundle config ($bundle)", 'error');
    return false;
  }

  $bundle_config = call_user_func($bundle_callback);
  if (empty($settings['target_bundle'])) {
    $settings['target_bundle'] = $bundle;
  }

  ctools_include('export');
  module_load_include('inc', 'opr2', 'opr2_cinc_field');
  module_load_include('inc', 'opr2', 'opr2_cinc_field_group');
  if (!empty($settings['translate']) || !empty($settings['translate_only'])) {
    module_load_include('inc', 'opr2', 'opr2_cinc_i18n');
  }

  $instances = field_info_instances('node', $settings['target_bundle']);
  $groups = field_group_info_groups('node', $settings['target_bundle'], 'form');

  $field_configs = array();
  if (!empty($bundle_config['fields'])) foreach ($bundle_config['fields'] as $config) {
    $field_configs[$config['field_name']] = $config;
  }

  $instance_configs = array();
  if (!empty($bundle_config['instances'])) foreach ($bundle_config['instances'] as $field_name => $config) {
    $config['field_name'] = $field_name;
    $instance_configs[$field_name] = $config;
  }

  $group_configs = array();
  if (!empty($bundle_config['groups'])) foreach ($bundle_config['groups'] as $group_name => $config) {
    $group_configs[$group_name] = $config;
  }

  $context = array_merge($settings, array(
    'instances' => $instances,
    'groups' => $groups,
    'field_configs' => $field_configs,
    'instance_configs' => $instance_configs,
    'group_configs' => $group_configs,
    'bundle' => $bundle,
  ));

  _o2l($bundle_config, 'update bunlde config');

  _opr2_cinc_process_bundle_tree($bundle_config['tree'], $context);
}

function _opr2_cinc_process_bundle_tree($tree, &$context = array(), $path = array()) {
  $weight = 10;

  $valid_parents = array();
  foreach ($tree as $parent => $children) {
    $do_translate = FALSE;
    if (is_numeric($parent)) { // FIELD
      if (!is_string($children)) {
        _o2l($children, 'wrong field spec');
        continue;
      }

      $field_name = $children;

      $do_update = _opr2_cinc_is_updatable($field_name, $context, $path);

      // process FIELD
      if (isset($context['field_configs'][$field_name])) {
        $field_config = $context['field_configs'][$field_name];
      } else {
        $field_config = opr2_cinc_field($field_name);
      }

      if (isset($field_config['instance'])) {
        $instance_config = $field_config['instance'];
        unset($field_config['instance']);

      } else {
        $instance_config = array();
      }

      $field_info = field_info_field($field_name);
      if (empty($field_config) && empty($field_info)) {
        drupal_set_message("Missing field: $field_name", 'warning');
        continue;
      }

      if ($do_update) {
        if (!empty($context['translate'])) {
          $do_translate = TRUE;
        }
        if (!empty($context['translate_only'])) {
          $do_translate = TRUE;
          $do_update = FALSE;
        }
      }

      if ($field_info && !empty($context['skip_existing'])) {
        $do_update = FALSE;
      }

      $field_config['field_name'] = $field_name;

      if ($do_update) {
        _o2l($field_config, 'updating field '.$field_name);
        if ($field_info) {
          if (!empty($field_config)) {
            try {
              field_update_field($field_config);
            } catch (FieldException $e) {
              drupal_set_message("Could not update field '$field_name': ".$e->getMessage(), 'warning');
            }
          }

        } else {
          field_create_field($field_config);
        }
      }
      if ($do_translate) {
        $translation = opr2_cinc_i18n($field_name, $context['bundle']);
        _o2l($translation, "field translation for $field_name");
        if (is_string($translation)) {
          $translation = array('label' => $translation);
        }
        if (!is_array($translation)) {
          $translation = array();
        }
        if (!empty($field_config['i18n']) && is_array($field_config['i18n'])) {
          $translation = $field_config['i18n'] + $translation;
        }
        if (FALSE) foreach (array('label') as $prop) { // TODO src?
          if (!empty($translation[$prop])) {
            $i18n_path = array('field', $field_name, "#$prop");
            i18n_string_update($i18n_path, $field_config[$prop]);
            i18n_string_translation_update($i18n_path, $translation[$prop], 'en', $field_config[$prop]);
            _o2l(array($i18n_path, $translation[$prop]), "translated $field_name #$prop");
          }
        }
        if (!empty($translation['allowed_values'])) foreach ($translation['allowed_values'] as $key => $label) {
          if (empty($field_config['settings']['allowed_values'][$key])) continue;
          $i18n_path = array('field', $field_name, '#allowed_values', $key);
          i18n_string_update($i18n_path, $field_config['settings']['allowed_values'][$key]);
          i18n_string_translation_update($i18n_path, $label, 'en', $field_config['settings']['allowed_values'][$key]);
          _o2l(array($i18n_path, $label), "translated $field_name option $key");
        }
      }

      // process FIELD INSTANCE
      if (isset($context['instance_configs'][$field_name])) {
        $instance_config = array_extend($instance_config, $context['instance_configs'][$field_name]);
      }

      if (!empty($instance_config['i18n']) && is_array($instance_config['i18n'])) {
        if (empty($translation)) $translation = array();
        $translation = $instance_config['i18n'] + $translation;
      }

      $instance_config['entity_type'] = 'node';
      $instance_config['bundle'] = $context['target_bundle'];
      $instance_config['field_name'] = $field_name;
      $instance_config['widget']['weight'] = $weight;

      if (isset($context['instances'][$field_name]) && !empty($context['skip_existing'])) {
        $do_update = FALSE;
      }

      if ($do_update) {
        _o2l($instance_config, 'updating field instance '.$field_name);
        if (isset($context['instances'][$field_name])) {
          field_update_instance($instance_config);

        } else {
          field_create_instance($instance_config);
        }
      }
      if ($do_translate && !empty($translation)) {
        foreach (array('label', 'description', 'suffix') as $prop) {
          if (!empty($translation[$prop])) {
            if (isset($instance_config[$prop])) {
              $src = $instance_config[$prop];
            } else if (isset($instance_config['settings'][$prop])) {
              $src = $instance_config['settings'][$prop];
            } else {
              _o2l($prop, "missing translation prop for $field_name");
              continue;
            }
            $i18n_path = array('field', $field_name, $context['bundle'], $prop);
            i18n_string_update($i18n_path, $src);
            i18n_string_translation_update($i18n_path, $translation[$prop], 'en', $src);
            _o2l(array($i18n_path, $translation[$prop]), "translated $field_name instance $prop");
          }
        }
      }

      $valid_parents[] = $field_name;

    } else { // FIELD GROUP
      $group_name = $parent;

      $do_update = _opr2_cinc_is_updatable($group_name, $context, $path);

      $group_config = opr2_cinc_field_group($group_name);

      if (isset($context['group_configs'][$group_name])) {
        $group_config = array_extend($group_config, $context['group_configs'][$group_name]);
      }

      if (empty($group_config)) {
        drupal_set_message("Missing group config: $group_name", 'warning');
        continue;
      }

      // setting group_name class
      if (!isset($group_config['format_settings']['instance_settings']['classes'])) {
        $group_config['format_settings']['instance_settings']['classes'] = '';
      }
      $group_config['format_settings']['instance_settings']['classes'] .= ' '.$group_name;

      $child_path = $path;
      $child_path[] = $group_name;
      $group_config['children'] = _opr2_cinc_process_bundle_tree($children, $context, $child_path);
      if (!empty($path)) {
        $group_config['parent_name'] = end($path);
      }

      $group_config['identifier'] = "$group_name|node|{$context['target_bundle']}|form";
      $group_config['group_name'] = $group_name;
      $group_config['entity_type'] = 'node';
      $group_config['bundle'] = $context['target_bundle'];
      $group_config['mode'] = 'form';
      $group_config['weight'] = $weight;

      if (!$do_update && !empty($group_config['children'])) {
        foreach ($group_config['children'] as $child) {
          if (_opr2_cinc_is_updatable($child, $context, $path)) {
            $do_update = true;
            break;
          }
        }
      }
      if (!$do_update && !empty($context['groups'][$group_name]->children)) {
        foreach ($context['groups'][$group_name]->children as $child) {
          if (_opr2_cinc_is_updatable($child, $context, $path)) {
            $do_update = true;
            break;
          }
        }
      }

      if ($do_update) {
        if (!empty($context['translate'])) {
          $do_translate = TRUE;
        }
        if (!empty($context['translate_only'])) {
          $do_translate = TRUE;
          $do_update = FALSE;
        }
      }

      $group_obj = (object) $group_config;

      if (isset($context['groups'][$group_name])) {
        if (!empty($context['skip_existing'])) {
          $do_update = false;
        }
        $group_obj->export_type = EXPORT_IN_DATABASE;
        $group_obj->id = $context['groups'][$group_name]->id;
      }

      if ($do_update) {
        _o2l($group_obj, 'updating field group '.$group_name);
        field_group_group_save($group_obj);
      }
      if ($do_translate) {
        $translation = opr2_cinc_i18n($group_name, $context['bundle']);

        if (empty($translation)) {
          $translation = array();
        }
        else if (is_string($translation)) {
          $translation = array('label' => $translation);
        }

        if (!empty($group_config['i18n']) && is_array($group_config['i18n'])) {
          $translation = $group_config['i18n'] + $translation;
        }

        if (!empty($translation['label'])) {
          $i18n_path = array('field_group', 'node', $context['bundle'], 'form', $group_name, 'label' );
          i18n_string_update($i18n_path, $group_config['label']);
          i18n_string_translation_update($i18n_path, $translation['label'], 'en', $group_config['label']);
          _o2l(array($i18n_path, $translation['label']), "translated $group_name label");
        }
      }
      $valid_parents[] = $group_name;
    }

    $weight += 2;
  }

  return $valid_parents;
}

function array_extend() {
  $arrays = func_get_args();
  $base = array_shift($arrays);
  foreach ($arrays as $array) {
    reset($base);
    while (list($key, $value) = @each($array))
      if (is_array($value) && @is_array($base[$key]))
        $base[$key] = array_extend($base[$key], $value);
      else $base[$key] = $value;
  }
  return $base;
}

function _opr2_cinc_is_updatable($item_name, &$context, $path = array()) {
  $has_filter = false;

  if (!empty($context['update_only'])) {
    if (in_array($item_name, $context['update_only'])) return true;

    $has_filter = true;
  }

  if (!empty($context['groups_only'])) {
    if (preg_match('/^field_/', $item_name)) return false;
  }

  if (!empty($context['update_filter'])) {
    $filters = is_array($context['update_filter']) ? $context['update_filter'] : array($context['update_filter']);
    foreach ($filters as $filter) if (preg_match($filter, $item_name)) {
      return true;
    }

    $has_filter = true;
  }

  if (!empty($context['update_group'])) {
    $path[] = $item_name;
    $update_group = (array) $context['update_group'];
    if (count(array_intersect($update_group, $path))) {
      return true;
    }

    $has_filter = true;
  }

  // deny if there was any unsuccesful filtering
  return !$has_filter;
}
