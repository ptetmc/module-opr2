<?php

function _opr2_form_set_defaults(&$form, $node) {
//  dpm($form, 'def form');
  foreach (element_children($form) as $child) {
    if (substr($child, 0, 6) != 'field_') continue;
    if (substr($child, 0, 9) == 'field_qa_') continue;
    $printdebug = FALSE && preg_match('/therapy_int/', $child);
    if ($printdebug) dpm($form[$child], 'def printdeb '.$child);
    if (empty($form[$child]['#language'])) continue;
    $lang = $form[$child]['#language'];

    $child_from = $child;
    if (!isset($node->$child_from)) {
      $child_from = _opr2_form_translate_defaults($child, $node->type, $form['#bundle']);
      if ($child_from != $child) {
        if (!isset($node->$child_from)) {
          if ($printdebug) dpm($child_from, "def $child translated missing");
          continue;
        }
        else if ($printdebug) {
          dpm($child_from, "def $child translate");
        }
      }
    }

    if (!empty($node->$child_from)) {
      $node_field = $node->$child_from;
      if ($printdebug) {
        dpm($child_from, "def $child from $child_from");
      }
    }
    else {
      $node_field = _opr2_form_generate_defaults($child, $node, $form['#bundle']);
      if (!empty($node_field)) {
        dpm($node_field, "def $child generated");
      }
    }

    if ($printdebug) dpm($node_field, "def $child node field");
    if (empty($node_field) || !isset($node_field[$lang])) {
      continue;
    }

    if (isset($form[$child][$lang]['#type'])) {
      $type = $form[$child][$lang]['#type'];
    }
    else if (isset($form[$child][$lang][0]['#type'])) {
      $type = $form[$child][$lang][0]['#type'];
    }
    else {
      $type = null;
    }

//    $field_info = isset($form[$child][$lang]['#field_name']) ? field_info_field($form[$child][$lang]['#field_name']) : null;
//    $field_type = isset($field_info['type']) ? $field_info['type'] : null;
//    dpm("$type - $field_type", "def $child #type");
    switch ($type) {
    case 'radios':
    case 'select':
    case 'checkboxes':
      $defaults = array();
      foreach ($node_field[$lang] as $delta => $item) {
        if (isset($item['value'])) {
          // correct for numeric & string value keys
          if (is_numeric($item['value'])) foreach (array_keys($form[$child][$lang]['#options']) as $option_value) {
            if (is_numeric($option_value) && intval($item['value']) == intval($option_value)) {
              if ($printdebug) dpm(var_export($item['value'], true).' => '.var_export($option_value, true), "def $child value mod");
              $item['value'] = $option_value;
              break;
            }
          }
          $defaults[] = $item['value'];
        }
      }

      if (!empty($defaults)) {
        if (count($defaults) == 1) {
          $defaults = array_pop($defaults);
        }

        $form[$child][$lang]['#default_value'] = $defaults;
        $form[$child][$lang]['#value'] = $defaults;

        foreach (array_keys($form[$child][$lang]['#options']) as $option_value) {
          if (empty($form[$child][$lang][$option_value])) continue;
          if ((is_array($defaults) && in_array($option_value, $defaults)) ||
              ($defaults == $option_value)) {
            $option_selected = $option_value;
          }
          else {
            $option_selected = FALSE;
          }

          $form[$child][$lang][$option_value]['#value'] = $option_selected;
          $form[$child][$lang][$option_value]['#default_value'] = $option_selected;
          $form[$child][$lang][$option_value]['#checked'] = !($option_selected === FALSE);
        }
      }
      break;

    case 'checkbox':
      if (isset($node_field[$lang][0]['value'])) {
        $form[$child][$lang]['#default_value'] = $node_field[$lang][0]['value'];
        $form[$child][$lang]['#value'] = $node_field[$lang][0]['value'];
        $form[$child][$lang]['#checked'] = $node_field[$lang][0]['value'] == $form[$child][$lang]['#on_value'];
      }
      break;

    case 'date_combo':
      if (isset($node_field[$lang][0]['value'])) {
        $form[$child][$lang][0]['value']['#default_value'] = $node_field[$lang][0]['value'];
        $form[$child][$lang][0]['value']['#value'] = $node_field[$lang][0]['value'];

        $d = new DateTime($node_field[$lang][0]['value']);
        $form[$child][$lang][0]['value']['year']['#value'] = $d->format('Y');
        $form[$child][$lang][0]['value']['year']['#default_value'] = $form[$child][$lang][0]['value']['year']['#value'];
        $form[$child][$lang][0]['value']['month']['#value'] = $d->format('m');
        $form[$child][$lang][0]['value']['month']['#default_value'] = $form[$child][$lang][0]['value']['month']['#value'];
        $form[$child][$lang][0]['value']['day']['#value'] = $d->format('d');
        $form[$child][$lang][0]['value']['day']['#default_value'] = $form[$child][$lang][0]['value']['day']['#value'];
      }
      break;

    case 'select_or_other':
      if (isset($node_field[$lang][0]['value'])) {
        $val = $node_field[$lang][0]['value'];

        if (isset($form[$child][$lang]['#options'][$val])) {
          $form[$child][$lang]['select']['#default_value'] = $val;
          $form[$child][$lang]['select']['#value'] = $val;
        }
        else {
          $form[$child][$lang]['select']['#default_value'] = 'select_or_other';
          $form[$child][$lang]['select']['#value'] = 'select_or_other';
          $form[$child][$lang]['select']['select_or_other']['#default_value'] = 'select_or_other';
          $form[$child][$lang]['select']['select_or_other']['#value'] = 'select_or_other';
          $form[$child][$lang]['select']['select_or_other']['#checked'] = TRUE;
          $form[$child][$lang]['other']['#default_value'] = $val;
          $form[$child][$lang]['other']['#value'] = $val;
        }
      }
      break;

    case 'medical_dosage_widget':
      $field_info = field_info_field($child);
      $field_instance = field_info_instance('node', $child, $form['#bundle']);
      $_fake_element = array(
        '#id' => '_fake_id',
        '#title' => '_fake_title',
      );
      foreach (element_children($form[$child]['und']) as $delta) {
        if (empty($node_field[$lang][$delta])) continue;

        $item_element = $form[$child]['und'][$delta];
        $defaults_widget = medical_fields_field_widget_form($form, $form_state, $field_info, $field_instance, 'und', $node_field[$lang], $delta, $_fake_element);

        foreach (element_children($defaults_widget) as $input_name) {
          if (isset($defaults_widget[$input_name]['#default_value'])) {
            $form[$child]['und'][$delta][$input_name]['#default_value'] = $defaults_widget[$input_name]['#default_value'];
            $form[$child]['und'][$delta][$input_name]['#value'] = $defaults_widget[$input_name]['#default_value'];
          }
        }
      }
      break;

    default:
      $field_info = field_info_field($child);
      if (is_array($node_field[$lang])) {
        foreach ($node_field[$lang] as $delta => $item) {

        if ($printdebug)  dpm($item, "def $child delta $delta type $type item");
          if (is_array($item)) {
            foreach (array_intersect(array_keys($item), array('value', 'target_id')) as $column) {
              if (isset($form[$child][$lang][$delta][$column])) {
                if (isset($form[$child]['#field_group_multiple'])) {
                  $input = &$form["fgm_node_{$form['#bundle']}_form_{$form[$child]['#field_group_multiple']}"]['fields']['items'][$delta][$child][$lang][$column];
//                  dpm($form["fgm_node_{$form['#bundle']}_form_{$form[$child]['#field_group_multiple']}"], "def $child fgm child stuff");
                } else {
                  $input = &$form[$child][$lang][$delta][$column];
                }

                $value = $item[$column];

                switch ($field_info['type']) {
                case 'number_decimal':
                  $value = str_replace('.', $field_info['settings']['decimal_separator'], $value);
                  break;
                }
                $input['#default_value'] = $value;
                $input['#value'] = $value;
              }
            }

          } else if (isset($form[$child][$lang][$delta]['#default_value'])) {
            $form[$child][$lang][$delta]['#default_value'] = $item;
            $form[$child][$lang][$delta]['#value'] = $item;
          }
        }

      } else if (isset($form[$child][$lang]['#default_value'])) {
        $form[$child][$lang]['#default_value'] = $node_field[$lang];
      }
    }
    if ($printdebug) dpm($form[$child], "def $child form child");
  }
}

function _opr2_form_translate_defaults($child, $bundle_from, $bundle_to) {
  switch ($child) {
  case 'field_stoma_since':
    return 'field_abdpain_since2';
  case 'field_abdpain_since2':
    return 'field_stoma_since';

  case 'field_therapy_ab_detail2':
    return 'field_therapy_ab_detail';
  case 'field_therapy_pm_detail2':
    return 'field_therapy_pm_detail';
  case 'field_therapy_ab_detail':
    return 'field_therapy_ab_detail2';
  case 'field_therapy_pm_detail':
    return 'field_therapy_pm_detail2';

  case 'field_therapy_intensive_desc':
    return 'field_therapy_intensive_desc2';
  case 'field_therapy_intensive_desc2':
    return 'field_therapy_intensive_desc';
  }

  return $child;
}

function _opr2_form_generate_defaults($child, $node_from, $bundle_to) {
  switch ($child) {
  case 'field_interview_date':
    if (!empty($node_from->field_inpatient['und'][0]['target_id'])) {
      $ip = node_load($node_from->field_inpatient['und'][0]['target_id']);

      if (!empty($ip) && !empty($ip->field_inpatient_start)) {
        return $ip->field_inpatient_start;
      }
    }
    break;

  case 'field_therapy_ins_med':
    if (!empty($node_from->field_therapy_ins_dosage2['und'])) {
      $items = array();
      foreach ($node_from->field_therapy_ins_dosage2['und'] as $delta => $item) {
        $item_out = array();
        if (!empty($item['what'])) {
          $item_out['value'] = $item['what'];
          $items[] = $item_out;
        }
      }
      if (!empty($items)) {
        return array('und'=>$items);
      }
    }
    break;

  case 'field_therapy_ins_dose':
    if (!empty($node_from->field_therapy_ins_dosage2['und'])) {
      $items = array();
      foreach ($node_from->field_therapy_ins_dosage2['und'] as $delta => $item) {
        $item_out = array();
        if (!empty($item['dosage'])) {
          $dose_out = array();
          $dosage = json_decode($item['dosage'], TRUE);
          foreach ($dosage as $time => $dose) {
            $dose_out[] = "$time: $dose";
          }

          if (!empty($dose_out)) {
            $item_out['value'] = implode(', ', $dose_out);
            if (strlen($item_out['value']) > 31) {
              $item_out['value'] = substr($item_out['value'], 0, 28).'...';
            }
            $items[] = $item_out;
          }
        }
      }
      if (!empty($items)) {
        return array('und'=>$items);
      }
    }
    break;

  case 'field_therapy_ins_dosage':
  case 'field_therapy_ins_dosage2':
    if (!empty($node_from->field_therapy_ins_med['und'])) {
      $items = array();
      foreach ($node_from->field_therapy_ins_med['und'] as $delta => $item) {
        $item_out = array();
        if (!empty($item['value'])) {
          $item_out['what'] = $item['value'];

          if (!empty($node_from->field_therapy_ins_dose['und'][$delta]['value'])) {
            $item_out['what'] .= ' '.$node_from->field_therapy_ins_dose['und'][$delta]['value'];
          }
        }

        if (!empty($item_out)) {
          $items[] = $item_out;
        }
      }
      if (!empty($items)) {
        return array('und'=>$items);
      }
    }
    break;
  }

  return null;
}

