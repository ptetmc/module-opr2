(function ($) {
Drupal.behaviors.Opr2Therapint = {
  attach: function(context) {
    var $main_radios = $('form.node-form input[name="field_therapyint[und]"]'),
        $result_radios = $('input[name^="field_therapint_"][type=radio]').not(':disabled');
    if ($main_radios.length
      && !$main_radios.filter(':checked').length
      && $result_radios.filter('[value="1"]:checked').length) {
      $main_radios.filter('[value="1"]').attr('checked', true);
      $main_radios.filter('[value="0"]').attr('disabled', true);
    }
    $main_radios.change(function(){
      var $this = $(this),
        value = $main_radios.filter(':checked').val();

      if (value === '1') {
        $result_radios.attr('checked', false).change();

      } else if (value === '0') {
        $result_radios.attr('checked', false).filter('[value="0"]').attr('checked', true);
        $result_radios.change();
      }
    });
  }
};
})(jQuery);

