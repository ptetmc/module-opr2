<?php

function opr2_page_stats($type, $id = null) { // @todo caching!
  $stats = _opr2_page_stat_generate($type, $id);
  $render = array();

  switch ($type) {
  case 'study':
    uasort($stats, '_opr2_stat_sort_by_forms');

    $render['table'] = array(
      '#theme' => 'table',
      '#header' => array(
        t('Country'),
        t('Centre'),
        t('Forms'),
      ),
      '#empty' => t('Could not find matching forms.'),
      '#sticky' => FALSE,
      '#attributes' => array(
        'class' => array('table', 'stat', 'table-striped', 'table-hover', 'table-responsive'),
      ),
    );

    foreach ($stats as $iso2 => $country_data) {
      $render['table']['#rows'][] = array(
        'class' => array('stat-country', 'country-'.$iso2),
        'data' => array(
          $country_data['name'],
          '',
          $country_data['forms'],
        ),
      );

      uasort($country_data['centers'], '_opr2_stat_sort_by_forms');
      foreach ($country_data['centers'] as $center_id => $center_data) {
        $render['table']['#rows'][] = array(
          'class' => array('stat-center'),
          'data' => array(
            '',
            $center_data['name'],
            $center_data['forms'],
          ),
        );
      }
    }
    break;

  case 'counts':
    $render['table'] = array(
      '#theme' => 'table',
      '#header' => array(
        '',
        t('General Registry'),
        t('Biobank'),
      ),
      '#empty' => t('Could not find matching forms.'),
      '#sticky' => FALSE,
      '#attributes' => array(
        'class' => array('table', 'stat', 'table-striped', 'table-hover', 'table-responsive'),
      ),
    );
    $stats = array(
      '_sum' => array(
        'formcount'  => 0,
        'bloodcount' => 0,
      ),
    );
    foreach (opr2_studies() as $study => $study_config) {
      $stats[$study] = _opr2_stat_fetch_study_formcount($study);
      $study_title = isset($study_config['title_long'])  ? $study_config['title_long'] : $study;
      $formcount  = empty($stats[$study]['formcount'])   ? 0 : $stats[$study]['formcount'];
      $bloodcount = empty($stats[$study]['bloodcount'])  ? 0 : $stats[$study]['bloodcount'];

      $render['table']['#rows'][] = array(
        'class' => array('stat-study'),
        'data' => array(
          '<span class="opr-study-label opr-study-label-button opr-study-'.$study.'">'.$study_title.'</span>',
          _opr2_stat_number_format($formcount),
          _opr2_stat_number_format($bloodcount),
        ),
      );

      $stats['_sum']['formcount'] += $formcount;
      $stats['_sum']['bloodcount'] += $bloodcount;
    }

    $render['table']['#rows'][] = array(
      'class' => array('stat-sum'),
      'data' => array(
        '',
        _opr2_stat_number_format($stats['_sum']['formcount']),
        _opr2_stat_number_format($stats['_sum']['bloodcount']),
      ),
    );

    break;

  case 'general':
    $render['table'] = array(
      '#theme' => 'table',
      '#empty' => t('Could not fetch statistics.'),
      '#sticky' => FALSE,
      '#attributes' => array(
        'class' => array('table', 'stat', 'table-striped', 'table-hover', 'table-responsive'),
      ),
    );

    $stats = _opr2_stat_fetch_general();

    $print = array(
      'patients' => t('Patients'),
      'samples' => t('Samples'),
      'countries' => t('Countries'),
      'centers' => t('Centers'),
      'doctors' => t('Doctors'),
      'registries' => t('Registries'),
      'studies' => t('Trials'),
    );

    $fa_icons = array(
      'patients' => 'fa-bed',
      'samples' => 'fa-flask',
      'countries' => 'fa-globe',
      'centers' => 'fa-map-marker',
      'doctors' => 'fa-user-md',
      'registries' => 'fa-database',
      'studies' => 'fa-bullseye',
    );

    foreach ($print as $name => $label) {
      if (isset($fa_icons[$name])) {
        $label = '<i class="fa fa-fw fa-lg ' . $fa_icons[$name] . '"></i>&nbsp;' . $label;
      }
      else {
        $label = '<i class="fa fa-fw fa-lg">&nbsp;</i>&nbsp;' . $label;
      }
      if (!isset($stats[$name])) {
        $render['table']['#rows'][] = array(
          'class' => array('stat-na'),
          'data' => array(
            array(
              'class' => array('stat-col-label'),
              'data' => $label,
            ),
            array(
              'class' => array('stat-col-count'),
              'data' => t('N/A'),
            ),
          ),
        );
      }
      else {
        $render['table']['#rows'][] = array(
          'class' => array('stat-general'),
          'data' => array(
            array(
              'class' => array('stat-col-label'),
              'data' => $label,
            ),
            array(
              'class' => array('stat-col-count'),
              'data' => _opr2_stat_number_format($stats[$name]),
            ),
          ),
        );
      }
    }
    break;
  }

  if (isset($_GET['json'])) {
    echo drupal_json_output(array(
      'html' => drupal_render($render),
      'data' => $stats,
    ));
    return;
  }

  return $render;
}

function _opr2_page_stat_generate($type, $id) {
  $stats = array();

  switch ($type) {
  case 'study':
    $stat_res = _opr2_stat_fetch_study_centers($id);

    $hosps = node_load_multiple(array_keys($stat_res));
    foreach ($stat_res as $hosp_id => $formcount) {
      $hospw = entity_metadata_wrapper('node', $hosps[$hosp_id]);
      $iso2 = $hospw->field_country->value()->iso2;

      if (!isset($stats[$iso2])) {
        $stats[$iso2] = array(
          'name' => $hospw->field_country->label(),
          'forms' => 0,
        );
      }

      $stats[$iso2]['forms'] += $formcount;
      $stats[$iso2]['centers'][$hosp_id] = array(
        'name' => $hospw->label(),
        'forms' => $formcount,
      );
    }
    break;

  case 'country':
    break;

  case 'center':
    break;
  }


  return $stats;
}

function _opr2_stat_sort_by_forms($a, $b) {
  return intval($a['forms']) < intval($b['forms']);
}

function _opr2_stat_fetch_study_centers($study) {
  if (empty($study)) {
    $study_configs = opr2_config('study');
  }
  else {
    $study_config = opr2_config('study', $study);
    if (empty($study_config)) return null;

    $study_configs = array($study => $study_config);
  }

  $results = array();
  foreach ($study_configs as $study => $study_config) {
    $study_types = array();
    foreach (opr2_config('form', array('study'=>$study)) as $type => $opr2_form) {
      if (in_array($opr2_form['type'], array('admission', 'outpatient'))) {
        $study_types[] = $type;
      }
    }

    if (empty($study_types)) {
      continue;
    }

    $stat_query = db_select('field_data_field_hospital', 'fhosp')
      ->fields('fhosp', array('field_hospital_target_id'));

    if ($study_config['inpatient']) {
      $stat_query->condition('fhosp.bundle', 'inpatient');
      $stat_query->leftJoin('field_data_field_inpatient', 'finp', 'finp.field_inpatient_target_id = fhosp.entity_id');
      $stat_query->condition('finp.bundle', $study_types, 'IN');
    }
    else {
      $stat_query->condition('fhosp.bundle', $study_types, 'IN');
    }

    $stat_query->leftJoin('node', 'nform', 'nform.nid = fhosp.entity_id');
    $stat_query->condition('nform.status', 1);

    $stat_query->leftJoin('node', 'nhosp', 'nhosp.nid = fhosp.entity_id');
    $stat_query->condition('nhosp.status', 1);

    $stat_query->groupBy('fhosp.field_hospital_target_id');
    $stat_query->addExpression('COUNT(fhosp.entity_id)', 'formcount');

    $stat_res = $stat_query->execute()->fetchAllKeyed();

    if (empty($results)) {
      $results = $stat_res;
    }
    else {
      foreach ($stat_res as $hosp_id => $formcount) {
        if (empty($results[$hosp_id])) {
          $results[$hosp_id] = 0;
        }
        $results[$hosp_id] += $formcount;
      }
    }
  }

  return $results;
}

function _opr2_stat_fetch_study_formcount($study) {
  $study_config = opr2_config('study', $study);
  if (empty($study_config)) return null;

  if (!empty($study_config['formcount']) || !empty($study_config['bloodcount'])) {
    $stats = array(
      'formcount' => isset($study_config['formcount']) ? $study_config['formcount'] : 0,
      'bloodcount' => isset($study_config['bloodcount']) ? $study_config['bloodcount'] : 0,
    );
    return $stats;
  }

  $study_types = array();
  foreach (opr2_config('form', array('study'=>$study)) as $type => $opr2_form) {
    if (in_array($opr2_form['type'], array('admission', 'outpatient'))) {
      $study_types[] = $type;
    }
  }

  if (empty($study_types)) {
    return null;
  }

  $stat_query = db_select('node', 'nform');
//  $stat_query->condition('nform.status', 1);
  $stat_query->condition('nform.type', $study_types, 'IN');

  if ($study_config['inpatient']) {
    $stat_query->leftJoin('field_data_field_inpatient', 'finp', 'finp.entity_id = nform.nid');
    $stat_query->leftJoin('field_data_field_blood_code', 'fblood', 'fblood.entity_id = finp.field_inpatient_target_id');
  }
  else {
    $stat_query->leftJoin('field_data_field_blood_code', 'fblood', 'fblood.entity_id = nform.nid');
  }

  $stat_query->addExpression('COUNT(nform.nid)', 'formcount');
  $stat_query->addExpression('COUNT(fblood.field_blood_code_value)', 'bloodcount');

  $stat_res = $stat_query->execute()->fetchAssoc();

  return $stat_res;
}

function _opr2_stat_number_format($num, $decimals = 0) {
  $format = number_format($num, $decimals, '.', ' ');
  return str_replace('.00', '', $format);
}

function opr2_page_counter($study = null) {
  $type = null;
  $count_rows = '';

  if (!empty($study)) switch (strtoupper($study)) {
  case 'EASY':
    $type = array('easy_form_a');
    break;

  case 'APPLE':
    $type = array('APPLE-P'=>'apple_p_form_a', 'APPLE-R'=>'apple_r_form_a');
    break;

  case 'PINEAPPLE':
    $study_r_config = opr2_config('study', 'PINEAPPLE-R');
    $type = array('PINEAPPLE-P'=>'pineapple_p_form_a', 'PINEAPPLE-R'=>$study_r_config['formcount']);
    break;
  }

  if (!empty($type)) foreach ($type as $label => $ct) {
    $count = null;
    if (is_numeric($ct)) {
      $count = $ct;

    } else {
      $stat_query = db_select('node', 'nform')
        ->fields('nform',array('nid'));
      $stat_query->condition('nform.type', $ct);
      $count = $stat_query->execute()->rowCount();
    }

    if (is_null($count)) {
      $count = t('N/A');
    }

    if (!is_numeric($label)) {
      $count_rows .= "<br>$label:";
    }

    $count_rows .= " <strong>$count</strong>";
  }


  header('Content-Type: text/html; charset=utf-8');
  print '<style>body {font-size:13px; color:#555; margin: 0; text-align:center; overflow:hidden;}</style>';
  if (empty($count_rows)) {
    print t('Could not determine enrolled patient numbers');
    return;
  }

  $output = t('Patients already enrolled:').$count_rows;
  print $output;
}

function _opr2_stat_fetch_general() {
  $stat = array();

  $stat['patients'] = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'patient')
    ->condition('n.status', 1)
    ->execute()
    ->rowCount();

  /*
  $stat['patients'] = db_select('field_data_field_patient', 'fp')
    ->distinct()
    ->fields('fp', array('field_patient_target_id'))
    ->execute()
    ->rowCount();
   */

  $studies = opr2_studies();
  foreach ($studies as $study_config) {
    if (empty($study_config['formcount'])) continue;
    $stat['patients'] += $study_config['formcount'];
  }

  /*
  $stat['samples'] = db_select('field_data_field_blood_code', 'fbc')
    ->fields('fbc', array('entity_id'))
    ->execute()
    ->rowCount();
   */
  $stat['samples'] = 2779;

  $stat['doctors'] = db_select('field_data_field_doctor_id', 'fdc')
    ->distinct()
    ->fields('fdc', array('field_doctor_id_value'))
    ->execute()
    ->rowCount();

  $stat['centers'] = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'hospital')
    ->condition('n.status', 1)
    ->execute()
    ->rowCount();

  $stat['countries'] = db_select('field_data_field_country', 'fc')
    ->distinct()
    ->fields('fc', array('field_country_iso2'))
    ->condition('fc.bundle', 'user')
    ->execute()
    ->rowCount();

  $stat['registries'] = 4;
  $stat['studies'] = 4;

  return $stat;
}

function opr2_page_inpatient($node) {
  $node_wrapper = entity_metadata_wrapper('node', $node);
  $inpatient_id = _opr2_get_serial($node);
  $render = array();
  drupal_set_title(t('Inpatient %inpatient_id', array('%inpatient_id'=>$inpatient_id)),PASS_THROUGH);
  $render['data'] = array(
    '#theme' => 'table',
    '#rows' => array(),
    '#weight' => 1,
  );
  $render['data']['#rows'][] = array(t('Inpatient id'), $inpatient_id);
  if (!empty($node->field_imported_id)) {
    $imported_id = $node_wrapper->field_imported_id->value();
    $render['data']['#rows'][] = array(t('Imported id'), l('OPR#'.$imported_id, 'http://opr.pancreas.hu/orvosinfo/inpatient/'.$imported_id, array('attributes'=>array('target'=>'_blank'))));
  }

  $patient = node_load($node_wrapper->field_patient->raw());
  $patient_id = _opr2_get_serial($patient);
  $render['data']['#rows'][] = array(t('Patient id'), l($patient_id, 'opr/patient/'.$patient_id));

  $doctor_id = $node_wrapper->field_opr_doctor_code->value();
  $render['data']['#rows'][] = array(t('Doctor id'), $doctor_id);

  $hosp = $node_wrapper->field_hospital;
  if (!empty($hosp)) {
    $hosp_id = $hosp->getIdentifier();
    $hosp_label = $hosp->label();
  }
  else {
    $hosp_id = 0;
    $hosp_label = 'N/A';
  }
  $render['data']['#rows'][] = array(t('Hospital'), "($hosp_id) $hosp_label");

  $date_start = isset($node_wrapper->field_inpatient_start) ? $node_wrapper->field_inpatient_start->value() : null;
  $render['data']['#rows'][] = array(t('Admission date'), empty($date_start) ? t('N/A') : format_date($date_start, 'day'));
  $date_end = isset($node_wrapper->field_inpatient_end) ? $node_wrapper->field_inpatient_end->value() : null;
  $render['data']['#rows'][] = array(t('Last day of treatment'), empty($date_end) ? t('N/A') : format_date($date_end, 'day'));

  $breadcrumbs = array(
    l(t('Home'), '<front>'),
  );
  $breadcrumbs[] = l(t('Forms'), 'opr/forms');
  $breadcrumbs[] = l(t('Patient history'), 'opr/patient/'.$patient_id);
  drupal_set_breadcrumb($breadcrumbs);

  $opr2_forms = opr2_forms();

  $days_list = _opr2_inpatient_days_list($node_wrapper);
  $days_submitted = _opr2_inpatient_days_submitted($node);

  $forms_ordered = _opr2_get_inpatient_forms($node->nid, TRUE, TRUE, TRUE);
  $form_approve_states = $forms_ordered['approve_states'];
  unset($forms_ordered['approve_states']);
  $studies_submitted = array_filter(array_keys($forms_ordered), function($study){return $study!='_none';});
  $render['data']['#rows'][] = array(t('Submitted for studies'), implode(', ', $studies_submitted));
  $render['data']['#rows'][] = array(t('Edit inpatient'), l(t('Edit inpatient'), 'node/'.$node->nid.'/edit', array('query'=>array('destination'=>current_path()))));

  $forms_available = array();
  foreach ($opr2_forms as $opr2type => $opr2form) {
    if (!user_access("create $opr2type content")) {
      unset($opr2_forms[$opr2type]);
      continue;
    }

    if (isset($opr2form['study']) && !in_array($opr2form['study'], $studies_submitted)) continue;

    $forms_available[$opr2type] = $opr2form;
  }

  $render['forms'] = array(
    '#theme' => 'table',
    '#header' => array(t('Form'), t('Approval'), t('Submitted on'), t('Doctor'), t('Actions')),
    '#rows' => array(),
    '#prefix' => '<h3>'.t('Submitted forms').'</h3>',
    '#weight' => 10,
    '#empty' => t('No forms were submitted yet.'),
  );

  $approval_fields = _opr2_form_approval_fields();
  $form_count = 0;
  $content_types = array();
  $form_b_days = array();
  foreach ($forms_ordered as $study => $study_forms) {
    $study_forms = $forms_ordered[$study];
    usort($study_forms, function($form1, $form2) {
      $day1 = empty($form1->field_inpatient_day['und'][0]['value']) ? 0 : intval($form1->field_inpatient_day['und'][0]['value']);
      $day2 = empty($form2->field_inpatient_day['und'][0]['value']) ? 0 : intval($form2->field_inpatient_day['und'][0]['value']);
      return $day1 - $day2;
    });
    foreach ($study_forms as $form) {
      $form_count++;
      $content_types[$form->nid] = $form->type;
      if (!node_access('view', $form)) continue;
      $form_wrapper = entity_metadata_wrapper('node', $form);

      if ($opr2_forms[$form->type]['type'] == 'daily') {
        $day = $form_wrapper->field_inpatient_day->value();
        if (!empty($day)) {
          $form_b_days[$form->type][$day] = $form;
        }
      }

      $approvals = _opr2_page_forms_approval_status($form_wrapper);

      $t_vars = array(
        '@identifier' => $form->title,
        '%patient' => _opr2_get_patient_data($form),
      );
      if (_opr2_node_is_published($form)) {
        $row_class = array('opr-form-row');
        $form_title = t('@identifier %patient', $t_vars);
        $form_link = 'opr/form/'.$form->nid;

      } else {
        $row_class = array('opr-form-row', 'opr-form-draft');
        $form_title = t('@identifier %patient (draft)', $t_vars);
        $form_link = 'node/'.$form->nid.'/edit';
      }

      $actions = array();
      if (node_access('update', $form)) {
        $actions[] = l(t('edit'), 'node/'.$form->nid.'/edit');
      }
      if (node_access('delete', $form)) {
        $actions[] = l(t('delete'), 'node/'.$form->nid.'/delete', array('query'=>array('destination'=>current_path())));
      }

      $render['forms']['#rows'][] = array(
        'class' => $row_class,
        'data' => array(
          l($form_title, $form_link, array('html'=>TRUE)),
          array(
            'data' => implode('&nbsp;', $approvals),
            'class' => 'opr-qa',
          ),
          format_date($form->created, 'medium'),
          check_plain($form_wrapper->field_inpatient->field_opr_doctor_code->raw()),
          implode(', ', $actions),
        ),
      );

      // disable single-use form buttons
      if (isset($forms_available[$form->type])) {
        if (in_array($forms_available[$form->type]['type'], array('admission', 'result'))) {
          $forms_available[$form->type]['_disabled'] = TRUE;
        }
      } else {
      }
    }
  }
  $render['data']['#rows'][] = array(t('Forms submitted'), $form_count);

  // handle compatible form buttons
  // if (user_access('administer opr forms'))
  foreach ($opr2_forms as $form_type => $form_data) {
    if (isset($forms_available[$form_type])) continue;
    if (empty($form_data['compatible'])) continue;
    if (!empty($forms_ordered[$form_data['study']])) continue;
    if (!in_array($form_data['type'], array('admission', 'outpatient'))) continue;

//    dpm($form_data, "compatible? $form_type");
    foreach ($form_data['compatible'] as $compatible_type => $compatible_method) {
      if ($compatible_nid = array_search($compatible_type, $content_types)) {
        $compatible = true;
        if (!empty($compatible_method['complete']) && !empty($opr2_forms[$compatible_type]['type_daily'])) {
          $form_b = $opr2_forms[$compatible_type]['type_daily'];

          if (!empty($days_submitted[$form_b]) && count($days_submitted[$form_b]) >= count($days_list)) {
            foreach ($forms_ordered[$opr2_forms[$compatible_type]['study']] as $comp_form) {
              if (empty($comp_form->status)
                && empty($form_approve_states[$comp_form->nid]['approve'])
                && empty($comp_form->field_qa_author['und'][0]['value'])) {
                dpm($comp_form, 'not yet approved: '.$comp_form->title);
                $compatible = user_access('access devel information'); // false;
                break;
              }
            }

          } else {
            $compatible = false;
          }
        }

        if ($compatible) {
          drupal_set_message(t('If your patient\'s blood sample was collected and you decide to take part in the general patient registry, please fill out the AP (acute pancreatitis) forms too. The answers given to the @study study will appear in the AP forms automatically, you need to answer only the additional questions.', array('@study'=>$opr2_forms[$compatible_type]['study'])));
          $forms_available[$form_type] = $form_data;
          if (!empty($compatible_method['copy'])) {
            $forms_available[$form_type]['_copy'] = $compatible_nid;
          }
          break;
        }
      }
    }
  }
//  dpm($forms_available, 'fable1');

  // handle compatible form button dependencies
  foreach ($forms_available as $form_type => $form_data) {
    if (empty($form_data['compatible'])) continue;

    $compatible = TRUE;
    foreach ($form_data['compatible'] as $compatible_type => $compatible_method) {
      if (isset($compatible_method['required']) && !in_array($compatible_type, $content_types)) {
//        dpm($compatible_method, "$form_type <> $compatible_type");
        $compatible = FALSE;
        break;
      }
      if (!empty($compatible_method['copy']) && $form_data['type'] = 'daily' && !empty($form_b_days[$compatible_type])) {
        foreach ($form_b_days[$compatible_type] as $day => $form_b) {
          if (empty($form_b_days[$form_type][$day])) {
            $forms_available[$form_type]['_copy_forms'][$day][$form_b->nid] = array(
              'title' => $form_b->title,
              'nid' => $form_b->nid,
              'study' => $opr2_forms[$compatible_type]['study'],
            );
          }
        }
      }
    }

    if (!$compatible) unset($forms_available[$form_type]);
  }
//  dpm($forms_available, 'fable2');

  if (!empty($forms_available)) {
    $render['available'] = array(
      '#theme' => 'item_list',
      '#title' => t('Submit a new form for the current inpatient'),
      '#items' => array(),
      '#attributes' => array('class'=>array('opr-study-list', 'opr-study-list-inline')),
      '#weight' => 5,
    );

    foreach ($forms_available as $form_type => $form_data) {
      if (!empty($form_data['_disabled'])) continue;
      if (!empty($days_submitted[$form_type])) {
        $remaining = array_diff(array_keys($days_list), $days_submitted[$form_type]);
        if (empty($remaining)) {
          $forms_available[$form_type]['_disabled'] = true;
        }
      }
    }

    $form_b_copy = false;
    foreach ($forms_available as $form_type => $form_data) {
      $disabled = !empty($form_data['_disabled']);

      $classes = array('opr-study-form', 'opr-study-button', 'opr-bundle-'.$form_type, 'opr-form-type-'.$form_data['type']);
      $attributes = array();
      if (isset($form_data['study'])) $classes[] = 'opr-study-'.$form_data['study'];

      if ($disabled) {
        $data = "<span>{$form_data['title']}</span>";
        $classes[] = 'opr-study-form-disabled';

      } else {
        $data_query = array('field_inpatient'=>$node->nid);
        if (!empty($form_data['_copy'])) {
          $data_query['copy_node'] = $form_data['_copy'];
        }
        if (!empty($form_data['_copy_forms'])) {
          $form_b_copy = true;
          $classes[] = 'opr-form-b-copy';
          $attributes['data-form-b-copy'] = json_encode($form_data['_copy_forms']);
        }
        $data = l($form_data['i18n_title'], 'node/add/'._opr2_type_encode($form_type), array('query'=>$data_query,'attributes'=>$attributes));
      }

      $render['available']['#items'][] = array(
        'data' => $data,
        'class' => $classes,
      );
    }

    if ($form_b_copy) {
      drupal_add_library('system', 'drupal.ajax'); 
      $render['available']['#attached']['js'][] = ctools_attach_js('modal');
      $render['available']['#attached']['css'][] = ctools_attach_css('modal');
      $render['available']['#attached']['js'][] = drupal_get_path('module', 'opr2').'/opr2-form-b-copy.js';
      $render['available']['#attached']['js'][] = array(
        'data' => array(
          'opr2-form-b-copy-modal-style' => array(
            'modalSize' => array(
              'type' => 'fixed',
              'width' => 600,
              'height' => 300,
            ),
            'closeText' => t('Cancel'),
            'closeImage' => '',
          ),
        ),
        'type' => 'setting',
      );
    }
  }

  // clear localStorage persisted form data, if any
  _opr2_garlic_clear_form();

  return $render;
}

