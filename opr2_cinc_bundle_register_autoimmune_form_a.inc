<?php

function opr2_cinc_bundle_register_autoimmune_form_a() {
  $bundle = array(
    'machine_name' => 'register_autoimmune_form_a',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => 'v1',
      ),
      'field_abdpain' => array(
        'label' => 'Hasi fájdalom jelenleg',
      ),
      'field_complication_panc' => array(
        'required' => FALSE,
      ),
      'field_complication_notes' => array(
        'label' => 'Egyéb szövődmények leírás',
      ),
      'field_epic' => array(
        'description' => 'A hospitalizáció rövid összefoglalója, beleértve, hogy hogyan került a beteg a kórházba, klinikára, mi történt vele a bentfekvés alatt és milyen javaslattal és hová távozott (kontroll vizsgálat, műtét, stb.)',
      ),
      'field_therapy_pm' => array(
        'label' => 'Tartós fájdalomcsillapítás',
      ),
    ),
    'groups' => array(
      'group_anamnestic' => array(
        'label' => '3. Anamnesztikus adatok',
      ),
      'group_lab_results_opt' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'Csak artériás vérgázparamétert lehet rögzíteni. Kérjük, jelezze a vérgáz paraméterek mérési körülményeit',
          ),
        ),
      ),
      'group_imagadm' => array(
        'label' => '7. Képalkotó eljárások',
        'format_settings' => array(
          'instance_settings' => array(
            'show_label' => 0,
          ),
        ),
      ),
      'group_gentest' => array(
        'label' => '8. Genetikai vizsgálat',
      ),
      'group_theradm' => array(
        'label' => '9. Konzervatív kezelés',
      ),
      'group_therapy_iv' => array(
        'format_settings' => array(
          'instance_settings' => array(
//            'notes' => 'A terápia kérdései ettől a kérdéstől kezdve a felvétel napjára vonatkoznak.',
          ),
        ),
      ),
      'group_therapint' => array(
        'label' => '10. Intervenciós kezelés',
      ),
      'group_complications' => array(
        'label' => '11. Egyéb szövődmények',
      ),
      'group_epic' => array(
        'label' => '12. Epikrízis',
      ),
      'group_finalreport' => array(
        'label' => '13. Ambuláns lap, zárójelentés',
      ),
    ),
    'tree' => array(
      'group_nopaging' => array(
        'group_nopage_1' => array(
          'group_opr_data' => array(
            'field_institute',
            'field_hospital',
            'field_opr_doctor_code',
            'field_blood_code',
            'field_blood_date',
          ),
          'group_personal' => array(
            'field_patient',
            'field_patient_race2',
            'field_interview_date',
          ),
          'group_checkin' => array(
            'field_checkin_type',
            'field_checkin_admission',
            'field_checkin_discharge',
            'field_checkin_days',
          ),

          'field_form_version',
          'field_imported',

          'group_diag' => array(
            'field_diag_date',

            'group_diag_base' => array(
              'group_diag_base_inline1' => array(
                'field_diag_base_morph',
              ),

              'group_diag_base_igg4' => array(
                'field_diag_base_igg4',
                'field_diag_base_igg4_amount'
              ),

              'group_diag_base_cyt' => array(
                'field_diag_base_cyt',
                'field_diag_base_cyt_desc'
              ),

              'group_diag_base_expm' => array(
                'field_diag_base_expm',
                'field_diag_base_expm_loc'
              ),

              'group_diag_base_inline2' => array(
                'field_diag_base_stereffect',
              ),
            ),
          ),
          'group_anamnestic' => array(
              'group_alcohol' => array(
                'field_alcohol_consumption',
                'field_alcohol_freq',
                'field_alcohol_amount2',
                'field_alcohol_since2',
                'field_alcohol_2w_amount',
                'group_alcohol_past' => array(
                  'field_alcohol_past',
                  'field_alcohol_past_freq',
                  'field_alcohol_past_amount',
                  'field_alcohol_past_years',
                  'field_alcohol_past_ended2',
                ),
              ),
              'group_smoking' => array(
                'field_smoking',
                'field_smoking_amount2',
                'field_smoking_since2',
                'group_smoking_past' => array(
                  'field_smoking_past',
                  'field_smoking_past_amount',
                  'field_smoking_past_years',
                  'field_smoking_past_ended2',
                ),
              ),
            'group_drugs' => array(
              'field_drug_consumption',
              'field_drug_name',
              'field_drug_amount',
              'field_drug_since3',
            ),
            'group_diabetes' => array(
              'field_diabetes',
              'field_diabetes_type',
              'field_diabetes_since',
            ),
            'group_lipdis' => array(
              'field_lipdis',
              'field_lipdis_since',
            ),
            'group_pancdis' => array(
              'field_pancdis',
              'field_pancdis_type',
              'field_pancdis_other',
            ),
            'group_pancdis_acute' => array(
              'field_pancdis_acute_times2',
              'field_pancdis_acute_first',
            ),
          ),
        ),
        'group_nopage_2' => array(
          'group_anamnestic_cont' => array(
            'group_pancdis_chronic' => array(
              'field_pancdis_chronic_when',
              'field_pancdis_chronic_first',
              'field_pancdis_chron_acute_times',
            ),
            'group_pancdis_tumor' => array(
              'field_pancdis_tumor_when',
              'field_pancdis_tumor_chron',
              'field_pancdis_tumor_chronic_when',
              'field_pancdis_tumor_acute_times',
              'field_pancdis_tumor_acute_first',
            ),
            'group_pancdis_panc' => array(
              'group_pancdis_endosc' => array(
                'field_pancdis_endosc',
                'field_pancdis_endosc_type',
                'field_pancdis_endosc_times',
                'field_pancdis_endosc_earlycomp',
                'group_pancdis_endosc_latecomp' => array(
                  'field_pancdis_endosc_latecomp_p',
                  'field_pancdis_endosc_latecomp_b',
                  'field_pancdis_endosc_latecomp_o',
                )
              ),

              'group_pancdis_surgery' => array(
                'field_pancdis_surgery',
                'field_pancdis_surgery_type',
                'field_pancdis_surgery_times',
                'field_pancdis_surgery_earlycomp',
                'group_pancdis_surgery_latecomp' => array(
                  'field_pancdis_surgery_latecomp_p',
                  'field_pancdis_surgery_latecomp_b',
                  'field_pancdis_surgery_latecomp_o',
                ),
              ),
            ),
            'group_pancdis_fam' => array(
              'field_pancdis_fam',
              'field_pancdis_fam_acute',
              'field_pancdis_fam_acute_rel',
              'field_pancdis_fam_chron',
              'field_pancdis_fam_chron_rel',
              'field_pancdis_fam_ai',
              'field_pancdis_fam_ai_rel',
              'field_pancdis_fam_tum',
              'field_pancdis_fam_tum_rel',
              'field_pancdis_fam_othertype',
              'field_pancdis_fam_othertype_rel',
            ),
            'group_pancdisord' => array(
              'field_pancdisord',
              'field_pancdisord_type',
            ),
            'group_otherdis' => array(
              'field_otherdis',
              'field_otherdis_type',
            ),
            'group_medi' => array(
              'field_medi',
              'field_medi_detail',
            ),
            'group_diet' => array(
              'field_diet',
              'field_diet_type',
            ),
          ),
        ),
        'group_nopage_3' => array(
          'group_symptoms' => array(
            'group_abdpain' => array(
              'field_abdpain',
              'field_stoma_since',
              'field_abdpain_type',
              'field_abdpain_strength',
              'field_stoma_loc',
              'field_stoma_loc_detail',
            ),
            'group_abdpain_general' => array(
              'field_abdpain_general',
              'field_abdpain_general_behav',
            ),
            'group_jaundice' => array(
              'field_jaundice',
              'field_jaundice_since3',
            ),
            'group_weightloss' => array(
              'field_weightloss',
              'field_weightloss_weeks2',
              'field_weightloss_amount2',
            ),
            'group_nausea' => array(
              'field_nausea',
            ),
            'group_vomiting' => array(
              'field_vomiting',
              'field_vomiting_times2',
              'field_vomiting_contents2',
            ),
            'group_fever' => array(
              'field_fever',
              'field_fever_since2',
              'field_fever_amount2',
              'field_fever_amount_rect',
            ),
            'group_appetite' => array(
              'field_appetite',
            ),
            'group_stool' => array(
              'field_stool',
            ),
          ),
        ),
        'group_nopage_4' => array(
          'group_admission' => array(
            'group_admission_rows' => array(
              'group_admission_row1' => array(
                'field_bp',
                'field_bp_dias',
                'field_pulse',
              ),
              'group_admission_row2' => array(
                'field_weight',
                'field_height',
              ),
              'group_admission_row3' => array(
                'field_resprate',
                'field_bodytemp',
              ),
              'group_admission_row3b' => array(
                'field_bodytemp_rect',
              ),
              'group_admission_row4' => array(
                'field_abdomtender',
                'field_abdomguard',
              ),
            ),
          ),
        ),
        'group_nopage_5' => array(
          'group_lab_results' => array(
            'group_lab_results_req' => array(
              'field_lab_result_amilase',
              'field_lab_resut_lipase',
              'field_lab_result_wbc',
              'field_lab_result_rbc',
              'field_lab_result_hg',
              'field_lab_result_htoc',
              'field_lab_result_throm',
              'field_lab_result_gluc',
              'field_lab_result_urean',
              'field_lab_result_creat',
              'field_lab_result_egfr2',
              'field_lab_result_crp2',
              'field_lab_result_asatgot',
              'field_lab_result_ldh',
              'field_lab_result_ca',
            ),
            'group_lab_results_opt' => array(
              'field_lab_result_na',
              'field_lab_result_k',
              'field_lab_result_totprot',
              'field_lab_result_alb',
              'field_lab_result_chol',
              'field_lab_result_trig',
              'field_lab_result_alatgpt',
              'field_lab_result_ggt',
              'field_lab_result_bilitot',
              'field_lab_result_dcbili',
              'field_lab_result_alph',
              'field_lab_result_esr',
              'field_lab_result_procalc2',
              'field_lab_result_iga',
              'field_lab_result_igm',
              'field_lab_result_igg',
              'field_lab_result_igg4',
              'field_lab_result_ca199',
              'field_lab_result_pao2',
              'field_lab_result_hco3',
              'field_lab_result_so2',
              'field_lab_result_swcl',
              'field_lab_result_uramil',
              'field_lab_result_urlip',
              'field_lab_result_urcreat',
            ),
            'group_virserol' => array(
              'field_virserol_virus',
              'field_virserol_res',
            ),
          ),
        ),
        'group_nopage_6' => array(
          'group_imagadm' => array(
            'group_imagadm_inline' => array(
              'field_imaging',
            ),
            'group_imaging_uhctmri' => array(
              'field_imaging_wirsung',
              'field_imaging_bildil',
              'field_imaging_gallstone',
              'field_imaging_adenomega',
              'field_imaging_adenomega_diff',
              'field_imaging_adenomega_circs',
              'field_imaging_panc_caps',
              'field_imaging_pseudocyst',
              'field_imaging_calcification',
            ),
            'group_imaging_abdus' => array(
              'field_imaging_abdus',
              'field_imaging_abdus_desc',
            ),
            'group_imaging_chestrtg' => array(
              'field_imaging_chestrtg',
              'field_imaging_chestrtg_desc',
            ),
            'group_imaging_chestct' => array(
              'field_imaging_chestct',
              'field_group_imaging_chestct_desc',
            ),
            'group_imaging_abdct' => array(
              'field_imaging_abdct',
              'field_imaging_abdct_desc',
            ),
            'group_imaging_rcp' => array(
              'field_imaging_rcp_mainduct',
              'field_imaging_rcp_branchducts',
              'field_imaging_rcp_biledil',
              'field_imaging_rcp_other',
            ),
            'group_imaging_ercp' => array(
              'field_imaging_ercp',
              'field_imaging_ercp_desc',
            ),
            'group_imaging_mrcp' => array(
              'field_imaging_mrcp',
              'field_imaging_mrcp_desc',
            ),
            'group_imaging_eussum' => array(
              'field_imaging_eus_adenomega',
              'field_imaging_eus_echodensles',
              'field_imaging_eus_lobed',
              'field_imaging_eus_echodensfoc',
              'field_imaging_eus_pseudocyst',
              'field_imaging_eus_ductabnorm',
              'field_imaging_eus_biliaryobst',
            ),
            'group_imaging_eus' => array(
              'field_imaging_eus',
              'field_imaging_eus_desc',
            ),
          ), // end imaging
        ),
        'group_nopage_7' => array(
          'group_gentest' => array(
            'group_gentest_pre' => array(
              'field_gentest_pre',
              'field_gentest_pre_res',
            ),
          ),
          'group_theradm' => array(
            'group_therapy_steroid' => array(
              'field_therapy_steroid',
              'field_therapy_steroid_med',
              'field_therapy_steroid_doseind',
              'field_therapy_steroid_dosesus',
            ),
            'group_therapy_ensime' => array(
              'field_therapy_ensime',
              'field_therapy_ensime_med',
              'field_therapy_ensime_dose',
            ),
            'group_therapy_oraladb' => array(
              'field_therapy_oraladb',
              'field_therapy_oraladb_med',
              'field_therapy_oraladb_dose',
            ),
            'group_therapy_pm' => array(
              'field_therapy_pm',
              'field_therapy_pm_detail',
            ),
            'group_therapy_ins' => array(
              'field_therapy_ins',
            ),
            'field_therapy_ins_dosage2',
            'group_therapy_other' => array(
              'field_therapy_other',
              'field_therapy_other_desc',
            ),
          ),
        ),
        'group_nopage_8' => array(
          'group_therapint' => array(
            'group_therapint_endosc' => array(
              'field_therapint_endosc',
              'field_therapint_endosc_type',
              'field_therapint_endosc_stent',
              'field_therapint_endosc_earlycomp',
            ),
            'group_therapint_ercp' => array(
              'field_therapint_ercp',
              'group_therapint_bilducan' => array(
                'field_therapint_bilducan',
                'field_therapint_bilducan_note',
              ),
              'group_therapint_precut' => array(
                'field_therapint_precut',
                'field_therapint_precut_type',
              ),
              'group_therapint_est' => array(
                'field_therapint_est',
                'field_therapint_est_type',
              ),
              'group_therapint_stonex' => array(
                'field_therapint_stonex',
              ),
              'group_ercp_stent' => array(
                'field_ercp_stent',
                'field_ercp_stent_material',
                'field_ercp_stent_amount',
                'field_ercp_stent_dia',
                'field_ercp_stent_length',
              ),
              'group_ercp_ductfill' => array(
                'field_ercp_ductfill',
                'field_ercp_ductfill_notes',
              ),
            ), // end ercp
            'field_therapint_ercp_desc',
            'group_therapint_surg' => array(
              'field_therapint_surg',
              'field_therapint_surg_type',
              'field_therapint_surg_detail',
              'group_therapint_surg_earlycomp' => array(
                'field_therapint_surg_earlycomp',
                'field_therapint_surg_earlycomp_t',
              ),
              'group_therapint_surg_reop' => array(
                'field_therapint_surg_reop',
                'field_therapint_surg_reop_desc',
              ),
              'field_therapint_surg_desc',
            ), // end surg
          ),
          'group_complications' => array(
            'group_complication_panc' => array(
              'field_complication_panc',
              'field_complication_panc_type2',
            ),
            'group_complication_biliar' => array(
              'field_complication_biliar',
              'field_complication_biliar_type',
            ),
            'group_complication_orgfail' => array(
              'field_complication_orgfail',
              'field_complication_orgfail_org2',
            ),
            'group_complication_other' => array(
              'field_complication_other',
              'field_complication_other_type',
            ),
            'field_complication_notes',
          ),
          'group_epic' => array(
            'field_epic',
          ),
          'group_finalreport' => array(
            'field_finalreport',
          ),
          'field_notes',
        ),
      ), // end paging

    ), // end tree
  );

  return $bundle;
}
