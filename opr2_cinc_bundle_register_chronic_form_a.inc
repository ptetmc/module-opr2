<?php

function opr2_cinc_bundle_register_chronic_form_a() {
  $bundle = array(
    'machine_name' => 'register_chronic_form_a',
    'fields' => array(
      array(
        'field_name' => 'field_serial_register_cp',
        'type' => 'serial',
        'cardinality' => '1',
        'instance' => 
        array (
          'label' => 'Serial Register-CP',
          'widget' => 
          array (
            'type' => 'serial',
          ),
        ),
      ),
    ), // end fields

    'instances' => array(
      'field_form_version' => array(
        'default_value' => array(array('value'=>'v1')),
      ),
      'field_complete' => array(
        'label' => '17. Hiánytalan adatfeltöltés',
        'i18n' => array(
          'label' => '17. Full, correct upload of data',
        ),
      ),
      'field_abdpain' => array(
        'label' => 'Hasi fájdalom jelenleg',
        'i18n' => array(
          'label' => 'Abdominal pain currently',
        ),
      ),
      'field_pancdis' => array(
        'description' => '<span class="red">Amennyiben a beteg korábban kezelésben (fekvő- vagy járóbetegként) részesült, szükséges feltüntetni.</span>',
        'i18n' => array(
          'description' => '<span class="red">Please indicate if the patient was treated earlier as an out-patient or inpatient.</span>',
        ),
      ),
      'field_medi_type' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_medi_amount' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_medi_since' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_weightloss_weeks2' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_weightloss_amount2' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_history' => array(
        'label' => 'Egyéb előzmény',
        'i18n' => array(
          'label' => 'Other events in the medical history',
        ),
      ),
      'field_etiology_biliary' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_etiology_recurracute' => array(
        'description' => '<span class="red">Amennyiben a krónikus pankreátitiszt ez okozta</span>',
        'i18n' => array(
          'description' => '<span class="red">If this is the cause of chronic pancreatitis.</span>',
        ),
      ),
      'field_imaging' => array(
        'label' => '8. Képalkotó eljárás',
        'description' => '<span class="red">Több képalkotó esetén a bentfekvés alatt készült leginkább releváns képalkotót kerjük betenni.</span>',
        'i18n' => array(
          'label' => '8. Imaging examinations',
          'description' => '&nbsp;',
        ),
      ),
      'field_imaging_adenomega' => array(
        'label' => 'Mirigy megnagyobbodás (>2x)',
      ),
      'field_functexam' => array(
        'label' => '9. Funkcionális vizsgálatok',
        'i18n' => array(
          'label' => '9. Functional examination',
        ),
      ),
      'field_histology' => array(
        'label' => '10. Szövettan',
        'i18n' => array(
          'label' => '10. Hystology/ cytology',
        ),
      ),
      'field_field_therapy_pm_med' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_pm_dose' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_pm_type' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapint' => array(
        'label' => '13. Intervenciós kezelés',
        'description' => '<span class="red">Amennyiben az adott befekvéskor intervenció történt, annak adatait kérjük feltölteni, különös tekintettel a beavatkozás részleteire! A lelet szöveges formában is feltöltendő!</span>',
        'i18n' => array(
          'label' => '13. Interventional treatment',
          'description' => '<span class="red">Please indicate the intervention detailed. Please provide the description as well.</span>',
        ),
      ),
    ),

    'groups' => array(
      'group_nopaging' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'Amennyiben krónikus pankreátitisz heveny fellángolása (akut shub-ja) miatt áll a beteg kezelés alatt, kérjük az ’akut pankreátitisz’, AP adatlap kitöltését is!',
            'notes_en' => 'Please fill the acute pancreatitis (AP) form as well if the patient is treated because of the acute shub of chronic pancreatitis!',
          ),
        ),
      ),
      'group_anamnestic' => array(
        'label' => '3. Anamnesztikus adatok',
        'i18n' => array(
          'label' => '3. Details from the medical history',
        ),
      ),
      'group_etiology' => array(
        'label' => '4. Etiológia',
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'A válasz igen ha az etiológiai faktor alátámasztott, a válasz nem, ha az etiológiai faktor kizárható, a válasz „nincs adat”, ha az etiológiai faktorra nem történt vizsgálat, a válasz „idiopáthiás”, ha etiológiai faktor nem azonosított.',
            'notes_en' => 'The answer is “yes” if the etiological factor is proved, the answer is “no” if the etiological factor can be ruled out, the answer is “no data” if the etiological factor was not examined. Please answer “yes” to ” Idiopathic” if etiological factor was not identified.',
          ),
        ),
        'i18n' => array(
          'label' => '4. Etiology',
        ),
      ),
      'group_symptoms' => array(
        'label' => '5. Panaszok, tünetek',
        'i18n' => array(
          'label' => '5. Complains, symptoms',
        ),
      ),
      'group_pancdis_panc' => array(
        'label' => 'Beavatkozások, amennyiben az anamnézisben hasnyálmirigy betegség szerepel',
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'A korábbi beavatkozások esetén fontos a beavatkozások számát és az ehhez társuló esetleges szövődményeket feltüntetni.',
            'notes_en' => 'Please indicate the number of previous interventions and accompanying complpications.',
          ),
        ),
        'i18n' => array(
          'label' => 'If the patient had pancreatic disorder in the medical history',
        ),
      ),
      'group_admission' => array(
        'label' => '6. Felvételi adatok, status',
        'i18n' => array(
          'label' => '6. Admission details and state',
        ),
      ),
      'group_lab_results' => array(
        'label' => '7. Laboratóriumi paraméterek',
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'Az adott befekvéshez köthető, a betegség szempontjából leginkább releváns laboratóriumi paraméterek feltöltése szükséges.',
            'notes_en' => 'Please indicate the laboratory parameters most relevant to chronic pancreatitis!',
          ),
        ),
        'i18n' => array(
          'label' => '7. Laboratory parameters',
        ),
      ),
      'group_lab_results_req' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'label_visibility' => 1,
          ),
        ),
      ),
      'group_lab_results_opt' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'label_visibility' => 1,
          ),
        ),
      ),
      'group_imaging' => array(
        'label' => '8. Képalkotó eljárás',
        'format_settings' => array(
          'instance_settings' => array(
            'show_label' => 0,
          ),
        ),
      ),
      'group_complication_orgfail' => array(
        'label' => 'Más szervrendszert érintő',
        'i18n' => array(
          'label' => '4. Etiology',
        ),
      ),
      'group_histology_outer' => array(
        'label' => '10. Szövettan',
        'format_settings' => array(
          'instance_settings' => array(
            'show_label' => 0,
          ),
        ),
      ),
      'group_histology_hyst' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'Amennyiben az adott befekvésnél történt szövettani/cytológiai mintavétel, kérjük annak eredményét pótlólag is bevinni a rendszerbe!',
            'notes_en' => 'Please enter here all hystology descriptions, even if the result is available later.',
          ),
        ),
      ),
      'group_gentest' => array(
        'label' => '11. Genetikai vizsgálat',
        'i18n' => array(
          'label' => '11. Genetic testing',
        ),
      ),
      'group_therapy' => array(
        'label' => '12. Konzervatív kezelés',
        'i18n' => array(
          'label' => '12. Conservative treatment',
        ),
      ),
      'group_therapint' => array(
        'label' => '13. Intervenciós kezelés',
        'format_settings' => array(
          'instance_settings' => array(
            'show_label' => 0,
          ),
        ),
      ),
      'group_complications' => array(
        'label' => '14. Szövődmények',
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'A bentfekvés során észlelt, a betegséghez, illetve beavatkozáshoz köthető szövődmény(ek) feltüntetését kérjük.',
            'notes_en' => 'Please indicate the complications experienced during the hospitalization, connected to chronic pancreatitis or the interventions!',
          ),
        ),
        'i18n' => array(
          'label' => '14. Other complications',
        ),
      ),
      'group_epic' => array(
        'label' => '15. Epikrízis',
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'A hospitalizáció/ambuláns megjelenés rövid összefoglalója, beleértve, hogy hogyan került a beteg a kórházba, klinikára, mi történt vele a bentfekvés alatt és milyen javaslattal és hová távozott (kontroll vizsgálat, műtét, stb.)',
            'notes_en' => 'A short summary of the hospitalization (how the patient got to medical care, diagnosis, most important facts and events of the hospitalization, what happened with the patient after the hospitalization, any recommended control examinations, surgery).',
          ),
        ),
        'i18n' => array(
          'label' => '15. Epicrisis',
        ),
      ),
      'group_finalreport' => array(
        'label' => '16. Ambuláns lap, zárójelentés',
        'i18n' => array('label' => '16. Final report'),
      ),
    ),

    'tree' => array(
      'field_serial_register_cp',
      'group_nopaging' => array(
        'group_nopage_1' => array(
          'field_inpatient',
          'group_personal_cont' => array(
            'field_patient_race2',
            'field_interview_date',
            'field_consent_form',
            'field_inpatient_type',
          ),
          'group_checkin' => array(
            'field_checkin_type',
            'field_checkin_admission',
            'field_checkin_discharge',
            'field_checkin_days', // @todo calc
          ),

          'field_form_version',
          'field_imported',

          'group_diag' => array(
            'group_diag_inline' => array(
              'field_diag_new',
              'field_diag_date',
              'field_diag_test',
            ),

            'group_diag_base' => array(
              'group_diag_base_inline1' => array(
                'field_diag_base_morph',
              ),

              'group_diag_base_cyt' => array(
                'field_diag_base_cyt2',
                'field_diag_base_cyt_desc'
              ),

              'group_diag_base_funct' => array(
                'field_diag_base_funct',
              ),
              'group_diag_base_clin' => array(
                'field_diag_base_clin',
                'field_diag_base_clin_type'
              ),

              'field_diag_base_desc',
            ),
          ),

          'group_anamnestic' => array(
            'group_smoking' => array(
              'field_smoking',
              'field_smoking_amount2',
              'field_smoking_since2',
              'group_smoking_past' => array(
                'field_smoking_past',
                'field_smoking_past_amount',
                'field_smoking_past_years',
                'field_smoking_past_ended2',
              ),
            ),
            'group_alcohol' => array(
              'field_alcohol_consumption',
              'field_alcohol_freq',
              'field_alcohol_amount2',
              'field_alcohol_since2',
              'field_alcohol_2w_amount',
              'group_alcohol_past' => array(
                'field_alcohol_past',
                'field_alcohol_past_freq',
                'field_alcohol_past_amount',
                'field_alcohol_past_years',
                'field_alcohol_past_ended2',
              ),
            ),
            'group_drugs' => array(
              'field_drug_consumption',
              'field_drug_name',
              'field_drug_amount',
              'field_drug_since3',
            ),
            'group_diabetes' => array(
              'field_diabetes',
              'field_diabetes_type',
              'field_diabetes_since',
            ),
            'group_lipdis' => array(
              'field_lipdis',
              'field_lipdis_since',
            ),
            'group_pancdis' => array(
              'field_pancdis',
              'field_pancdis_type',
              'field_pancdis_other',
              'group_pancdis_acute' => array(
                'field_pancdis_acute_times2',
                'field_pancdis_acute_first',
                'field_pancdis_acute_lastip',
              ),
              'group_pancdis_chronic' => array(
                'field_pancdis_chronic_when',
                'field_pancdis_chronic_first',
                'field_pancdis_chron_acute_times',
              ),
              'group_pancdis_tumor' => array(
                'field_pancdis_tumor_when',
                'field_pancdis_tumor_chron',
                'field_pancdis_tumor_chronic_when',
                'field_pancdis_tumor_acute_times',
                'field_pancdis_tumor_acute_first',
              ),
              'field_pancdis_notes',
              'group_pancdis_panc' => array(
                'group_pancdis_endosc' => array(
                  'field_pancdis_endosc',
                  'field_pancdis_endosc_type',
                  'field_pancdis_endosc_times',
                  'group_pancdis_endosc_earlycomp' => array(
                    'field_pancdis_endosc_earlycomp',
                  ),
                  'group_pancdis_endosc_latecomp' => array(
                    'field_pancdis_endosc_latecomp_p',
                    'field_pancdis_endosc_latecomp_b',
                    'field_pancdis_endosc_latecomp_o',
                  ),
                ),

                'group_pancdis_surgery' => array(
                  'field_pancdis_surgery',
                  'field_pancdis_surgery_type',
                  'field_pancdis_surgery_times',
                  'group_pancdis_surgery_earlycomp' => array(
                    'field_pancdis_surgery_earlycomp',
                  ),
                  'group_pancdis_surgery_latecomp' => array(
                    'field_pancdis_surgery_latecomp_p',
                    'field_pancdis_surgery_latecomp_b',
                    'field_pancdis_surgery_latecomp_o',
                  ),
                ),
              ),
            ),
          ),
        ),
        'group_nopage_2' => array(
          'group_anamnestic_cont' => array(
            'group_pancdis_fam' => array(
              'field_pancdis_fam',
              'field_pancdis_fam_acute',
              'field_pancdis_fam_acute_rel',
              'field_pancdis_fam_chron',
              'field_pancdis_fam_chron_rel',
              'field_pancdis_fam_ai',
              'field_pancdis_fam_ai_rel',
              'field_pancdis_fam_tum',
              'field_pancdis_fam_tum_rel',
              'field_pancdis_fam_othertype',
              'field_pancdis_fam_othertype_rel',
            ),
            'group_pancdisord' => array(
              'field_pancdisord',
              'field_pancdisord_type',
            ),
            'group_otherdis' => array(
              'field_otherdis',
              'field_otherdis_type',
            ),
            'group_medi' => array(
              'field_medi',
              'field_medi_detail',
            ),
            'group_medi_list' => array(
              'field_medi_type',
              'field_medi_amount',
              'field_medi_since',
            ),
            'group_diet' => array(
              'field_diet',
              'field_diet_type',
            ),
            'group_history' => array(
              'field_history',
            ),
          ),
        ),
        'group_nopage_3' => array(
          'group_etiology' => array(
            'field_etiology_biliary',
            'field_etiology_alcohol',
            'field_etiology_virus',
            'field_etiology_autoimmune',
            'field_etiology_recurracute',
            'field_etiology_obstructive',
            'field_etiology_cysticfibrosit',
            'field_etiology_genetic',
            'field_etiology_idiopathic',
            'group_etiology_other' => array(
              'field_etiology_other',
              'field_etiology_other_desc',
            ),
          ),
          'group_symptoms' => array(
            'group_abdpain' => array(
              'field_abdpain',
              'field_stoma_since',
              'field_abdpain_type',
              'field_abdpain_strength',
              'field_stoma_loc',
              'field_stoma_loc_detail',
              'field_stoma_rad',
            ),
            'group_abdpain_general' => array(
              'field_abdpain_general',
              'field_abdpain_general_behav',
              'field_abdpain_general_cpdays',
            ),
            'group_nausea' => array(
              'field_nausea',
            ),
            'group_vomiting' => array(
              'field_vomiting',
              'field_vomiting_times2',
              'field_vomiting_contents2',
            ),
            'group_fever' => array(
              'field_fever',
              'field_fever_since2',
              'field_fever_amount2',
            ),
            'group_appetite' => array(
              'field_appetite',
            ),
            'group_weightloss' => array(
              'field_weightloss',
              'field_weightloss_6m',
              'field_weightloss_3y',
              'field_weightloss_weeks2',
              'field_weightloss_amount2',
            ),
            'group_stool' => array(
              'field_stool',
              'field_stool_freq',
            ),
            'group_enzymether' => array(
              'field_enzymether',
              'field_enzymether_name',
              'field_enzymether_actsub',
              'field_enzymether_dose',
              'field_enzymether_eff',
            ),
          ),
        ),
        'group_nopage_4' => array(
          'group_admission' => array(
            'group_admission_rows' => array(
              'group_admission_row1' => array(
                'field_bp',
                'field_bp_dias',
                'field_pulse',
              ),
              'group_admission_row2' => array(
                'field_weight',
                'field_height',
              ),
              'group_admission_row3' => array(
                'field_resprate',
                'field_bodytemp',
              ),
              'group_admission_row4' => array(
                'field_abdomtender',
                'field_abdomguard',
              ),
              'group_jaundice' => array(
                'field_jaundice',
                'field_jaundice_since3',
              ),
            ),
          ),
        ),
        'group_nopage_5' => array(
          'group_lab_results' => array(
            'group_lab_results_req' => array(
              'field_lab_result_amilase',
              'field_lab_resut_lipase',
              'field_lab_result_wbc',
              'field_lab_result_rbc',
              'field_lab_result_hg',
              'field_lab_result_htoc',
              'field_lab_result_throm',
              'field_lab_result_gluc',
              'field_lab_result_urean',
              'field_lab_result_creat',
              'field_lab_result_egfr2',
              'field_lab_result_crp2',
              'field_lab_result_asatgot',
              'field_lab_result_ldh',
              'field_lab_result_ca',
            ),
            'group_lab_results_opt' => array(
              'field_lab_result_na',
              'field_lab_result_k',
              'field_lab_result_totprot',
              'field_lab_result_alb',
              'field_lab_result_chol',
              'field_lab_result_trig',
              'field_lab_result_alatgpt',
              'field_lab_result_ggt',
              'field_lab_result_bilitot',
              'field_lab_result_dcbili',
              'field_lab_result_alph',
              'field_lab_result_esr',
              'field_lab_result_procalc2',
              'field_lab_result_iga',
              'field_lab_result_igm',
              'field_lab_result_igg',
              'field_lab_result_igg4',
              'field_lab_result_ca199',
              'field_lab_result_pao2',
              'field_lab_result_hco3',
              'field_lab_result_so2',
              'field_lab_result_swcl',
              'field_lab_result_uramil',
              'field_lab_result_urlip',
              'field_lab_result_urcreat',
            ),
          ),
        ),
        'group_nopage_6' => array(
          'group_imaging' => array(
            'group_imagadm_inline' => array(
              'field_imaging',
            ),
            'group_imaging_uhctmri' => array(
              'field_imaging_wirsung',
              'field_imaging_bildil',
              'field_imaging_gallstone',
              'field_imaging_adenomega',
              'field_imaging_pseudocyst',
              'field_imaging_calcification',
            ),
            'group_imaging_abdus' => array(
              'field_imaging_abdus',
              'field_imaging_abdus_desc',
            ),
            'group_imaging_abdrtg' => array(
              'field_imaging_abdrtg',
              'field_imaging_abdrtg_desc',
            ),
            'group_imaging_chestrtg' => array(
              'field_imaging_chestrtg',
              'field_imaging_chestrtg_desc',
            ),
            'group_imaging_chestct' => array(
              'field_imaging_chestct',
              'field_group_imaging_chestct_desc',
            ),
            'group_imaging_abdct' => array(
              'field_imaging_abdct',
              'field_imaging_abdct_desc',
            ),
            'group_imaging_rcp' => array(
              'field_imaging_rcp_mainduct',
              'field_imaging_rcp_branchducts',
              'field_imaging_rcp_biledil',
              'field_imaging_rcp_other',
            ),
            'group_imaging_ercp' => array(
              'field_imaging_ercp',
              'field_imaging_ercp_desc',
            ),
            'group_imaging_mrcp' => array(
              'field_imaging_mrcp',
              'field_imaging_mrcp_desc',
            ),
            'group_imaging_eussum' => array(
              'field_imaging_eus_adenomega',
              'field_imaging_eus_echodensles',
              'field_imaging_eus_pseudocyst',
              'field_imaging_eus_ductabnorm',
              'field_imaging_eus_biliaryobst',
            ),
            'group_imaging_eus' => array(
              'field_imaging_eus',
              'field_imaging_eus_desc',
            ),
          ), // end imaging
          'group_functexam_cat' => array(
            'group_functexam_inline' => array(
              'field_functexam',
            ),
            'group_functexam_elastase' => array(
              'field_functexam_elastase',
              'field_functexam_elastase_res',
            ),
            'group_functexam_breath' => array(
              'field_functexam_breath',
              'field_functexam_breath_res',
            ),
            'group_functexam_other' => array(
              'field_functexam_other',
              'field_functexam_other_type',
              'field_functexam_other_res',
            ),
          ),
          'group_histology_outer' => array(
            'group_histology_inline' => array(
              'field_histology',
            ),
            'group_histology_hyst' => array(
              'field_histology_hyst',
              'field_histology_hyst_desc',
            ),
            'group_histology_cyt' => array(
              'field_histology_cyt',
              'field_histology_type',
              'field_histology_desc',
            ),
          ),
          'group_gentest' => array(
            'group_gentest_pre' => array(
              'field_gentest_pre',
              'field_gentest_pre_res',
              'field_gentest_pre_res2',
            ),
          ),
        ), // page 6 end
        'group_nopage_7' => array(
          'group_therapy' => array(
            'group_therapy_ensime' => array(
              'field_therapy_ensime',
              'field_therapy_ensime_med',
              'field_therapy_ensime_dose',
            ),
            'group_therapy_pm' => array(
              'field_therapy_pm',
              'field_therapy_pm_type',
              'field_therapy_pm_detail',
            ),
            'group_therapy_pm_list' => array(
              'field_field_therapy_pm_med',
              'field_therapy_pm_dose',
            ),
            'group_therapy_oraladb' => array(
              'field_therapy_oraladb',
              'field_therapy_oraladb_med',
              'field_therapy_oraladb_dose',
            ),
            'group_therapy_ins' => array(
              'field_therapy_ins',
            ),
            'field_therapy_ins_dosage2',
            'group_therapy_intensive' => array(
              'field_therapy_intensive',
              'field_therapy_intensive_type',
              'field_therapy_intensive_notes',
            ),
            'group_therapy_other' => array(
              'field_therapy_other',
              'field_therapy_other_desc',
            ),
          ),
          'group_therapint' => array(
            'group_therapint_inline' => array(
              'field_therapint',
            ),
            'group_therapint_endosc' => array(
              'field_therapint_endosc',
              'field_therapint_endosc_type',
              'field_therapint_endosc_stent',
              'group_therapint_endosc_earlycomp' => array(
                'field_therapint_endosc_earlycomp',
              ),
            ),
            'group_therapint_ercp' => array(
              'field_therapint_ercp',
              'group_therapint_bilducan' => array(
                'field_therapint_bilducan',
                'field_therapint_bilducan_note',
              ),
              'group_therapint_precut' => array(
                'field_therapint_precut',
                'field_therapint_precut_type',
              ),
              'group_therapint_est' => array(
                'field_therapint_est',
                'field_therapint_est_type',
              ),
              'group_therapint_stonex' => array(
                'field_therapint_stonex',
              ),
              'group_ercp_stent' => array(
                'field_ercp_stent',
                'field_ercp_stent_material',
                'field_ercp_stent_amount',
                'field_ercp_stent_dia',
                'field_ercp_stent_length',
              ),
              'group_ercp_ductfill' => array(
                'field_ercp_ductfill',
                'field_ercp_ductfill_notes',
              ),
            ), // end ercp
            'field_therapint_ercp_desc',
            'group_therapint_surg' => array(
              'field_therapint_surg',
              'field_therapint_surg_type',
              'field_therapint_surg_detail',
              'group_therapint_surg_earlycomp' => array(
                'field_therapint_surg_earlycomp',
                'field_therapint_surg_earlycomp_t',
              ),
              'group_therapint_surg_hist' => array(
                'field_therapint_surg_hist',
                'field_therapint_surg_hist_desc',
              ),
              'group_therapint_surg_reop' => array(
                'field_therapint_surg_reop',
                'field_therapint_surg_reop_desc',
              ),
            ),
            'field_therapint_surg_desc',
          ),
        ),
        'group_nopage_8' => array(
          'group_complications' => array(
            'group_complication_panc' => array(
              'field_complication_panc',
              'field_complication_panc_type2',
            ),
            'group_complication_biliar' => array(
              'field_complication_biliar',
              'field_complication_biliar_type',
            ),
            'group_complication_orgfail' => array(
              'field_complication_orgfail',
              'field_complication_orgfail_org2',
            ),
            'group_complication_death' => array(
              'field_complication_death',
              'field_complication_death_date',
              'field_complication_death_time',
            ),
            'group_complication_other' => array(
              'field_complication_other',
              'field_complication_other_type',
            ),
            'group_complication_notes' => array(
              'field_complication_notes',
            ),
          ),
          'group_epic' => array(
            'field_epic',
          ),
          'group_finalreport' => array(
            'field_finalreport',
          ),
          'group_complete' => array(
            'field_complete',
          ),
        ),
      ), // end paging
    ), // end tree
  );

  return $bundle;
}

function opr2_cinc_bundle_register_chronic_form_a_compatibility($version = 'v0') {
  switch ($version) {
  case 'v0':
    return array(
      'field_consent_form' => array(
        'required' => FALSE,
      ),
      'field_diag_new' => array(
        'required' => FALSE,
      ),
      'field_complication_notes' => array(
        'required' => FALSE,
      ),
      'field_complete' => array(
        'required' => FALSE,
      ),
    );
  }
}
