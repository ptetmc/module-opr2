(function ($) {
Drupal.behaviors.Opr2ApproveButtons = {
  attach: function(context) {
    $('form.node-form').delegate('input.form-submit.form-submit-reject', 'click', function(){
      var $this = $(this),
        $reason = $('.opr2-approvals-qa-reject-reason-container'),
        $button = $this.clone().attr('id', null);

      if ($this.data('message.done')) return true;

      $button.click(function(){
        $reason.find('textarea').val($('#modal-content .form-item--qa-reject-reason textarea').val());
        $this.data('message.done', true);
        $this.click();
      });

      Drupal.CTools.Modal.show('opr2-approve-modal-style');
      $('#modal-title').html($button.val());
      $('#modal-content').html($reason.html()+'<div class="form-actions"></div>');
      $('#modal-content .form-actions').append($button);
      return false;
    });
  }
};
})(jQuery);


