<?php

define('OPR2_TASK_NOTIFY_REJECTED_TIMEOUT', 23*3600);

function opr2_task_notify_rejected($users = null, $options = array()) {
  module_load_include('inc', 'opr2', 'opr2_o2l');

  if (empty($users)) {
    _o2l('fetching users...');
    $uqry = new EntityFieldQuery();
    $uqry->entityCondition('entity_type', 'user')
      ->propertyCondition('status', 1);

    if (empty($options['ignore_setting'])) {
      $uqry->fieldCondition('field_notify_rejected', 'value', 1);
    }

    $ures = $uqry->execute();

    if (empty($ures['user'])) {
      // nothing to do
      _o2l(null, 'no users to notify', 'ok');
      return;
    }
    $users = user_load_multiple(array_keys($ures['user']));
  }
  _o2l($users, 'looking up notifications for '.count($users).' users', 'ok');

  $opr2_forms = opr2_forms();
  $opr2_studies = opr2_studies();
  $approval_fields = _opr2_form_approval_fields();
  $now = time();

  foreach ($users as $usr) {
    _o2l($usr, "processing user ({$usr->uid}) {$usr->mail}");

    $last_time = $last_date = null;
    if (isset($usr->data['opr2_task_notify_rejected_last'])) {
      $last_time = $usr->data['opr2_task_notify_rejected_last'];
      $last_date = date('Y-m-d H:i:s', $last_time);
      _o2l($usr->data, "\tlast notification time was $last_date");
    }
    else {
      _o2l($usr->data, "\tlast notification time is not set");
    }

    if (!empty($options['reset_last'])) {
      // empty last rejected time, then exit
      if ($last_time) {
        _o2l($usr->data, "\tresetting last notification time for {$usr->mail}: was $last_date");
        unset($usr->data['opr2_task_notify_rejected_last']);
        user_save($usr);
      }
      continue;
    }

    $rqry = db_select('opr2_form_log', 'l')
      ->fields('l', array('entity_id', 'uid', 'datetime', 'data'))
      ->condition('l.op', 'reject');

    $rqry->leftJoin('node', 'n', 'n.nid = l.entity_id');
    $rqry->fields('n', array('type'))
      ->condition('n.uid', $usr->uid)
      ->condition('n.status', 0);

    if ($last_time && empty($options['ignore_lastnote']) && $last_time > $now - OPR2_TASK_NOTIFY_REJECTED_TIMEOUT) {
      _o2l($usr, "\tskipping user {$usr->mail}: last notification date was $last_date");
      continue;
    }

    if ($last_time && empty($options['old_rejections'])) {
      _o2l($usr, "\tfetching from $last_date for {$usr->mail}");
      $rqry->condition('l.datetime', $last_date, '>');
    }

    $rqry->orderBy('l.datetime', 'DESC');
    $rres = $rqry->execute();
    _o2l($rres, "\trejected results (".$rres->rowCount().") for {$usr->mail}");

    $rejects = array();
    $rejects_cnt = 0;
    while ($rrow = $rres->fetchAssoc()) {
      if (!isset($opr2_forms[$rrow['type']])) {
        continue;
      }
      $opr2_form = $opr2_forms[$rrow['type']];
      if (isset($opr2_studies[$opr2_form['study']]['title'])) {
        $study = $opr2_studies[$opr2_form['study']]['title'];
      }
      else {
        $study = $opr2_form['study'];
      }

      $form = opr2_load_node($rrow['entity_id']);
      if (empty($form)) {
        continue;
      }

      if (empty($form->field_inpatient['und'][0]['target_id'])) {
        $ip_id = '_outpatient';
      }
      else {
        $ip_id = $form->field_inpatient['und'][0]['target_id'];
      }

      if (isset($rejects[$ip_id][$form->nid])) {
        continue;
      }

      if ($opr2_forms[$rrow['type']]['type'] == 'admission') {
        $rejects[$study][$ip_id]['_label'][] = "$study #"._opr2_get_serial($form, 'N', FALSE);
      }

      $rreason = '';
      if (!empty($form->field_qa_reject_reason['und'][0]['value'])) {
        $rreason = $form->field_qa_reject_reason['und'][0]['value'];
        $firstline_end = strpos($rreason, "\n");
        if ($firstline_end > 0) {
          $rreason = substr($rreason, 0, $firstline_end).'...';
        }
      }

      $rejects[$study][$ip_id][$form->nid] = array(
        $form->title,
        $rreason,
        $rrow['datetime'],
      );
      $rejects_cnt++;
    }
    _o2l($rejects, "\t$rejects_cnt rejects for {$usr->mail}");

    if (empty($rejects)) {
      continue;
    }

    $list = array();
    $list_break = "\n===================\n";
    foreach ($rejects as $study => $ips) {
      $list[] = "*** $study ***";
      foreach ($ips as $ip_id => $forms) {
        $ipname = null;
        if (isset($forms['_label'])) {
          $ipname = implode(' / ', array_unique($forms['_label']));
          unset($forms['_label']);
        }
        else if ($ip_id != '_outpatient') {
          $ipname = _opr2_get_inpatient_label($ip_id);
        }
        if (!empty($ipname)) {
          $list[] = "\n - $ipname";
        }
        foreach ($forms as $nid => $form) {
          $list[] = "   + {$form[0]}: {$form[1]}";
        }
      }
      $list[] = $list_break;
    }

    $params = array(
      'list' => implode("\n", $list),
      'rejects_cnt' => $rejects_cnt,
      'recipient_name' => $usr->name,
    );
    $send_mail = empty($options['mail_override']) ? $usr->mail : $options['mail_override'];
    _o2l($params, 'sending mail for '.$send_mail, 'ok');

    drupal_mail('opr2', 'notify_rejected', $send_mail, user_preferred_language($usr), $params);

    $usr_edit = array(
      'data' => array(
        'opr2_task_notify_rejected_last' => $now,
      ),
    );
    user_save($usr, $usr_edit);
  }
}

function opr2_task_fetch_log_diff($nid, $options = array()) {
  $qry = db_select('opr2_form_log', 'l')
    ->fields('l')
    ->condition('entity_id', $nid)
    ->orderBy('datetime', 'ASC');

  $res = $qry->execute();

  $ret = array();
  while ($row = $res->fetchAssoc()) {
    $usr = user_load($row['uid']);

    $retkey = implode(' -- ', array(
      $row['datetime'],
      'op:'.$row['op'],
      'uid:'.$row['uid'],
      'uname:'.(empty($usr) ? '[deleted]' : $usr->name),
      empty($row['path']) ? '[nopath]' : ('path:'.$row['path']),
    ));

    if (in_array($row['op'], array('add', 'edit'))) {
      $submit_data = _opr2_get_form_submit_data($row['datetime'], $row['entity_id']);
      if (empty($submit_data)) {
        $ret[$retkey] = "[err] missing submit data ({$row['datetime']}, {$row['entity_id']})";
        continue;
      }

      $diff = _opr2_tasks_calc_log_line_diff($nid, $submit_data, $options);

      if (empty($diff)) {
        $diff = '[unchanged]';
      }
      $ret[$retkey] = $diff;
    }
    else {
      $ret[$retkey] = $row['data'];
    }
  }

  // calculate diff for current state
  $curr = node_load($nid);
  $curr_diff = _opr2_tasks_calc_log_line_diff($nid, $curr, $options);
  if (empty($curr_diff)) {
    $curr_diff = '[unchanged]';
  }
  $ret["current {$curr->title}"] = $curr_diff;

  return $ret;
}

function _opr2_tasks_calc_log_line_diff($nid, $submit_data, $options) {
  static $last_states = array();

  $last_state = array();
  if (isset($last_states[$nid])) {
    $last_state = $last_states[$nid];
  }

  $checked_params = array();
  $diff = array();

  foreach ($submit_data as $param => $new_val) {
    // some params are shown no matter what
    if (!in_array($param, array('status','title'))) {
      if ($options['fields'] && !preg_match('/'.$options['fields'].'/', $param)) continue;
    }

    // empty field values filtering
    if (is_array($new_val)) {
      if (empty($new_val)) continue;
      if (empty($new_val['und'])) continue;
    }

    $checked_params[] = $param;

    $mod = null;
    if (empty($last_state[$param])) {
      $mod = '+';
    }
    else if ($new_val != $last_state[$param]) {
      $mod = '*';
    }

    if ($mod) {
      $diff[$param] = array(
        'flag' => $mod,
        'val' => $new_val,
      );
    }

    $last_state[$param] = $new_val;
  }

  $removed_params = array_diff(array_keys($last_state), $checked_params);
  if (!empty($removed_params)) {
    foreach ($removed_params as $param) {
      unset($last_state[$param]);
      $diff[$param] = array(
        'flag' => '-',
        'val' => '[DELETED]',
      );
    }
  }

  $last_states[$nid] = $last_state;
  return $diff;
}

function opr2_task_generate_approval_log($nid, $options = array()) {
  $approval_fields = _opr2_form_approval_fields(TRUE, TRUE);
  $start_time = time();
  $write = !empty($options['write']);

  if ($nid) {
    echo "=====\nfetching log data for $nid...\n";
    $qry = db_select('opr2_form_log', 'l')
      ->fields('l')
      ->condition('entity_id', $nid)
      ->orderBy('datetime', 'ASC');

    $res = $qry->execute();

    $qa_status = array();
    $past_ops = array();
    while ($row = $res->fetchAssoc()) {
      $usr = user_load($row['uid']);
      $retkey = implode(' -- ', array(
        $row['datetime'],
        'op:'.$row['op'],
        'uid:'.$row['uid'],
        'uname:'.(empty($usr) ? '[deleted]' : $usr->name),
      ));

      if ($insert) {
        $do_insert = TRUE;
        if ($insert['op'] == $row['op'] && $insert['original']['datetime'] == $row['datetime']) {
          echo "\t\tskipping dup insert\n";
          $do_insert = FALSE;
        }

        if ($do_insert) {
          _opr2_approval_insert_log($nid, $insert, $write);
        }
      }
      $insert = FALSE;

      echo "\t$retkey...\n";

      if (in_array($row['op'], array('add', 'edit'))) {
        $submit_data = _opr2_get_form_submit_data($row['datetime'], $row['entity_id']);
        if (empty($submit_data)) {
          echo "\t\tmissing submit data for {$row['datetime']}\n";
          continue;
        }

        $past_data = FALSE;
        if (count($past_ops)) {
          $past_data = in_array('add', $past_ops) || in_array('edit', $past_ops);
        }

        $insert_op = FALSE;
        $insert_data = array();
        foreach ($approval_fields as $field_name => $qa_param) {
          $new_val = NULL;
          if (!empty($submit_data[$field_name]['und'][0]['value'])) {
            $new_val = $submit_data[$field_name]['und'][0]['value'];
          }

          $updated = _opr2_task_approval_value_update($qa_status, $qa_param, $new_val);
          if ($updated) {
            if ($new_val == OPR2_QA_REJECTED) {
              $insert_op = 'reject';
              $insert_data[$qa_param] = $new_val;
            }

            else if ($new_val == OPR2_QA_APPROVED) {
              $insert_op = 'approve';
              $insert_data[$qa_param] = $new_val;
            }
          }
        }

        if ($insert_op) {
          if ($past_data || !empty($options['ignore-missing-ref'])) {
            $insert_data['original_op'] = $row['op'];
            $insert_data['generated'] = $start_time;
            if ($insert_op == 'reject' && empty($insert_data['reject_reason']) && !empty($qa_status['reject_reason'])) {
              $insert_data['reject_reason'] = $qa_status['reject_reason'];
            }

            $insert = array(
              'op' => $insert_op,
              'original' => $row,
              'data' => $insert_data,
            );
          }
          else {
            echo "\t\tskipping insert ($insert_op) because of missing reference data\n";
          }
        }
      }

      else if (in_array($row['op'], array('approve', 'reject', 'disqualify'))) {
        $approve_data = json_decode($row['data'], TRUE);

        foreach ($approval_fields as $field_name => $qa_param) {
          $new_val = FALSE;

          if (empty($approve_data[$qa_param])) {
            if ($row['op'] == 'approve' && isset($qa_status[$qa_param]) && $qa_status[$qa_param] == OPR2_QA_REJECTED) {
              $new_val = NULL;
            }
          }

          else {
            $new_val = $approve_data[$qa_param];
          }

          _opr2_task_approval_value_update($qa_status, $qa_param, $new_val);
        }
      }
      else {
        echo "\t\tdata: ".$row['data'].PHP_EOL;
      }

      $past_ops[] = $row['op'];
    }

    if ($insert) {
      _opr2_approval_insert_log($nid, $insert, $write);
    }
  }
}

  function _opr2_task_approval_value_print($val) {
    if (is_null($val)) {
      return 'NULL';
    }

    return $val;
  }

  function _opr2_task_approval_value_update(&$qa_status, $qa_param, $new_val = NULL) {
    if ($new_val === FALSE) {
      return FALSE;
    }

    $old_val = NULL;
    if (!empty($qa_status[$qa_param])) {
      $old_val = $qa_status[$qa_param];
    }

    if ($new_val != $old_val) {
      echo "\t\tchange in qa $qa_param: "._opr2_task_approval_value_print($old_val)." -> "._opr2_task_approval_value_print($new_val)."\n";
      $qa_status[$qa_param] = $new_val;
      return TRUE;
    }

    return FALSE;
  }

  function _opr2_approval_insert_log($nid, $insert, $write = TRUE) {
    $insert_fields = array(
      'entity_id' => $nid,
      'uid' => $insert['original']['uid'],
      'datetime' => $insert['original']['datetime'],
      'op' => $insert['op'],
      'data' => json_encode($insert['data']),
      'path' => isset($insert['original']['path']) ? $insert['original']['path'] : null,
    );

    echo "\t\tINSERT! ".json_encode($insert_fields);
    if ($write) {
      db_insert('opr2_form_log')->fields($insert_fields)->execute();
      echo "... inserted.";
    }
    echo PHP_EOL;
  }
