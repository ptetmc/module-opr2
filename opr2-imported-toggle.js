(function ($) {
Drupal.behaviors.Opr2ImportedToggle = {
  attach: function(context) {
    var state = readCookie('opr2_imported_toggle') || 'off';
    $('div.opr-forms-container .opr-imported-toggle > a').each(function() {
      var $a = $(this), $container = $a.closest('.opr-forms-container');

      if (state == 'off') {
        $container.addClass('opr-imported-toggled-off');
      }

      $a.click(function(){
        $container.toggleClass('opr-imported-toggled-off');
        createCookie('opr2_imported_toggle', $container.hasClass('opr-imported-toggled-off') ? 'off' : 'on');
        return false;
      });
    });
  }
};
})(jQuery);


