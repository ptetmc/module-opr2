(function ($) {
Drupal.behaviors.Opr2LabConvert = {
  attach: function(context) {
    $('form.node-form').delegate('a.opr2-lab-convert', 'click', function(){
      var $this = $(this),
        $input = $this.closest('.form-item').find('input'),
        toConvert = prompt(Drupal.t('Please enter the value in @unit', {'@unit':$this.data('unit')}), $input.val()),
        digits = $this.data('digits') || 3;

      if (toConvert != null) {
        toConvert = toConvert.replace(',', '.');
        $input.val((parseFloat(toConvert)*parseFloat($this.data('multiplier'))).toFixed(digits).replace('.', ','));
      }
      return false;
    });
  }
};
})(jQuery);

