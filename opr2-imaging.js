(function ($) {
Drupal.behaviors.Opr2Imaging = {
  attach: function(context) {
    var $imaging_radios = $('form.node-form input[name="field_imaging[und]"]'),
        $group_category = $imaging_radios.parents('div.group-category'),
        $inline_radios = $('div.group-inline-fields input[type=radio]', $group_category).not($imaging_radios).add('input[name="field_imagadm_pericardfluid[und]"], input[name="field_imagadm_lunginf2[und]"], input[name="field_imagadm_pancabnorm[und]"]'),
        $result_radios = $('input[name^="field_imaging_"][type=radio], input[name="field_imagadm_mrcp[und]"], input[name="field_imagadm_eus[und]"]').not($inline_radios);

    if ($imaging_radios.length
      && !$imaging_radios.filter(':checked').length
      && $result_radios.filter('[value="1"]:checked').length) {
      $imaging_radios.filter('[value="1"]').attr('checked', true);
//      $imaging_radios.filter('[value="0"]').attr('disabled', true);
    }

    $imaging_radios.change(function(){
      var $this = $(this),
        value = $imaging_radios.filter(':checked').val();

      if (value === '1') {
      // keeping already checked values
//        $inline_radios.attr('checked', false).change();
//        $result_radios.attr('checked', false).change();

      } else if (value === '0') {
        $inline_radios.attr('checked', false).filter('[value="-1"]').attr('checked', true);
        $result_radios.attr('checked', false).filter('[value="0"]').attr('checked', true);
        $inline_radios.change();
        $result_radios.change();
      }
    });
  }
};
})(jQuery);

