(function ($) {
Drupal.behaviors.Opr2Multipage = {
  attach: function(context) {
    $('.multipage-panes', context).once('opr2multipage', function () {
      var $panes = $('> div.field-group-multipage', this);
      var $form = $panes.parents('form');
      var $controls = $panes.find('.multipage-controls-list');

      if (Drupal.settings.field_group.multipage_move_submit && $controls.last().find('.form-actions').length) {
        var $controlsClone = $controls.last().find('.form-actions').clone();
        $controlsClone.find(':input:not(#edit-draft)').remove();
        $controlsClone.find(':input[id]').attr('id', null);
        $controls.each(function(){
          if (!$(this).find('.form-actions').length) {
            $controlsClone.clone().appendTo(this);
          }
        });
      }

    });

    $('.field-group-multiple-container .multiple-inline-element').each(function() {
      var $table = $(this).find('.multiple-field-gentes-family-res2'),
        $select = $(this).find('select');

      $select.change(function(){
        $table.toggle($select.val()!='_none');
      }).change();
    });
  }
};
})(jQuery);
