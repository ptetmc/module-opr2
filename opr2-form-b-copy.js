(function ($) {
Drupal.behaviors.Opr2FormBCopy = {
  attach: function(context) {
    $('ul.opr-study-list li.opr-form-b-copy a').click(function(){
      var $a = $(this),
        copyData = $a.data('form-b-copy'),
        modalHtml = '';

      if (!copyData) return true;
      console.log($a.attr('href'), copyData, $a);
      modalHtml += '<div class="messages status">'+
        Drupal.t('Please choose below which submitted form you would like to import data from:')+
        '</div>';

      modalHtml += '<ul>';
      $.each(copyData, function(day, forms) {
        day = parseInt(day);
        $.each(forms, function(nid, formData) {
          modalHtml += '<li class="leaf"><a href="'+$a.attr('href')+'&amp;copy_node='+nid+'">'+Drupal.t('Day @day: @form', {'@day':day+1,'@form':formData.title})+'</a></li>';
        });
      });
      modalHtml += '<li class="leaf"><a href="'+$a.attr('href')+'">'+Drupal.t('submit new @form without importing any data', {'@form':$a.text()})+'</a></ul>';

      Drupal.CTools.Modal.show('opr2-form-b-copy-modal-style');
      $('#modal-title').html(Drupal.t('Submit new @form', {'@form':$a.text()}));
      $('#modal-content').html(modalHtml);
      return false;
    });
  }
};
})(jQuery);

