(function ($) {
Drupal.behaviors.Opr2GarlicClearForm = {
  attach: function(context) {
    // console.log('clearing garlic form', Drupal.settings.opr2_garlic_clear_form);
    if (!Drupal.settings.opr2_garlic_clear_form) return;
    if (!localStorage) return;

    for (var key in localStorage) {
      if (key.indexOf(Drupal.settings.opr2_garlic_clear_form) !== -1) {
        localStorage.removeItem(key);
        // console.log('garlic: removed key', key);
      }
    }
  }
};
})(jQuery);
