(function ($) {
Drupal.behaviors.Opr2GarlicAttachForm = {
  attach: function(context) {
    var $forms = $('form.opr2-garlic-form');

    if ($.fn.garlic && $forms.length) {
      $forms.each(function() {
        var $form = $(this);
        $form.garlic({
          destroy: false,
          onRetrieve: function($emt, val) {
            // console.log('garlic.retr', $emt.attr('id'), val);

            if (!$form.data('retrieved.opr2') && !$('div.opr2-garlic-retrieved').length) {
              var messageTxt = Drupal.t('Previously lost form data has been recovered. If you want to delete the recovered data and start with an empty form, please use the following link:'),
              $message = $('<div class="messages warning opr2-garlic-retrieved">'+messageTxt+' </div>'),
              $messageLink = $('<a href="#" class="opr2-garlic-destroy-toggle"><strong>'+Drupal.t('Empty backup data')+'</strong></a>');

              $messageLink.click(function(){
                $form.find(':input').garlic('destroy');
                location.reload();
                return false;
              }).appendTo($message);

              $form.parents('div.region-content:first').before($message);
              $form.data('retrieved.opr2', 1);
            }

            if ($emt.parents('td.field-group-conditional-parent-field').length) {
              $emt.trigger('change');
              // console.log('garlic.change', $emt.attr('id'), val);
            }
          },
          onPersist: function(emt, val) {
            // console.log('garlic.pers', emt.attr('id'), val);
          }
        });
      });
    }

  }
};
})(jQuery);

