<?php

function opr2_cinc_bundle_register_acute_form_a() {
  $bundle = array(
    'machine_name' => 'register_acute_form_a',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => array(array('value'=>'v1')),
      ),
      'field_therapadm_iv_24h' => array(
        'description' => 'Az első 24 órát a beteg orvosi/mentős ellátásának kezdetétől kell számolni. A beteg osztályos felvételét követő, az első 24 órában történt folyadékpótlás is hozzáadandó. (A beteg kikérdezésekor még nem áll rendelkezésre ez az adat, csak a feltöltéskor.)',
        'i18n' => array(
          'description' => 'To be counted from the first moment    until the end of the first 24 hours of     medical treatment including ANY KIND of intravenous fluid (e.g. i.v. antibiotics) given by the    ambulance, emergency unit and inpatient department.',
        ),
      ),
      'field_therapint_endosc' => array(
        'label' => '10. Intervenció, endoszkópos kezelés az első napon',
        'i18n' => array(
          'label' => '10. Interventions, endoscopic treatment',
        ),
      ),
      'field_complication_panc' => array(
        'required' => FALSE,
      ),
      'field_jaundice_since' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_andpain_location' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_medi_type' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_medi_amount' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_medi_since' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_ab_med' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_ab_dose' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_ab_mode' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_field_therapy_pm_med' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_pm_dose' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_pm_type' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_complication_death' => array(
        'label' => 'Mortalitás',
      ),
      'field_complication_death_time' => array(
        'i18n' => array(
          'label' => 'the exact time of death',
          'description' => 'e.g. 10.25 or 22.45',
        ),
      ),
    ), // end instances

    'groups' => array(
      'group_etiology' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'A válasz igen ha az etiológiai faktor alátámasztott, a válasz nem, ha az etiológiai faktor kizárható, a válasz „nincs adat”, ha az etiológiai faktorra nem történt vizsgálat, a válasz „idiopáthiás”, ha etiológiai faktor nem azonosított.',
            'notes_en' => 'The answer is “yes” if the etiological factor is proved, the answer is “no” if the etiological factor can be ruled out, the answer is “no data” if the etiological factor was not examined. Please answer “yes” to ” Idiopathic” if etiological factor was not identified.',
          ),
        ),
      ),
      'group_lab_results_inline' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'Csak artériás vérgázparamétert lehet rögzíteni. Kérjük, jelezze a vérgáz paraméterek mérési körülményeit',
            'notes_en' => 'Only arterial blood gas parameters should be registered. Please indicate the measuring condition of blood gas parameters',
          ),
        ),
      ),
      'group_lab_results_req' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'label_visibility' => 1,
          ),
        ),
      ),
      'group_lab_results_opt' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'label_visibility' => 1,
          ),
        ),
      ),
      'group_imagadm' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'show_label' => 0,
          ),
        ),
      ),
      'group_imagadm_inline' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'classes' => 'group-inline-fields group-imagadm-inline field-group-div group-first-field-heading',
          ),
        ),
      ),
      'group_theradm' => array(
        'label' => '9.b Terápia a felvétel napján',
        'i18n' => array(
          'label' => '9.b Immediate therapy on the day of admission',
        ),
      ),
      'group_therapy_iv' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'A terápia kérdései ettől a kérdéstől kezdve a felvétel napjára vonatkoznak.',
            'notes_en' => 'The therapy questions refer to the day of admission  from here.',
          ),
        ),
      ),
      'group_therapint' => array(
        'label' => '8. Intervenció, endoszkópos kezelés az első napon',
        'format_settings' => array(
          'instance_settings' => array(
            'show_label' => FALSE,
          ),
        ),
        'i18n' => array(
          'label' => '8. Interventions - first day',
        ),
      ),
      'group_histology_outer' => array(
        'label' => '10. Szövettan',
        'i18n' => array(
          'label' => '10. Hystology',
        ),
      ),
      'group_therapint_endosc' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'singlecol_head' => '1',
            'classes' => 'group-category',
          ),
        ),
      ),
      'group_gentest' => array(
        'label' => '11. Genetikai vizsgálat',
        'i18n' => array(
          'label' => '11. Genetic testing',
        ),
      ),
      'group_complications' => array(
        'label' => '12. Szövődmények',
        'format_settings' => array(
          'instance_settings' => array(
            'notes_en' => 'Please register pancreatic complication of fluid collection/pseudocyst/necrosis only if you had imaging proof on the day of admission.',
          ),
        ),
        'i18n' => array(
          'label' => '12. Complications',
        ),
      ),
      'group_complication_death' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'keep_descriptions' => '1',
          ),
        ),
      ),
      'group_epic' => array(
        'label' => '13. Epikrízis',
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'A hospitalizáció rövid összefoglalója, beleértve, hogy hogyan került a beteg a kórházba, klinikára, mi történt vele a bentfekvés alatt és milyen javaslattal és hová távozott (kontroll vizsgálat, műtét, stb.)',
            'notes_en' => 'A short summary of the  hospitalization (how the patient got to medical care, diagnosis, most important facts and events of the hospitalization, what happened with the patient after the hospitalization, any recommended control examinations, surgery).',
          ),
        ),
        'i18n' => array(
          'label' => '13. Epicrisis',
        ),
      ),
      'group_finalreport' => array(
        'label' => '14. Zárójelentés',
        'i18n' => array(
          'label' => '14. Final report',
        ),
      ),
    ), // end groups

    'tree' => array(
      'group_nopaging' => array(
        'group_nopage_1' => array(
          'field_inpatient',
          'group_personal_cont' => array(
            'field_interview_date',
          ),
          'field_form_version',
          'group_anamnestic' => array(
            'group_alcohol' => array(
              'field_alcohol_consumption',
              'field_alcohol_freq',
              'field_alcohol_amount2',
              'field_alcohol_since2',
              'field_alcohol_2w_amount',
              'group_alcohol_past' => array(
                'field_alcohol_past',
                'field_alcohol_past_freq',
                'field_alcohol_past_amount',
                'field_alcohol_past_years',
                'field_alcohol_past_ended2',
              ),
            ),
            'group_smoking' => array(
              'field_smoking',
              'field_smoking_amount2',
              'field_smoking_since2',
              'group_smoking_past' => array(
                'field_smoking_past',
                'field_smoking_past_amount',
                'field_smoking_past_years',
                'field_smoking_past_ended2',
              ),
            ),
            'group_drugs' => array(
              'field_drug_consumption',
              'field_drug_name',
              'field_drug_amount',
              'field_drug_since3',
            ),
            'group_diabetes' => array(
              'field_diabetes',
              'field_diabetes_type',
              'field_diabetes_since',
            ),
            'group_lipdis' => array(
              'field_lipdis',
              'field_lipdis_since',
            ),
            'group_pancdis' => array(
              'field_pancdis',
              'field_pancdis_type',
              'field_pancdis_other',
              'group_pancdis_acute' => array(
                'field_pancdis_acute_times2',
                'field_pancdis_acute_first',
              ),
              'group_pancdis_chronic' => array(
                'field_pancdis_chronic_when',
                'field_pancdis_chronic_first',
                'field_pancdis_chron_acute_times',
              ),
              'group_pancdis_tumor' => array(
                'field_pancdis_tumor_when',
                'field_pancdis_tumor_chron',
                'field_pancdis_tumor_chronic_when',
                'field_pancdis_tumor_acute_times',
                'field_pancdis_tumor_acute_first',
              ),
              'field_pancdis_notes',
            ),
          ),
        ),
        'group_nopage_2' => array(
          'group_anamnestic_cont' => array(
            'group_pancdis_fam' => array(
              'field_pancdis_fam',
              'field_pancdis_fam_acute',
              'field_pancdis_fam_acute_rel',
              'field_pancdis_fam_chron',
              'field_pancdis_fam_chron_rel',
              'field_pancdis_fam_ai',
              'field_pancdis_fam_ai_rel',
              'field_pancdis_fam_tum',
              'field_pancdis_fam_tum_rel',
              'field_pancdis_fam_othertype',
              'field_pancdis_fam_othertype_rel',
            ),
            'group_pancdisord' => array(
              'field_pancdisord',
              'field_pancdisord_type',
            ),
            'group_otherdis' => array(
              'field_otherdis',
              'field_otherdis_type',
            ),
            'group_medi' => array(
              'field_medi',
              'field_medi_detail2',
            ),
            'group_medi_list' => array(
              'field_medi_type',
              'field_medi_amount',
              'field_medi_since',
            ),
            'group_diet' => array(
              'field_diet',
              'field_diet_type',
            ),
            'field_history',
          ),
        ),
        'group_nopage_3' => array(
          'group_etiology' => array(
            'field_etiology_biliary',
            'field_etiology_alcohol',
            'field_etiology_hypertrig',
            'field_etiology_postercp',
            'field_etiology_virus',
            'field_etiology_trauma',
            'field_etiology_druginduced',
            'field_etiology_congenital',
            'field_etiology_cysticfibrosit',
            'field_etiology_glutent',
            'field_etiology_genetic',
            'field_etiology_idiopathic',
            'group_etiology_other' => array(
              'field_etiology_other',
              'field_etiology_other_desc',
            ),
          ),
          'group_symptoms' => array(
            'group_abdpain' => array(
              'field_abdpain',
              'field_stoma_since',
              'field_abdpain_type',
              'field_abdpain_strength',
              'field_stoma_loc',
              'field_stoma_loc_detail',
              'field_stoma_rad',
            ),
            'group_nausea' => array(
              'field_nausea',
            ),
            'group_vomiting' => array(
              'field_vomiting',
              'field_vomiting_times2',
              'field_vomiting_contents2',
            ),
            'group_fever' => array(
              'field_fever',
              'field_fever_since2',
              'field_fever_amount2',
              'field_fever_amount_rect',
            ),
            'group_appetite' => array(
              'field_appetite',
            ),
            'group_weightloss' => array(
              'field_weightloss',
              'field_weightloss_weeks2',
              'field_weightloss_amount2',
            ),
            'group_stool' => array(
              'field_stool',
            ),
          ),
        ),
        'group_nopage_4' => array(
          'group_admission' => array(
            'group_admission_rows' => array(
              'group_admission_row1' => array(
                'field_bp',
                'field_bp_dias',
                'field_pulse',
              ),
              'group_admission_row2' => array(
                'field_weight',
                'field_height',
              ),
              'group_admission_row3' => array(
                'field_resprate',
                'field_bodytemp',
              ),
              'group_admission_row3b' => array(
                'field_bodytemp_rect',
              ),
              'group_admission_row6' => array(
                'field_o2sat',
                'field_o2sat_prevtherapy',
              ),
              'group_admission_row4_2' => array(
                'field_abdomtender',
                'field_abdomguard',
              ),
              'group_admission_row5_2' => array(
                'field_jaundice',
                'field_gcs',
              ),
            ),
          ),
        ),
        'group_nopage_5' => array(
          'group_lab_results' => array(
            'group_lab_results_3x' => array(
              'field_lab_3x_amilase',
              'field_lab_3x_lipase',
            ),
            'group_lab_results_req' => array(
              'field_lab_result_amilase',
              'field_lab_resut_lipase',
              'field_lab_result_wbc',
              'field_lab_result_rbc',
              'field_lab_result_hg',
              'field_lab_result_htoc',
              'field_lab_result_throm',
              'field_lab_result_gluc',
              'field_lab_result_urean',
              'field_lab_result_creat',
              'field_lab_result_egfr2',
              'field_lab_result_crp2',
              'field_lab_result_asatgot',
              'field_lab_result_ldh',
              'field_lab_result_ca',
            ),
            'group_lab_results_inline' => array(
              'field_lab_bloodgasmeas',
              'field_lab_o2therapy',
            ),
            'group_lab_results_opt' => array(
              'field_lab_result_na',
              'field_lab_result_k',
              'field_lab_result_totprot',
              'field_lab_result_alb',
              'field_lab_result_chol',
              'field_lab_result_trig',
              'field_lab_result_alatgpt',
              'field_lab_result_ggt',
              'field_lab_result_bilitot',
              'field_lab_result_dcbili',
              'field_lab_result_alph',
              'field_lab_result_esr',
              'field_lab_result_procalc2',
              'field_lab_result_iga',
              'field_lab_result_igm',
              'field_lab_result_igg',
              'field_lab_result_igg4',
              'field_lab_result_ca199',
              'field_lab_result_pao2',
              'field_lab_result_hco3',
              'field_lab_result_so2',
              'field_lab_result_swcl',
              'field_lab_result_uramil',
              'field_lab_result_urlip',
              'field_lab_result_urcreat',
            ),
            'group_virserol' => array(
              'field_virserol_virus',
              'field_virserol_res',
            ),
          ),
        ),
        'group_nopage_6' => array(
          'group_imagadm' => array(
            'group_imagadm_inline' => array(
              'field_imaging',
              'field_imagadm_pericardfluid',
              'field_imagadm_lunginf2',
            ),
            'group_imagadm_pancabnorm' => array(
              'field_imagadm_pancabnorm',
              'field_imagadm_pancabnorm_type',
            ),
            'group_imaging_abdus' => array(
              'field_imaging_abdus',
              'field_imaging_abdus_desc',
            ),
            'group_imaging_abdrtg' => array(
              'field_imaging_abdrtg',
              'field_imaging_abdrtg_desc',
            ),
            'group_imaging_chestrtg' => array(
              'field_imaging_chestrtg',
              'field_imaging_chestrtg_desc',
            ),
            'group_imaging_chestct' => array(
              'field_imaging_chestct',
              'field_group_imaging_chestct_desc',
            ),
            'group_imaging_abdct' => array(
              'field_imaging_abdct',
              'field_imaging_abdct_desc',
            ),
            'group_imagadm_mrcp' => array(
              'field_imagadm_mrcp',
              'field_imagadm_mrcp_desc',
            ),
            'group_imagadm_eus' => array(
              'field_imagadm_eus',
              'field_imagadm_eus_desc',
            ),
          ),
          'group_therapint' => array(
            'group_therapint_inline' => array(
              'field_therapyint',
            ),
            'group_therapint_ercp' => array(
              'field_therapint_ercp',
              'group_therapint_bilducan' => array(
                'field_therapint_bilducan',
                'field_therapint_bilducan_note',
              ),
              'group_therapint_precut' => array(
                'field_therapint_precut',
                'field_therapint_precut_type',
              ),
              'group_therapint_est' => array(
                'field_therapint_est',
                'field_therapint_est_type',
              ),
              'group_therapint_stonex' => array(
                'field_therapint_stonex',
              ),
              'group_ercp_stent' => array(
                'field_ercp_stent',
                'field_ercp_stent_material',
                'field_ercp_stent_amount',
                'field_ercp_stent_dia',
                'field_ercp_stent_length',
              ),
              'group_ercp_ductfill' => array(
                'field_ercp_ductfill',
                'field_ercp_ductfill_notes',
              ),
            ),
            'field_therapint_ercp_desc',
            'group_therapint_necrosect' => array(
              'field_therapint_necrosect',
              'field_therapint_necrosect_type',
              'field_therapint_necrosect_notes',
            ),
            'group_therapint_drain' => array(
              'field_therapint_drain',
              'field_therapint_drain_desc',
            ),
            'group_therapint_rinse' => array(
              'field_therapint_rinse',
              'field_therapint_rinse_notes',
            ),
            'group_therapint_other' => array(
              'field_therapint_other',
              'field_therapint_other_desc',
            ),
          ),
        ),
        'group_nopage_7' => array(
          'group_therapy_accent' => array(
            'field_therapadm_iv_24h',
          ),
          'group_theradm' => array(
            'group_therapy_iv' => array(
              'field_therapy_iv',
            ),
            'group_therapy_iv_list' => array(
              'field_therapy_iv_type',
              'field_therapy_iv_amount',
            ),
            'group_therapy_parfluid' => array(
              'field_therapy_parfluid',
            ),
            'group_therapy_parfeed_list' => array(
              'field_therapy_parfeed_formula',
              'field_therapy_parfeed_amount',
            ),
            'group_therapy_enteral' => array(
              'field_therapy_enteral',
              'field_therapy_enteral_type',
            ),
            'group_therapy_oral_list' => array(
              'field_therapy_enteral_formula',
              'field_therapy_enteral_amount',
              'field_therapy_enteral_dil',
            ),
            'group_therapy_oral_feeding' => array(
              'field_therapy_oral_feeding',
              'field_therapy_oral_desc',
            ),
            'group_therapy_pm' => array(
              'field_therapy_pm',
              'field_therapy_pm_detail2',
            ),
            'group_therapy_pm_list' => array(
              'field_field_therapy_pm_med',
              'field_therapy_pm_dose',
              'field_therapy_pm_type',
            ),
            'group_therapy_ab' => array(
              'field_therapy_ab',
              'field_therapy_ab_detail2',
            ),
            'group_therapy_ab_list' => array(
              'field_therapy_ab_med',
              'field_therapy_ab_dose',
              'field_therapy_ab_mode',
            ),
            'group_therapy_ins' => array(
              'field_therapy_ins',
            ),
            'field_therapy_ins_dosage2',
            'group_therapy_intensive' => array(
              'field_therapy_intensive',
              'field_therapy_intensive_desc2',
              'field_therapy_intensive_notes',
            ),
            'group_therapy_other' => array(
              'field_therapy_other',
              'field_therapy_other_desc',
            ),
          ),
          'group_histology_outer' => array(
            'group_histology' => array(
              'field_histology',
              'field_histology_desc',
            ),
          ),
          'group_gentest' => array(
            'group_gentest_pre' => array(
              'field_gentest_pre',
              'field_gentest_pre_res',
              'field_gentest_pre_res2',
            ),
          ),
        ),
        'group_nopage_8' => array(
          'group_complications' => array(
            'group_complication_panc' => array(
              'field_complication_panc',
              'field_complication_panc_type',
            ),
            'group_complication_orgfail' => array(
              'field_complication_orgfail',
              'field_complication_orgfail_org',
            ),
            'group_complication_death' => array(
              'field_complication_death',
              'field_complication_death_time',
            ),
          ),
          'group_epic' => array(
            'field_epic',
          ),
          'group_finalreport' => array(
            'field_finalreport',
          ),
          'group_severity' => array(
            'field_severity',
          ),
          'field_consent_form',
          'field_notes',
        ),
      ), // end paging
    ), // end tree
  );

  return $bundle;
}

function opr2_cinc_bundle_register_acute_form_a_compatibility($version = 'v0') {
  switch ($version) {
  case 'v0':
    return array(
      'field_etiology_idiopathic' => array(
        'required' => FALSE,
      ),
      'field_bp' => array(
        'required' => FALSE,
      ),
      'field_bp_dias' => array(
        'required' => FALSE,
      ),
      'field_pulse' => array(
        'required' => FALSE,
      ),
      'field_weight' => array(
        'required' => FALSE,
      ),
      'field_height' => array(
        'required' => FALSE,
      ),
      'field_resprate' => array(
        'required' => FALSE,
      ),
      'field_abdomtender' => array(
        'required' => FALSE,
      ),
      'field_abdomguard' => array(
        'required' => FALSE,
      ),
      'field_jaundice2' => array(
        'required' => FALSE,
      ),
      'field_gcs' => array(
        'required' => FALSE,
      ),
      'field_lab_result_amilase' => array(
        'required' => FALSE,
      ),
      'field_lab_result_wbc' => array(
        'required' => FALSE,
      ),
      'field_lab_result_rbc' => array(
        'required' => FALSE,
      ),
      'field_lab_result_hg' => array(
        'required' => FALSE,
      ),
      'field_lab_result_htoc' => array(
        'required' => FALSE,
      ),
      'field_lab_result_throm' => array(
        'required' => FALSE,
      ),
      'field_lab_result_gluc' => array(
        'required' => FALSE,
      ),
      'field_lab_result_urean' => array(
        'required' => FALSE,
      ),
      'field_lab_result_creat' => array(
        'required' => FALSE,
      ),
      'field_lab_result_crp2' => array(
        'required' => FALSE,
      ),
      'field_lab_result_asatgot' => array(
        'required' => FALSE,
      ),
      'field_lab_result_ldh' => array(
        'required' => FALSE,
      ),
      'field_lab_result_ca' => array(
        'required' => FALSE,
      ),
      'field_therapint_ercp' => array(
        'required' => FALSE,
      ),
    );

  case 'i1':
    return array(
      'field_alcohol_consumption' => array(
        'required' => FALSE,
      ),
      'field_smoking' => array(
        'required' => FALSE,
      ),
      'field_drug_consumption' => array(
        'required' => FALSE,
      ),
      'field_diabetes' => array(
        'required' => FALSE,
      ),
      'field_lipdis' => array(
        'required' => FALSE,
      ),
      'field_pancdis' => array(
        'required' => FALSE,
      ),
      'field_medi' => array(
        'required' => FALSE,
      ),
      'field_otherdis' => array(
        'required' => FALSE,
      ),
      'field_diet' => array(
        'required' => FALSE,
      ),
      'field_abdpain' => array(
        'required' => FALSE,
      ),
      'field_nausea' => array(
        'required' => FALSE,
      ),
      'field_vomiting' => array(
        'required' => FALSE,
      ),
      'field_fever' => array(
        'required' => FALSE,
      ),
      'field_appetite' => array(
        'required' => FALSE,
      ),
      'field_weightloss' => array(
        'required' => FALSE,
      ),
      'field_complication_orgfail' => array(
        'required' => FALSE,
      ),
      'field_jaundice' => array(
        'required' => FALSE,
      ),
      'field_stool' => array(
        'required' => FALSE,
      ),
      'field_jaundice2' => array(
        'required' => FALSE,
      ),
      'field_consent_form' => array(
        'required' => FALSE,
      ),
      'field_complication_death' => array(
        'required' => FALSE,
      ),
      'field_therapy_other' => array(
        'required' => FALSE,
      ),
      'field_therapy_intensive' => array(
        'required' => FALSE,
      ),
      'field_therapy_ins' => array(
        'required' => FALSE,
      ),
      'field_therapy_ab' => array(
        'required' => FALSE,
      ),
      'field_therapy_pm' => array(
        'required' => FALSE,
      ),
      'field_therapy_enteral' => array(
        'required' => FALSE,
      ),
      'field_therapy_iv' => array(
        'required' => FALSE,
      ),
      'field_imaging_chestct' => array(
        'required' => FALSE,
      ),
      'field_imaging_abdct' => array(
        'required' => FALSE,
      ),
      'field_imaging_chestrtg' => array(
        'required' => FALSE,
      ),
      'field_imaging_abdrtg' => array(
        'required' => FALSE,
      ),
      'field_imaging_abdus' => array(
        'required' => FALSE,
      ),
      'field_gentest_pre' => array(
        'required' => FALSE,
      ),
      'field_etiology_other' => array(
        'required' => FALSE,
      ),
      'field_etiology_idiopathic' => array(
        'required' => FALSE,
      ),
      'field_imagadm_mrcp' => array(
        'required' => FALSE,
      ),
      'field_imagadm_eus' => array(
        'required' => FALSE,
      ),
      'field_histology' => array(
        'required' => FALSE,
      ),
      'field_therapint_ercp' => array(
        'required' => FALSE,
      ),
      'field_severity' => array(
        'required' => FALSE,
      ),
      'field_therapy_parfluid' => array(
        'required' => FALSE,
      ),
      'field_imaging' => array(
        'required' => FALSE,
      ),
      'field_therapyint' => array(
        'required' => FALSE,
      ),
    );
    break;
  }
}
