<?php

$groups_from = field_group_info_groups('node', 'pineapple_p_form_a_eng', 'form');
$groups_to = field_group_info_groups('node', 'easy_form_a', 'form');

foreach ($groups_to as $group_name => $group_conf) {
  if (isset($groups_from[$group_name])) i18n_string_translation_update(
    array('field_group', 'node', 'easy_form_a', 'form', $group_name, 'label' ),
    $groups_from[$group_name]->label, 'en'
  );
}

