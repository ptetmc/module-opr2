<?php

module_load_include('inc', 'opr2', 'opr2_cinc_field');
$instances = field_info_instances('node', 'register_cancer_form_a');
//dpm($instances);

$export = array();
$fields = array();
$existing = array();
$dump = '';

foreach ($instances as $field_name => $instance) {
  $config = opr2_cinc_field($field_name);
  if (!empty($config)) {
    $existing[$field_name] = $config;
    continue;
  }
  $field_info = field_info_field($field_name);
  $fields[$field_name] = array( 'type' => $field_info['type'], 'cardinality' => $field_info['cardinality'], );
  foreach ($field_info['settings'] as $prop => $value) {
    if ($value !== '') {
      $fields[$field_name]['settings'][$prop] = $value;
    }
  }
  foreach (array('label', 'required', 'description', 'default_value') as $prop) {
    if (!empty($instance[$prop])) {
      $export[$field_name][$prop] = $instance[$prop];
    }
  }
  if (isset($export[$field_name]['default_value']) && $export[$field_name]['default_value'] === array(array('value'=>0))) {
    unset($export[$field_name]['default_value']);
  }
  $export[$field_name]['widget'] = array( 'weight' => $instance['widget']['weight'], 'type' => $instance['widget']['type'], );
  if (!empty($instance['widget']['settings'])) {
    $export[$field_name]['widget']['settings'] = $instance['widget']['settings'];
  }
  foreach ($instance['settings'] as $prop => $value) {
    if (in_array($prop, array('user_register_form', 'text_processing'))) continue;
    if ($value === '') continue;
    $export[$field_name]['settings'][$prop] = $value;
  }
  $fields[$field_name]['instance'] = $export[$field_name];
  $dump .= "\ncase '$field_name':\nreturn ".var_export($fields[$field_name], TRUE).";\n";
}
dpm($existing, 'already existing configs');
dpm($dump);
