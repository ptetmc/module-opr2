<?php

$bundles = array_keys(opr2_forms());

$qry = new EntityFieldQuery();
$qry->entityCondition('entity_type', 'node')
  ->propertyCondition('type', $bundles, 'IN')
  ->propertyCondition('nid', 3000, '<')
  ->propertyCondition('status', 1);

$forms = $qry->execute();

if (!empty($forms['node'])) {
  $nids = array_keys($forms['node']);
  dpm($nids, 'nids to delete');

  node_delete_multiple($nids);
}
