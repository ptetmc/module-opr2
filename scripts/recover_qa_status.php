<?php

$query = db_select('node', 'n')->fields('n', array('nid', 'title'))
  	->condition('n.status', 0);
$query->join('opr2_form_log', 'l', 'l.entity_id = n.nid');
$query->fields('l', array('data', 'datetime', 'op'))
  ->condition('l.op', array('approve', 'reject', 'edit'));

$query->orderBy('l.datetime', 'ASC');

$results = $query->execute()->fetchAll();
$states = array();
foreach ($results as $key => $res) {
  
  if (!empty($res->data)) {
    $decoded = json_decode($res->data, true);
    
    if (!empty($decoded)) {
      $states[$res->nid]['o'][$res->op][] = $decoded;
      if (FALSE && isset($states[$res->nid]['l']) && $states[$res->nid]['l'] == 'edit') {
        if (!empty($states[$res->nid]['s'])) dpm($states[$res->nid], 'reset '.$res->nid);
        $states[$res->nid]['s'] = array();
        $states[$res->nid]['r'][] = $res->op;
      }
      foreach ($decoded as $level => $val) {
        $states[$res->nid]['s'][$level] = $val;
      }
      if (!empty($decoded['reject_reason'])) {
        $states[$res->nid]['rr'][] = $decoded['reject_reason'];
      }
    }
  } else {
    $states[$res->nid]['o'][$res->op][] = $res->op;
  }
  
  $states[$res->nid]['l'] = $res->op;
  $states[$res->nid]['t'] = $res->title;

}

$levels = array('own', 'local', 'admin', 'investigator');

foreach ($states as $nid => $st) {
  if ($st['l'] != 'edit') continue;
  if (empty($st['s'])) continue;
  if (empty($st['o']['reject'])) continue;
  $stxt = '';
  $empty_levels = array();
  foreach ($levels as $level) {
    if (empty($st['s'][$level])) {
      $empty_levels[] = $level;
    }
    else {
      if (!empty($empty_levels)) {
        foreach ($empty_levels as $empty_level) {
          $st['s'][$empty_level] = 1;
        }
      }
    }
  }
  
  foreach ($levels as $level) {
    if (empty($st['s'][$level])) {
      $stxt .= "N\t";
    }
    else {
      $stxt .= $st['s'][$level]."\t";
    }
  }
  
  $node = node_load($nid);
  if (!empty($node->field_qa_author)) {
  	dpm($node, 'node_before '.$nid);
    continue;
  }
  
  if (!empty($st['s']['own'])) {
    $node->field_qa_author = array('und'=>array(array('value'=>$st['s']['own'])));
  }
  if (!empty($st['s']['local'])) {
    $node->field_qa_local = array('und'=>array(array('value'=>$st['s']['local'])));
  }
  if (!empty($st['s']['admin'])) {
    $node->field_qa_admin = array('und'=>array(array('value'=>$st['s']['admin'])));
  }
  if (!empty($st['s']['ivestigator'])) {
    $node->field_qa_investigator = array('und'=>array(array('value'=>$st['s']['investigator'])));
  }
  dpm($node, 'node_after '.$nid);
  node_save($node);
  
  dpm($stxt.$st['t'], "last edited $nid");
}

//dpm($states);

//dpm($results);
