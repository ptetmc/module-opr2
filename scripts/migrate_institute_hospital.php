<?php

$forms = node_load_multiple(array(), array('type' => 'inpatient'));
$hosps = node_load_multiple(array(), array('type' => 'hospital'));
$users = entity_load('user');

$hosp_names = array_map(function($hosp) { return $hosp->title; }, $hosps);
//dpm($hosp_names);

$state = array();

foreach ($forms as $form) {
  $hosp_id = false;
  if (!empty($form->field_hospital)) {
    dpm($form->field_hospital, 'existing hosp for '. $form->title);
    $state['existing'][] = $form->nid;
    continue;
  }

  if (!empty($form->field_institute)) {
    dpm($form->field_institute['und'][0]['value'], 'existing inst for '.$form->title);

    if ($hosp_id = array_search($form->field_institute['und'][0]['value'], $hosp_names)) {
      $from = 'inst';

    } else {
      dpm($form->field_institute['und'][0]['value'], 'couldnt find hosp for '.$form->title);
    }
  }

  if (!empty($form->uid)) {
    if (!isset($users[$form->uid])) {
      dpm($form, 'missing user for '.$form->title);

    } else if (empty($users[$form->uid]->field_hospital)) {
      dpm($users[$form->uid], 'missing user hospital for '.$form->title);

    } else {
      $from = 'user';
      $hosp_id = $users[$form->uid]->field_hospital['und'][0]['target_id'];
    }
  }

  if ($hosp_id) {
    $form->field_hospital['und'][0]['target_id'] = $hosp_id;
    node_save($form);
    dpm($hosp_id, 'found hosp for '.$form->title.' from '.$from);
    $state['from-'.$from][] = $form->nid;

  } else {
    $state['none'][] = $form->nid;
  }
}

dpm($state, 'finis');
