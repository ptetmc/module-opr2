<?php

$fields_from = field_info_instances('node', 'easy_form_a');

$export = array();
foreach ($fields_from as $field_name => $field_instance) {
  $i18n = array();
  $translated_instance = i18n_string_object_translate('field_instance', $field_instance, array('langcode'=>'en'));
  foreach (array('label', 'description') as $prop) {
    if (empty($field_instance[$prop])) continue;
    if (empty($translated_instance[$prop])) continue;
    if ($translated_instance[$prop] == $field_instance[$prop]) continue;
    $i18n[$prop] = $translated_instance[$prop];
  }

  $field_info = field_info_field($field_name);
  if (in_array($field_info['type'], array('list_text', 'list_integer'))) {
    $allowed_values = i18n_field_translate_allowed_values($field_info, 'en');
    if (!empty($allowed_values)) foreach($field_info['settings']['allowed_values'] as $key => $label) {
      if (empty($allowed_values[$key])) continue;
      if ($allowed_values[$key] == $label) continue;
      $i18n['allowed_values'][$key] = $allowed_values[$key];
    }
  }

  if (empty($i18n)) continue;
  $export[$field_name] = "\ncase \"$field_name\":\n  return ".var_export($i18n, true).";\n";
}
drupal_set_message('<textarea>'.implode('', $export).'</textarea>');
//dpm($export);
