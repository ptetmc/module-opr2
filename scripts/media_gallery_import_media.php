<?php

$gallery = node_load(107);

$gallery_media = isset($gallery->media_gallery_media[LANGUAGE_NONE]) ? $gallery->media_gallery_media[LANGUAGE_NONE] : array();
dpm($gallery_media, 'starting files for '.$gallery->title);

$existing = array();
foreach ($gallery_media as $gm) {
  $existing[$gm['fid']] = TRUE;
}

$query = db_select('file_managed', 'f');
$query->fields('f', array('fid', 'filename'));
$query->orderBy('f.filename', 'ASC');
$query->condition('f.type', 'image');
$query->condition('f.filename', '_MG_%', 'LIKE');

$result = $query->execute()->fetchAllKeyed();
dpm($result, 'found files');

foreach ($result as $result_fid => $result_filename) {
  if (isset($existing[$result_fid])) continue;
  if (!preg_match('/_MG_(\d+)_web\.jpg/', $result_filename, $matches)) continue;
  $img_id = intval($matches[1]);
  if ($img_id < 2547 || $img_id > 2829) continue;
  $gallery_media[] = array(
    'fid' => $result_fid,
    'title' => null,
    'data' => '',
  );
}

/*
foreach ($result as $result_fid => $result_filename) {
  if (!preg_match('/21-friday-(\d+)/', $result_filename, $matches)) continue;
  $img_id = intval($matches[1]);
  $gm = array(
    'fid' => $result_fid,
    'title' => null,
    'data' => '',
  );
  if ($img_id < 740) {
    $gallery_media_new[] = $gm;
  } else {
    $gallery_media_supper[] = $gm;
  }
}
*/

dpm($gallery_media, 'resulting files');

$gallery->media_gallery_media[LANGUAGE_NONE] = $gallery_media;
node_save($gallery);

