<?php

$bundle = 'apple_p_form_a';
$clones = array(
  array('field_alcohol_since2', 'field_alcpreg_since2'),
  array('field_smoking_amount2', 'field_smoksec_amount2'),
  array('field_smoking_since2', 'field_smoksec_since2'),
  array('field_smoking_amount2', 'field_smokpreg_amount2'),
);

foreach ($clones as $clone_fields) {
  $source_field = field_info_field($clone_fields[0]);
  $source_instance = field_info_instance('node', $clone_fields[0], $bundle);

  try {
    $source_field['field_name'] = $clone_fields[1];
    field_create_field($source_field);

    $source_instance['field_name'] = $clone_fields[1];
    field_create_instance($source_instance);
    dpm('cloned', $clone_fields[1]);

  } catch (Exception $e) {
    dpm($e->getMessage(), $clone_fields[1]);
  }
}
