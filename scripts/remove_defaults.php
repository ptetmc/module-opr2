<?php

/* remove default values for required boolean list fields in easy_form_a */
$instances = field_info_instances('node', 'easy_form_a');
foreach ($instances as $instance) {
  if (!empty($instance['default_value']) && !empty($instance['required']) && $instance['widget']['type'] == 'options_buttons') {
    $instance['default_value'] = null;
    field_update_instance($instance);
  }
}
