<?php

$fields_to = field_info_instances('node', 'easy_form_a');

foreach ($fields_to as $field_name => $field_instance) {
  $field_info = field_info_field($field_name);
  if ($field_info['type'] == 'number_decimal') {
    $field_info['settings']['decimal_separator'] = ',';
    if (field_update_field($field_info)) drupal_set_message('Updated '.$field_instance['label']);
    else drupal_set_message('Failed '.$field_instance['label'], 'warning');
  }
}


