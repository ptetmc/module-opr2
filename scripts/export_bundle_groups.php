<?php

module_load_include('inc', 'opr2', 'opr2_cinc_field_group');
$groups = field_group_info_groups('node', 'register_cancer_form_a', 'form');

$existing = array();

$export = "";
foreach ($groups as $group_name => $group) {
  $config = opr2_cinc_field_group($group_name);
  if (!empty($config)) {
    $existing[$group_name] = $config;
    continue;
  }

  $exp = array();
  foreach ( array('label', 'format_type') as $prop) { // , 'weight'
    if (!empty($group->$prop)) {
      $exp[$prop] = $group->$prop;
    }
  }
  if (!empty($group->format_settings['formatter'])) {
    $exp['format_settings']['formatter'] = $group->format_settings['formatter'];
  }
  foreach ($group->format_settings['instance_settings'] as $prop => $val) {
    if (trim($val) === '') continue;
    $exp['format_settings']['instance_settings'][$prop] = $val;
  }
  $export .= "\ncase '$group_name':\nreturn ".var_export($exp, TRUE).";\n";
}

dpm($existing, 'already existing configs');
drupal_set_message("<textarea>$export</textarea>");
