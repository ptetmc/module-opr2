<?php

function _opr2_build_form_tree($type) {
  $groups = field_group_info_groups('node', $type, 'form');
  $fields = field_info_instances('node', $type);
  dpm($groups);
  dpm($fields);

  return _opr2_build_form_tree_do_tree($groups, $fields);
}

function _opr2_build_form_tree_do_tree($groups, $fields = array(), $group_name = null) {
  $tree = array();

  if (empty($group_name)) {
    $children = array();
    foreach ($groups as $group) {
      if (empty($group->children)) continue;
      $children = array_merge($children, $group->children);
    }

    $root_groups = array_diff(array_keys($groups), $children);
    foreach ($root_groups as $parent) { // TODO order by weight
      $tree[$parent] = _opr2_build_form_tree_do_tree($groups, $parent);
    }

    return $tree;
  }

  if (empty($groups[$group_name]->children)) return array();

  $children = array();
  foreach ($groups[$group_name]->children as $child) {
    $children[$child] = 0;
    if (isset($groups[$child]->weight)) $children[$child] = $groups[$child]->weight;
    else if (isset($fields[$child]['widget']['weight'])) $children[$child] = $fields[$child]['widget']['weight'];
  }
  asort($children);

  foreach ($children as $child => $weight) {
    if (isset($groups[$child])) {
      $tree[$child] = _opr2_build_form_tree_do_tree($groups, $fields, $child);
    } else {
      $tree[] = $child;
    }
  }

  return $tree;
}

dvm(_opr2_build_form_tree('register_cancer_form_a'));
