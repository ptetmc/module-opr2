jQuery('textarea[name*=allowed_values]').each(function(){
  jQuery(this).val(
    jQuery(this).val()
      .replace('1 óra', '1 hour')
      .replace('óra', 'hours')
      .replace('1 hónap', '1 month')
      .replace('hónap', 'months')
      .replace('1 nap', '1 day')
      .replace('nap', 'days')
      .replace('1 hét', '1 week')
      .replace('hét', 'weeks')
      .replace('év', 'year')
      .replace('több, mint', 'more than')
  );
})
