<?php

$users = entity_load('user');
$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'institute');
$result = $query->execute();

$institutes = entity_load('node', array_keys($result['node']));

$user_institutes = array();
foreach ($users as $usr) {
  if (empty($usr->institute_user_reference)) continue;
  if (!isset($institutes[$usr->institute_user_reference['und'][0]['nid']])) continue;
  $user_institutes[$usr->mail] = $institutes[$usr->institute_user_reference['und'][0]['nid']]->title;
}

dvm($user_institutes);

/**
 * OPR2
 */


$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'user')
    ->propertyCondition('mail', array_keys($user_institutes), 'IN');

$result = $query->execute();
$users = entity_load('user', array_keys($result['user']));
dpm($users);

foreach ($users as $usr) {
  $usrw = entity_metadata_wrapper('user', $usr);
  $usrw->field_institute->set($user_institutes[$usr->mail]);
  $usrw->save();
  dpm($usrw->field_institute->value(), $user_institutes[$usr->mail]);
}
    
