<?php

$fields_to = field_info_instances('node', 'easy_form_a');

foreach ($fields_to as $field_name => $field_instance) {
  $field_info = field_info_field($field_name);
  if ($field_info['type'] == 'list_boolean' && i18n_string_translate(array('field', $field_name, '#allowed_values', 1), 'igen', array('langcode'=>'en')) == 'igen') {
    i18n_string_update(array('field', $field_name, '#allowed_values', 1), 'igen', array('messages'=>true));
    i18n_string_update(array('field', $field_name, '#allowed_values', 0), 'nem', array('messages'=>true));
    i18n_string_translation_update(array('field', $field_name, '#allowed_values', 1), 'yes', 'en');
    i18n_string_translation_update(array('field', $field_name, '#allowed_values', 0), 'no', 'en');
    dpm($field_info, $field_instance['label']);
  }
}

