<?php

$fields_from = field_info_instances('node', 'easy_form_a');
$fields_to = field_info_instances('node', 'apple_p_form_a');

foreach ($fields_to as $field_name => $field_instance) {
  if (isset($fields_from[$field_name]) && $field_instance['label'] == $fields_from[$field_name]['label']) {
    $translation = i18n_string_translate(array('field', $field_name, $fields_from[$field_name]['bundle'], 'label'), $fields_from[$field_name]['label'], array('langcode'=>'en'));
    if ($translation != $field_instance['label']) {
      i18n_string_translation_update(array('field', $field_name, $field_instance['bundle'], 'label'), $translation, 'en');
      dpm($translation, $field_instance['label']);
    }
  }
}

