<?php

$fields_from = field_info_instances('node', 'pineapple_p_form_a_eng');
$fields_to = field_info_instances('node', 'easy_form_a');

foreach ($fields_to as $field_name => $field_instance) {
  if (isset($fields_from[$field_name])) {
    i18n_string_translation_update(array('field', $field_name, $field_instance['bundle'], 'label'), $fields_from[$field_name]['label'], 'en');
    dpm($fields_from[$field_name]['label'], $field_instance['label']);
  }
}
