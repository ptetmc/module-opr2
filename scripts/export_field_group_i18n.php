<?php

$bundle_from = 'register_acute_form_a';
$bundle_to = 'register_cancer_form_a';
$groups_from = field_group_info_groups('node', $groups_from, 'form');
$groups_to = field_group_info_groups('node', $groups_to, 'form');

$export = array();
foreach ($groups_to as $group_name => $group_conf) {
  if (!isset($groups_from[$group_name])) {
    dpm('missing group', $group_name);
    continue;
  }
  
  $translation = i18n_string_translate(
    array('field_group', 'node', $bundle_from, 'form', $group_name, 'label' ),
    $groups_from[$group_name]->label,
    array('langcode'=>'en')
  );

  if (empty($translation)) {
    dpm('empty translation', $group_name);
    continue;
  }
  if ($translation == $groups_from[$group_name]->label) {
    dpm('identical translation: '.$translation, $group_name);
    continue;
  }

  $export[] = "case \"$group_name\":\n  return '$translation';\n";

  if ($groups_to[$group_name]->label == $groups_from[$group_name]->label) {
    dpm('translating', $group_name);
    i18n_string_translation_update(
      array('field_group', 'node', $bundle_to, 'form', $group_name, 'label' ),
      $translation,
      'en',
      $groups_to[$group_name]->label
    );
  }
}

drupal_set_message("<textarea>".implode("\n", $export)."</textarea>");
