<?php

set_time_limit(0);
ini_set('memory_limit', '512M');
module_load_include('inc', 'opr2', 'opr2_import');

$args = drush_get_arguments();
$data_filename = $args[2];

echo "loading data file $data_filename...\n";
$patients_serialized = file_get_contents($data_filename);
$patients = unserialize($patients_serialized);

opr2_import($patients, 1);

