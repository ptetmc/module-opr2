<?php

$bundles = array('register_acute_form_a', 'register_acute_form_b');
$clear_all = FALSE;

ini_set('memory_limit', '512M');
set_time_limit(0);
require_once (__DIR__.'/../opr2_o2l.inc');

if (empty($bundles)) {
  $bundles = array_keys(opr2_form());
}

if ($clear_all) {
  cache_clear_all('opr2_node_', 'cache', TRUE);
  _o2l($clear_all, 'emptying opr2_node_% cache...', 'ok');
}

_o2l($bundles, 'updating node cache for '.count($bundles)." bundles\n");

$qry = new EntityFieldQuery();
$qry->entityCondition('entity_type', 'node')
  ->propertyCondition('type', $bundles, 'IN');

$nids = $qry->execute();

if (empty($nids['node'])) {
  _o2l($nids, "0 nodes to update\n", 'ok');
  return;
}

$chunks = array_chunk(array_keys($nids['node']), 100);
$chunks_num = count($chunks);
foreach ($chunks as $chunk_id => $chunk) {
  $chunk_cnt = count($chunk);
  _o2l($chunk, "loading $chunk_cnt ($chunk_id / $chunks_num) nodes {$chunk[0]} through ".$chunk[$chunk_cnt-1]."...", 'ok');
  $chunk_nodes = opr2_load_node($chunk);
  _o2l(array_keys($chunk_nodes), "  loaded.", 'ok');
}

_o2l($nids, 'done.', 'ok');
