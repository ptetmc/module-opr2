(function ($) {
Drupal.behaviors.Opr2Patient = {
  attach: function(context) {
    var $insurance = $('form.node-form input[name*=field_patient_insuration_id]');
//    console.log('OPR2PATIENT', $insurance);
    if (!$insurance.length) return;
  
    var $throbber = $('<div class="ajax-progress ajax-progress-throbber" style="display:none;"><div class="throbber">&nbsp;</div><div class="message">'+Drupal.t('Please wait...')+'</div></div>'),
      $inputs = $insurance.closest('div.field-name-field-patient').find('input, select').not($insurance);
//    $inputs.attr('disabled', 'disabled');

      if ($insurance.parents('.patient-readonly').length) {
        $insurance.attr('readonly', true);
        $inputs.filter('[type=text]').attr('readonly', true);
        $inputs.find('option').not(':selected').attr('disabled', true);
        $inputs.filter('[type=radio], [type=checkbox]').not(':checked').attr('disabled', true);

      } else {
        $insurance.after($throbber);
        $insurance.change(function(){
          $insurance.attr('disabled', 'disabled');
          $throbber.show();

          $.get(Drupal.settings.basePath+'opr/patient.json', {insuration_id:$insurance.val()}, function(data) {
            $throbber.hide();
            if (data.patient) {
              var patient = $.extend({name:'',gender:'_none',race:'_none',birth_date_year:'',birth_date_month:'',birth_date_day:''},data.patient);
              $inputs.filter('[name*=field_patient_name]').val(patient.name).attr('readonly', true);
              $inputs.filter('[name*=field_patient_gender]').attr({disabled:true,checked:false}).filter('[value="'+patient.gender+'"]').attr({disabled:false,checked:true});
              $inputs.filter('[name*=field_patient_race]').find('option').attr({disabled:true,selected:false}).filter('[value="'+patient.race+'"]').attr({disabled:false,selected:true});
              $inputs.filter('[name*="field_patient_birth_date][und][0][value][year"]').find('option').attr({disabled:true,selected:false}).filter('[value='+patient.birth_date_year+']').attr({disabled:false,selected:true});
              $inputs.filter('[name*="field_patient_birth_date][und][0][value][month"]').find('option').attr({disabled:true,selected:false}).filter('[value='+patient.birth_date_month+']').attr({disabled:false,selected:true});
              $inputs.filter('[name*="field_patient_birth_date][und][0][value][day"]').find('option').attr({disabled:true,selected:false}).filter('[value='+patient.birth_date_day+']').attr({disabled:false,selected:true});
            } else {
              $inputs.filter('[name*=field_patient_name]').attr('readonly', false).val('');
              $inputs.filter('[disabled]').attr('disabled', false);
              $inputs.find('option').attr({disabled:false,selected:false}).filter('[value=_none]').attr('selected', true);
            }

            $insurance.attr('disabled', false);
          });
        });
      }
  },

  toggleReadonly: function($inputs) { // TODO
    $inputs.each(function(){
      var $input = $(this);
    });
  }
};
})(jQuery);

