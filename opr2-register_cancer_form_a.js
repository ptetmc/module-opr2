(function ($) {
Drupal.behaviors.Opr2RegisterCancerFormA = {
  attach: function(context) {
    $('form.node-form').delegate('#edit-field-height-before-und-0-value, #edit-field-weight-before-und-0-value', 'change', function(){
      var height = parseInt($('#edit-field-height-before-und-0-value').val()),
        weight = parseInt($('#edit-field-weight-before-und-0-value').val()), bmi;

      if (height && weight) {
        bmi = weight * 10000 / height / height;
        $('#edit-field-bmi-before-und-0-value').val(bmi.toFixed(2).toString().replace('.', ','));
      }
    });
    $('form.node-form').delegate('#edit-field-height-und-0-value, #edit-field-weight-und-0-value', 'change', function(){
      var height = parseInt($('#edit-field-height-und-0-value').val()),
        weight = parseInt($('#edit-field-weight-und-0-value').val()), bmi;

      if (height && weight) {
        bmi = weight * 10000 / height / height;
        $('#edit-field-bmi-und-0-value').val(bmi.toFixed(2).toString().replace('.', ','));
      }
    });
    $('form.node-form').delegate('#edit-field-tumor-histology-res-und', 'change', function(){
      var val = $(this).val();

      $('#edit-field-tumor-histology-res-duct').toggle(val == 1);
      $('#edit-field-tumor-histology-res-nonepi').toggle(val == 10);
    });
    $('#edit-field-tumor-histology-res-und').change();

    $('#edit-field-chemtherapy-protocol-type-und').change(function(){
      $('#edit-field-chemtherapy-protocol-med').toggle($(this).val() != '3').find('input[type=text]').val('');
    }).change();
    $('#edit-field-moltherapy-protocol-type-und').change(function(){
      $('#edit-field-moltherapy-protocol-med').toggle($(this).val() == '2').find('input[type=text]').val('');
    }).change();
    $('#edit-field-moltherapy-protocol-type-und').change(function(){
      $('#edit-field-molther-ne-med').toggle($(this).val() == '3').find('input[type=text]').val('');
    }).change();
    $('#edit-field-peptherapy-type-und').change(function(){
      $('#edit-field-peptherapy-med').toggle($(this).val() == '4').find('input[type=text]').val('');
    }).change();
  }
};
})(jQuery);


