(function ($) {
Drupal.behaviors.Opr2FieldFunctexam = {
  attach: function(context) {
    var $functexam_radios = $('form.node-form input[name="field_functexam[und]"]'),
        $group_category = $functexam_radios.parents('div.group-category'),
        $result_radios = $group_category.find('td.field-group-conditional-parent-field > .field-type-list-boolean input[type=radio]');

    if ($functexam_radios.length
      && !$functexam_radios.filter(':checked').length
      && $result_radios.filter('[value="1"]:checked').length) {
      $functexam_radios.filter('[value="1"]').attr('checked', true);
//      $functexam_radios.filter('[value="0"]').attr('disabled', true);
    }

    $functexam_radios.change(function(){
      var $this = $(this),
        value = $functexam_radios.filter(':checked').val();

      if (value === '1') {
      // keeping already checked values
      // $result_radios.attr('checked', false).change();

      } else if (value === '0') {
        $result_radios.attr('checked', false).filter('[value="0"]').attr('checked', true);
        $result_radios.change();
      }
    });
  }
};
})(jQuery);

