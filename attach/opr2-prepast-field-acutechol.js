(function ($) {
Drupal.behaviors.Opr2PREPASTFieldAcuteChol = {
  attach: function(context) {
    var $acutechol = $('input[name="field_rand_acutechol[und]"]'),
      $critAll = $('div.group_acutechol input[type=radio]'),
      $crit = $critAll.filter('[value=1]'),
      compute = function() {
        var acutechol = true;
        if ($crit.filter(':checked').length != $crit.length) {
          acutechol = false;
        }

        $acutechol.filter('[value=1]').attr({
          disabled: !acutechol,
          checked: acutechol,
        });
        $acutechol.filter('[value=0]').attr({
          disabled: acutechol,
          checked: !acutechol,
        });
        $acutechol.change();
      };

      $critAll.change(compute);
      compute();
  }
};
})(jQuery);


