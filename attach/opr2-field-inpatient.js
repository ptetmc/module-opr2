(function ($) {
Drupal.behaviors.Opr2FieldInpatient = {
  attach: function(context) {
    var $ip_start = $('#edit-field-inpatient-und-form-field-inpatient-start-und-0-value'),
      $ip_end = $('#edit-field-inpatient-und-form-field-inpatient-end-und-0-value'),
      getParts = function($container) {
        var parts = $container.find('select').map(function(){return $(this).val();}).filter(function(idx, part) { return part.length;});
        if (parts.length == 3) return parts;
        return null;
      },
      getDate = function($container) {
        var parts = getParts($container);
        if (!parts) return null;

        var date = new Date(parts[0], parts[1], parts[2]);
        return date;
      },
      changeDate = function($container, newDate) {
        var $sel_year = $container.find('select[id$="-year"]' ),
          $sel_month  = $container.find('select[id$="-month"]'),
          $sel_day    = $container.find('select[id$="-day"]'  );

        $sel_year .val(newDate.getFullYear());
        $sel_month.val(newDate.getMonth());
        $sel_day  .val(newDate.getDate());
      };

    $ip_start.change(function(){
      var start_date = getDate($ip_start),
        end_date = getDate($ip_end);

      if (start_date) {
        if (end_date) {
          if (start_date > end_date) {
            changeDate($ip_end, start_date);
          }
        }
        else {
          changeDate($ip_end, start_date);
        }
      }
    });

    $ip_end.change(function(){
      var start_date = getDate($ip_start),
        end_date = getDate($ip_end);

      if (start_date > end_date) {
        changeDate($ip_end, start_date);
      }
    });
  }
};
})(jQuery);


