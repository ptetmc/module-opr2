(function ($) {
Drupal.behaviors.Opr2PREPASTFieldRandID = {
  attach: function(context) {
  return;
    var $hospital = $('#edit-field-hospital-und'),
      $critmet = $('input[name="field_rand_critmet[und]"]'),
      $acutechol = $('input[name="field_rand_acutechol[und]"]'),
      $cholest = $('input[name="field_rand_cholest[und]"]'),
      $randid = $('#edit-field-serial-prepast-und-0-value'),
      compute = function() {
        if (!$critmet.filter('[value="1"]:checked').length) {
          $randid.val('');
          return;
        }

        partHosp = $hospital.val();
        partGroup = 'C';
        if ($acutechol.filter('[value="1"]:checked').length) {
          partGroup = 'A';
        }
        else if ($cholest.filter('[value="1"]:checked').length) {
          partGroup = 'B';
        }

        partRand = 'xxx';
        $randid.val(partHosp+'-'+partGroup+'-'+partRand);
      };

      $hospital.change(compute);
      $critmet.change(compute);
      $acutechol.change(compute);
      $cholest.change(compute);
  }
};
})(jQuery);


