(function ($) {
Drupal.behaviors.Opr2FieldHistology = {
  attach: function(context) {
    var $histology_radios = $('form.node-form input[name="field_histology[und]"]'),
        $group_category = $histology_radios.parents('div.group-category'),
        $result_radios = $group_category.find('td.field-group-conditional-parent-field > .field-type-list-boolean input[type=radio]');

    if ($histology_radios.length
      && !$histology_radios.filter(':checked').length
      && $result_radios.filter('[value="1"]:checked').length) {
      $histology_radios.filter('[value="1"]').attr('checked', true);
//      $histology_radios.filter('[value="0"]').attr('disabled', true);
    }

    $histology_radios.change(function(){
      var $this = $(this),
        value = $histology_radios.filter(':checked').val();

      if (value === '1') {
      // keeping already checked values
      // $result_radios.attr('checked', false).change();

      } else if (value === '0') {
        $result_radios.attr('checked', false).filter('[value="0"]').attr('checked', true);
        $result_radios.change();
      }
    });
  }
};
})(jQuery);

