(function ($) {
Drupal.behaviors.Opr2PREPASTFieldCholest = {
  attach: function(context) {
    var $cholest = $('input[name="field_rand_cholest[und]"]'),
      $critAll = $('div.group_cholest input[type=radio]'),
      $crit = $critAll.filter('[value=1]'),
      compute = function() {
        var cholest = true;
        if ($crit.filter(':checked').length != $crit.length) {
          cholest = false;
        }

        $cholest.filter('[value=1]').attr({
          disabled: !cholest,
          checked: cholest,
        });
        $cholest.filter('[value=0]').attr({
          disabled: cholest,
          checked: !cholest,
        });
        $cholest.change();
      };

      $critAll.change(compute);
      compute();
  }
};
})(jQuery);


