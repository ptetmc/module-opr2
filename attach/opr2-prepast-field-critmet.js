(function ($) {
Drupal.behaviors.Opr2PREPASTFieldCritMet = {
  attach: function(context) {
    var $critmet = $('input[name="field_rand_critmet[und]"]'),
      $inclAll = $('div.group_incl input[type=radio]'),
      $incl = $inclAll.filter('[value=1]'),
      $exclAll = $('div.group_excl input[type=radio]'),
      $excl = $exclAll.filter('[value=0]'),
      compute = function() {
        var critmet = true;
        if ($incl.filter(':checked').length != $incl.length) {
          critmet = false;
        }
        if ($excl.filter(':checked').length != $excl.length) {
          critmet = false;
        }

        $critmet.filter('[value=1]').attr({
          disabled: !critmet,
          checked: critmet,
        });
        $critmet.filter('[value=0]').attr({
          disabled: critmet,
          checked: !critmet,
        });
        $critmet.change();
      };

      $inclAll.change(compute);
      $exclAll.change(compute);
      compute();
  }
};
})(jQuery);


