(function ($) {
Drupal.behaviors.Opr2BundleRegisterCPA = {
  attach: function(context) {
    var $form = $('#register-chronic-form-a-node-form'),
      $checkin_days = $('form.node-form input[name="field_checkin_days[und][0][value]"]'),
      $checkin_start = $('#edit-field-checkin-admission-und-0-value'),
      $checkin_end = $('#edit-field-checkin-discharge-und-0-value'), parts_start, parts_end, date_start, date_end;

    $checkin_days.bind('refresh.opr2', function(){
      parts_start = $checkin_start.find('select').map(function(){return $(this).val();}).filter(function(idx, part) { return part.length;});
      if (parts_start.length < 3) return;

      parts_end = $checkin_end.find('select').map(function(){return $(this).val();}).filter(function(idx, part) { return part.length;});
      if (parts_end.length < 3) return;

      date_start = new Date(parts_start[0], parts_start[1], parts_start[2]);
      date_end   = new Date(parts_end  [0], parts_end  [1], parts_end  [2]);

      $checkin_days.val(Math.floor((date_end.getTime() - date_start.getTime()) / 86400000));
    });

    $checkin_start.change(function(){
      $checkin_days.trigger('refresh.opr2');
    });
    $checkin_end.change(function(){
      $checkin_days.trigger('refresh.opr2');
    });
  }
};
})(jQuery);



