<?php

function opr2_drush_command() {
  $items['opr2-notify-rejected'] = array(
    'description' => 'Notify uploaders about new form rejections',
    'aliases' => array('o2nr'),
    'options' => array(
      'usr' => 'user to fetch',
      'old-rejections' => 'send notification for past rejections',
      'mail-override' => 'send notification to',
      'ignore-setting' => 'send notification to all users ignoring settings',
      'ignore-lastnote' => 'ignore last notification time',
      'reset-last' => 'reset last notification dates; don\'t send email',
    ),
  );

  $items['opr2-log-diff'] = array(
    'description' => 'Fetch logged data diffs',
    'aliases' => array('o2ld'),
    'arguments' => array(
      'nid' => 'nid to fetch',
    ),
    'options' => array(
      'usr' => 'user to fetch',
      'datetime' => 'fetch a single logged event at datetime',
      'fields' => 'fetch only fields matchin regex',
    ),
  );

  $items['opr2-log-approve-generate'] = array(
    'description' => 'Generate missing approval log',
    'aliases' => array('o2lag'),
    'arguments' => array(
      'nid' => 'nid(s) to process',
    ),
    'options' => array(
      'write' => 'write changes',
      'imported-only' => 'process only imported forms',
      'ignore-missing-ref' => 'generate approval log even if reference form data is missing',
    ),
  );

  return $items;
}

function drush_opr2_notify_rejected() {
  module_load_include('inc', 'opr2', 'opr2_o2l');

  $usr = drush_get_option('usr', null);
  $options = array(
    'old_rejections' => drush_get_option('old-rejections', 0),
    'mail_override' => drush_get_option('mail-override', null),
    'ignore_setting' => drush_get_option('ignore-setting', null),
    'ignore_lastnote' => drush_get_option('ignore-lastnote', null),
    'reset_last' => drush_get_option('reset-last', null),
  );

  $users = array();
  if (!empty($usr)) {
    $users[] = user_load($usr);
  }

  _o2l($options, 'executing task: notify rejected...');
  module_load_include('inc', 'opr2', 'opr2.tasks');
  opr2_task_notify_rejected($users, $options);

  _o2l(null, 'done.', 'ok');
}

function drush_opr2_log_diff($nid = null) {
  module_load_include('inc', 'opr2', 'opr2_o2l');

  if (empty($nid)) {
    drush_set_error(dt('Missing nid argument.'));
    return 1;
  }

  $options = array(
    'usr' => drush_get_option('usr', null),
    'datetime' => drush_get_option('datetime', null),
    'fields' => drush_get_option('fields', 'field_.*'),
  );

  _o2l($options, 'fetching opr2 log data...');
  module_load_include('inc', 'opr2', 'opr2.tasks');
  $data = opr2_task_fetch_log_diff($nid, $options);

  if (empty($data)) {
    _o2l($data, 'no log data found', 'ok');
    return 0;
  }

  foreach ($data as $meta => $diff) {
    echo "=====\n$meta:\n";

    if (is_array($diff)) {
      foreach ($diff as $field => $changes) {
        if (is_array($changes) || is_object($changes)) {
          if (isset($changes['flag'])) {
            echo "\t[{$changes['flag']}] $field: ";
            unset($changes['flag']);
          }
          else {
            echo "\t$field: ";
          }
          if (isset($changes['val']) && count($changes) == 1) {
            $changes = $changes['val'];
          }
          echo json_encode($changes), PHP_EOL;
        }
        else {
          echo "\t$field: ", (string) $changes, PHP_EOL;
        }

      }
    }
    else {
      echo "\tdata: ", (string) $diff, PHP_EOL;
    }
  }

  _o2l(null, 'done.', 'ok');
}

function drush_opr2_log_approve_generate($nid = null) {
  module_load_include('inc', 'opr2', 'opr2_o2l');

  if (empty($nid)) {
    drush_set_error(dt('Missing nid argument.'));
    return 1;
  }

  $options = array(
    'write' => drush_get_option('write', '0'),
    'imported-only' => drush_get_option('imported-only', '0'),
    'ignore-missing-ref' => drush_get_option('ignore-missing-ref', '0'),
  );

  echo "fetching opr2 log data with options ", json_encode($options), "...\n";

  if (strpos($nid, '-') > 0) {
    $nid_limits = explode('-', $nid);

    $opr2_forms = opr2_forms();
    $qry = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.nid', $nid_limits[0], '>=')
      ->condition('n.nid', $nid_limits[1], '<=')
      ->condition('n.type', array_keys($opr2_forms), 'IN');

    if (FALSE) { // turned off for performance reasons
      $log_qry = db_select('opr2_form_log', 'l')
        ->fields('l', array('entity_id'))
        ->where('l.entity_id = n.nid');
      $qry->exists($log_qry);
    }

    if (!empty($options['imported-only'])) {
      $imp_qry = db_select('field_data_field_imported', 'fi')
        ->fields('fi', array('entity_id'))
        ->condition('fi.field_imported_value', 1)
        ->where('fi.entity_id = n.nid');
      $qry->exists($imp_qry);
    }

    $qry->orderBy('n.nid');

    $res = $qry->execute();
    $nids = $res->fetchCol();

    if (count($nids) > 100) {
      set_time_limit(0);
      ini_set('memory_limit', '512M');
    }
  }
  else if (strpos($nid, ',') > 0) {
    $nids = explode(',', $nid);
  }
  else {
    $nids = array($nid);
  }

  echo "\tprocessing nids: ", implode(', ', $nids), PHP_EOL;

  module_load_include('inc', 'opr2', 'opr2.tasks');
  foreach ($nids as $nid) {
    opr2_task_generate_approval_log($nid, $options);
  }

  _o2l(null, 'done.', 'ok');
}
