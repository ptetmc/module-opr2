<?php

define ('O2DBG', FALSE);

function _o2l($data, $label = null, $type = 'notice') {
  if (drupal_is_cli()) {
    require_once  DRUPAL_ROOT . '/includes/utility.inc';
    if (O2DBG) {
      if (is_null($label)) {
        $label = '[nolabel]';
      }
      if (!is_string($data)) {
        $data = drupal_var_export($data, $label);
      }
      drush_log("$label: $data", $type);
    }
    else {
      if (is_null($label) && is_string($data)) {
        $label = $data;
      }
      drush_log($label, $type);
    }
  }
  else {
    dpm($data, $label);
  }
}
